//
// Created by akhtyamovpavel on 4/14/20.
//

#include <memory>
#include "LinearizationVisitor.h"
#include "IncludeNodes.h"

namespace IRT {

LinearizationVisitor::LinearizationVisitor(const std::string &filename)
    : stream_(filename), type_checker(filename + "_type_.out") {

}

void LinearizationVisitor::Visit(ExprStatement *stmt) {
  stream_ << "ExpStatement:" << std::endl;
  auto t = Accept(stmt->expr_);
  tos_value_.statement_ = new ExprStatement(t.expression_);

}

void LinearizationVisitor::Visit(ConstExpr *const_expression) {
  stream_ << "ConstExpression " << const_expression->Value() << std::endl;
  tos_value_.expression_ = new ConstExpr(const_expression->Value());
}

void LinearizationVisitor::Visit(JumpConditionalSt *jump_conditional_statement) {
  stream_ << "JumpConditionalStatement: ";
  auto l = Accept(jump_conditional_statement->left_);
  auto r = Accept(jump_conditional_statement->right_);
  tos_value_.statement_ = new JumpConditionalSt(jump_conditional_statement->op_,
                                                l.expression_,
                                                r.expression_,
                                                jump_conditional_statement->l_true_,
                                                jump_conditional_statement->l_false_);
}

void LinearizationVisitor::Visit(MoveStatement *move_statement) {
  stream_ << "MoveStatement:" << std::endl;

  auto l = Accept(move_statement->source_).expression_;
  auto r = Accept(move_statement->target_).expression_;
  tos_value_.statement_ = new MoveStatement(l, r);
}

void LinearizationVisitor::Visit(SeqStatement *seq_statement) {
  stream_ << "SeqStatement:" << std::endl;
  auto first = Accept(seq_statement->first_).statement_;
  auto first_last = tos_last_seq;
  auto second = Accept(seq_statement->second_).statement_;
  auto second_last = tos_last_seq;
  auto first_data = type_checker.Accept(first);
  auto second_data = type_checker.Accept(second);
  if (first_data.type_ == BaseNode::IrType::SeqStatement) {
    auto first_last_data = type_checker.Accept(first_last);
    auto p =  first_last_data.first_.statement_;
    auto t =  new SeqStatement(first_last_data.second_.statement_, second);
    type_checker.Set(dynamic_cast<SeqStatement *>(first_last),
        IrNodeInfo(BaseNode::IrType::SeqStatement,
            IrStorage(p),
            IrStorage(t)));

    if (second_data.type_ == BaseNode::IrType::SeqStatement) {
      tos_last_seq = second_last;
    } else {
      tos_last_seq = t;
    }
    tos_value_.statement_ = first;
    return;
  }
  auto t = new SeqStatement(first, second);
  if (second_data.type_ == BaseNode::IrType::SeqStatement) {
    tos_last_seq = second_last;
  } else {
    tos_last_seq = t;
  }
  tos_value_.statement_ = t;

}
void LinearizationVisitor::Visit(LabelStatement *label_statement) {
  stream_ << "LabelStatement: " << std::endl;
  tos_value_.statement_ = new LabelStatement(label_statement->label_);
}
void LinearizationVisitor::Visit(BinopExpr *binop_statement) {
  stream_ << "BinopExpression: " << ToString(binop_statement->op_) << std::endl;
  auto first = Accept(binop_statement->first_).expression_;
  auto second = Accept(binop_statement->second_).expression_;
  tos_value_.expression_ = new BinopExpr(binop_statement->op_, first, second);
}
void LinearizationVisitor::Visit(TempExpr *temp_exression) {
  stream_ << "TempExpression: " << std::endl;
  tos_value_.expression_ = new TempExpr(temp_exression->temp_);
}

void LinearizationVisitor::Visit(MemExpr *mem_expression) {
  stream_ << "MemExpression: " << std::endl;
  auto addr = Accept(mem_expression->expr_).expression_;
  tos_value_.expression_ = new MemExpr(addr);
}

void LinearizationVisitor::Visit(JumpStatement *jump_statement) {
  stream_ << "JumpStatement: " << std::endl;
  tos_value_.statement_ = new JumpStatement(jump_statement->label_);
}

void LinearizationVisitor::Visit(CallExpr *call_expression) {
  auto func = Accept(call_expression->expr_).expression_;
  auto args = Accept(call_expression->args_).expr_list_;

  tos_value_.expression_ = new CallExpr(func, args);
}

void LinearizationVisitor::Visit(ExprList *expression_list) {
  stream_ << "ExpressionList: " << std::endl;
  auto expression_list_ = new ExprList();
  for (auto expression: expression_list->exprs_) {
    expression_list_->Add(Accept(expression).expression_);
  }
  tos_value_.expr_list_ = expression_list_;
}

void LinearizationVisitor::Visit(NameExpr *name_expression) {
  stream_ << "NameExpression: " << std::endl;
  tos_value_.expression_ = new NameExpr(name_expression->label_);
}

void LinearizationVisitor::Visit(EseqExpr *eseq_expression) {
  stream_ << "EseqExpression:" << std::endl;
  auto st = Accept(eseq_expression->st_).statement_;
  auto expr = Accept(eseq_expression->expr_).expression_;
  tos_value_.expression_ = new EseqExpr(st, expr);
}

LinearizationVisitor::~LinearizationVisitor() {
  stream_.close();

}
Statement *LinearizationVisitor::GetIrTree() {
  return tos_value_.statement_;
}
std::unordered_map<std::string, std::vector<Statement *>> LinearizationVisitor::GetBlocks(Statement* start) {
  std::unordered_map<std::string, std::vector<Statement *>> blocks;
  auto temp_info = type_checker.Accept(start);
  auto right_child = type_checker.Accept(temp_info.second_.statement_);
  auto left_child = type_checker.Accept(temp_info.first_.statement_);
  std::string current_label;
  while (temp_info.type_ == BaseNode::IrType::SeqStatement) {
    if (left_child.type_ == BaseNode::IrType::LabelStatement) {
      auto next_label = dynamic_cast<LabelStatement *>(temp_info.first_.statement_)->label_.ToString();
      if (!current_label.empty()) {
        blocks[current_label].emplace_back(new JumpStatement(Label(next_label)));
      }
      current_label = next_label;
      blocks[current_label];
      blocks[current_label].emplace_back(temp_info.first_.statement_);
    } else if (left_child.type_ == BaseNode::IrType::JumpConditionalSt || left_child.type_ == BaseNode::IrType::JumpStatement) {
      blocks[current_label].emplace_back(temp_info.first_.statement_);
      current_label = "";
    } else {
      blocks[current_label].emplace_back(temp_info.first_.statement_);
    }

    if (right_child.type_ == BaseNode::IrType::LabelStatement) {
      auto next_label = dynamic_cast<LabelStatement *>(temp_info.second_.statement_)->label_.ToString();
      if (!current_label.empty()) {
        blocks[current_label].emplace_back(new JumpStatement(Label(next_label)));
      }
      current_label = next_label;
      blocks[current_label];
      blocks[current_label].emplace_back(temp_info.second_.statement_);
    } else if (right_child.type_ == BaseNode::IrType::JumpConditionalSt || right_child.type_ == BaseNode::IrType::JumpStatement) {
      blocks[current_label].emplace_back(temp_info.second_.statement_);
      current_label = "";
    } else if (right_child.type_ != BaseNode::IrType::SeqStatement) {
      blocks[current_label].emplace_back(temp_info.second_.statement_);
    }

    temp_info = right_child;
    if (temp_info.type_ == BaseNode::IrType::SeqStatement) {
      right_child = type_checker.Accept(temp_info.second_.statement_);
      left_child = type_checker.Accept(temp_info.first_.statement_);
    }
  }
  if (!current_label.empty()) {
    blocks[current_label].emplace_back(new JumpStatement(Label("done")));
    blocks["done"];
    blocks["done"].emplace_back(new LabelStatement(Label("done")));
  }

  return blocks;
}

}