//
// Created by akhtyamovpavel on 4/14/20.
//

#include "InstructionSelector.h"

#include <utility>
#include <assembler/visitors/PrintVisitor.h>
#include "IncludeNodes.h"

namespace IRT {

Temporary r_0("r_0");

ASM::BaseInstruct::InstructType ConvertOperations(LogicOperator op) {
  switch (op) {
    case LogicOperator::LT:return ASM::BaseInstruct::InstructType::BLT;
    case LogicOperator::GT:return ASM::BaseInstruct::InstructType::BGE;
    case LogicOperator::NE:return ASM::BaseInstruct::InstructType::BNG;
    case LogicOperator::EQ:return ASM::BaseInstruct::InstructType::BEQ;
  }
}

InstructionSelector::InstructionSelector(const std::string &filename) : /* stream_(filename), */ printer(filename) {

}
// TODO : есть ли такое вообще
void InstructionSelector::Visit(ExprStatement *stmt) {
  // stream_ << "ExpStatement:" << std::endl;
  std::cerr << "ExpStatement:" << std::endl;
  Accept(stmt->expr_);
  res_prev_instruction_ = {"_empty_"};
}

void InstructionSelector::Visit(ConstExpr *const_expression) {
  // stream_ << "ConstExpression " << const_expression->Value() << std::endl;
  Temporary const_temp;
  tos_value_ = {new ASM::AddI(const_temp, r_0, const_expression->Value())};
  res_prev_instruction_ = const_temp;
}

void InstructionSelector::Visit(JumpConditionalSt *jump_conditional_statement) {
  // stream_ << "JumpConditionalStatement: " << std::endl;
  auto l = Accept(jump_conditional_statement->left_);
  auto l_res = res_prev_instruction_;
  auto r = Accept(jump_conditional_statement->right_);
  auto r_res = res_prev_instruction_;
  Temporary after_sub;
  l.splice(l.end(), r);
  tos_value_ = std::move(l);
  tos_value_.emplace_back(new ASM::Sub(after_sub, l_res, r_res));
  tos_value_.emplace_back(new ASM::CondBranch(ConvertOperations(jump_conditional_statement->op_), jump_conditional_statement->l_true_, after_sub));
  tos_value_.emplace_back(new ASM::Jump(jump_conditional_statement->l_false_));
}

void InstructionSelector::Visit(MoveStatement *move_statement) {
  // stream_ << "MoveStatement:" << std::endl;
  auto l_info = type_checker_.Accept(move_statement->source_);
  if (l_info.type_ == BaseNode::IrType::MemExpr) {
    auto l = Accept(l_info.first_.expression_);
    auto l_res = res_prev_instruction_;
    auto r = Accept(move_statement->target_);
    auto r_res = res_prev_instruction_;

    l.splice(l.end(), r);

    tos_value_ = std::move(l);
    tos_value_.emplace_back(new ASM::Store(l_res, 0, r_res));
  } else {
//    std::cerr << "Left operand of move not mem" << std::endl;
    auto l = Accept(move_statement->source_);
    auto l_res = res_prev_instruction_;
    auto r = Accept(move_statement->target_);
    auto r_res = res_prev_instruction_;
    l.splice(l.end(), r);
    tos_value_ = std::move(l);
    tos_value_.emplace_back(new ASM::AddI(l_res, r_res, 0));
  }
//  tos_value_.statement_ = new MoveStatement(l, r);
}

// Их нет
void InstructionSelector::Visit(SeqStatement *seq_statement) {
  // stream_ << "SeqStatement:" << std::endl;
  std::cerr << "SeqStatement" << std::endl;
}
void InstructionSelector::Visit(LabelStatement *label_statement) {
  // stream_ << "LabelStatement: " << std::endl;
  tos_value_ = {new ASM::Label(label_statement->label_)};
}
void InstructionSelector::Visit(BinopExpr *binop_statement) {
  // stream_ << "BinopExpression: " << ToString(binop_statement->op_) << std::endl;

  auto first_instr = Accept(binop_statement->first_);
  auto first_res = res_prev_instruction_;
  auto second_instr = Accept(binop_statement->second_);
  auto second_res = res_prev_instruction_;
  Temporary ret;
  first_instr.splice(first_instr.end(), second_instr);
  tos_value_ = std::move(first_instr);
  std::list<ASM::BaseInstruct*> next_instr;
  switch (binop_statement->op_) {
    case BinOperator::PLUS:tos_value_.emplace_back(new ASM::Add(ret, first_res, second_res));
      break;
    case BinOperator::MINUS:tos_value_.emplace_back(new ASM::Sub(ret, first_res, second_res));
      break;
    case BinOperator::MUL:tos_value_.emplace_back(new ASM::Mul(ret, first_res, second_res));
      break;
    case BinOperator::DIV:tos_value_.emplace_back(new ASM::Div(ret, first_res, second_res));
      break;
  }
  res_prev_instruction_ = ret;

}
void InstructionSelector::Visit(TempExpr *temp_exression) {
  // stream_ << "TempExpression: " << std::endl;
  tos_value_ = {};
  res_prev_instruction_ = temp_exression->temp_;
}

void InstructionSelector::Visit(MemExpr *mem_expression) {
  // stream_ << "MemExpression: " << std::endl;
//  std::cerr << "MemExpr" << std::endl;
  Accept(mem_expression->expr_);
  auto addr = res_prev_instruction_;
  Temporary res;
  tos_value_.emplace_back(new ASM::Load(res, addr, 0));
  res_prev_instruction_ = res;
}

void InstructionSelector::Visit(JumpStatement *jump_statement) {
  // stream_ << "JumpStatement: " << std::endl;
  tos_value_ = {new ASM::Jump(jump_statement->label_)};
}

void InstructionSelector::Visit(CallExpr *call_expression) {
  // stream_ << "CallExpression: " << std::endl;
  auto func_name = dynamic_cast<NameExpr*>(call_expression->expr_)->label_;
  Temporary ret_value;
  auto args = call_expression->args_;
  std::vector<Temporary> args_res;
  std::list<ASM::BaseInstruct*> args_instr;
  for (auto arg : args->exprs_) {
    args_instr.splice(args_instr.end(), Accept(arg));
    args_res.emplace_back(res_prev_instruction_);
  }
  tos_value_ = std::move(args_instr);
  tos_value_.emplace_back(new ASM::Call(ret_value, func_name, args_res));
  res_prev_instruction_ = ret_value;
}

void InstructionSelector::Visit(ExprList *expression_list) {
  // stream_ << "ExpressionList: " << std::endl;
  std::cerr << "ExpressionList" << std::endl;
}
// TODO : когда используем
void InstructionSelector::Visit(NameExpr *name_expression) {
  // stream_ << "NameExpression: " << std::endl;
  std::cerr << "NameExpr" << std::endl;
}
// Их нет
void InstructionSelector::Visit(EseqExpr *eseq_expression) {
  // stream_ << "EseqExpression:" << std::endl;
  std::cerr << "NameExpr" << std::endl;
}



InstructionSelector::~InstructionSelector() {
  // stream_.close();
}


std::vector<ASM::BaseInstruct *> InstructionSelector::GetInstructions(std::unordered_map<std::string, std::vector<Statement *>> traces) {
  traces_ = std::move(traces);
  std::vector<ASM::BaseInstruct *> instructions;
  for (auto trace : traces_) {
    printer.Print("----- Trace:" + trace.first + " -----");
    for (auto instr : trace.second) {
      auto a = Accept(instr);
      printer.Print("");
      for (auto b : a) {
        b->Accept(&printer);
        instructions.emplace_back(b);
      }
    }
  }
  return instructions;
}






}