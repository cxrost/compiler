//
// Created by akhtyamovpavel on 4/14/20.
//


#pragma once

#include <string>
#include <fstream>
#include "Visitor.h"
namespace IRT {
class PrintVisitor: public Visitor {

 public:
  explicit PrintVisitor(const std::string& filename);
  ~PrintVisitor();
  void Visit(ExprStatement *stmt) override;
  void Visit(ConstExpr *const_expression) override;
  void Visit(JumpConditionalSt *jump_conditional_statement) override;
  void Visit(MoveStatement *move_statement) override;
  void Visit(SeqStatement *seq_statement) override;
  void Visit(LabelStatement *label_statement) override;
  void Visit(BinopExpr *binop_statement) override;
  void Visit(TempExpr *temp_exression) override;
  void Visit(MemExpr *mem_expression) override;
  void Visit(JumpStatement *jump_statement) override;
  void Visit(CallExpr *call_expression) override;
  void Visit(NameExpr *name_expression) override;
  void Visit(EseqExpr *eseq_expression) override;
  void Visit(ExprList *expression_list) override;
  void PrintString(std::string str);
 private:
  std::ofstream stream_;
  int num_tabs_ = 0;

  void PrintTabs();

};

}

