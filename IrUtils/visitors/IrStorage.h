//
// Created by pavel on 24.07.2020.
//
#pragma once

#include "IncludeNodes.h"

namespace IRT {

class IrStorage {
 public:
  explicit IrStorage() = default;
  explicit IrStorage(Expression* expr) : expression_(expr) {}
  explicit IrStorage(Statement* st) : statement_(st) {}
  explicit IrStorage(ExprList* expr_list) : expr_list_(expr_list) {}
  Expression* expression_{nullptr};
  Statement* statement_{nullptr};
  ExprList* expr_list_{nullptr};
};


}
