//
// Created by akhtyamovpavel on 4/14/20.
//


#pragma once

#include <string>
#include <fstream>
#include "TemplateVisitor.h"
#include "IrStorage.h"
#include "IrNodeInfo.h"
namespace IRT {
class InfoNodeVisitor: public TemplateVisitor<IRT::IrNodeInfo> {

 public:
  explicit InfoNodeVisitor(const std::string& filename);
  explicit InfoNodeVisitor();
  ~InfoNodeVisitor();
  void Visit(ExprStatement *stmt) override;
  void Visit(ConstExpr *const_expression) override;
  void Visit(JumpConditionalSt *jump_conditional_statement) override;
  void Visit(MoveStatement *move_statement) override;
  void Visit(SeqStatement *seq_statement) override;
  void Visit(LabelStatement *label_statement) override;
  void Visit(BinopExpr *binop_statement) override;
  void Visit(TempExpr *temp_exression) override;
  void Visit(MemExpr *mem_expression) override;
  void Visit(JumpStatement *jump_statement) override;
  void Visit(CallExpr *call_expression) override;
  void Visit(NameExpr *name_expression) override;
  void Visit(EseqExpr *eseq_expression) override;
  void Visit(ExprList *expression_list) override;

  std::pair<Label, Label> GetLabels(JumpConditionalSt *jump_conditional_statement);

  void Set(ExprStatement *stmt, IRT::IrNodeInfo store);
  void Set(ConstExpr *const_expression, IRT::IrNodeInfo store);
  void Set(JumpConditionalSt *jump_conditional_statement, IRT::IrNodeInfo store);
  void Set(MoveStatement *move_statement, IRT::IrNodeInfo store);
  void Set(SeqStatement *seq_statement, IRT::IrNodeInfo store);
  void Set(LabelStatement *label_statement, IRT::IrNodeInfo store);
  void Set(BinopExpr *binop_statement, IRT::IrNodeInfo store);
  void Set(TempExpr *temp_exression, IRT::IrNodeInfo store);
  void Set(MemExpr *mem_expression, IRT::IrNodeInfo store);
  void Set(JumpStatement *jump_statement, IRT::IrNodeInfo store);
  void Set(CallExpr *call_expression, IRT::IrNodeInfo store);
  void Set(NameExpr *name_expression, IRT::IrNodeInfo store);
  void Set(EseqExpr *eseq_expression, IRT::IrNodeInfo store);
  void Set(ExprList *expression_list, IRT::IrNodeInfo store);

 private:
  std::ofstream stream_;
  int num_tabs_ = 0;

  void PrintTabs();

};

}

