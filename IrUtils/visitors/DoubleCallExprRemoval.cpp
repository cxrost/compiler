//
// Created by akhtyamovpavel on 4/14/20.
//

#include "DoubleCallExprRemoval.h"
#include "IncludeNodes.h"

namespace IRT {

DoubleCallExprRemoval::DoubleCallExprRemoval(const std::string &filename) : stream_(filename) {

}

void DoubleCallExprRemoval::Visit(ExprStatement *stmt) {
  stream_ << "ExpStatement:" << std::endl;
  auto t = Accept(stmt->expr_);
  tos_value_.statement_ = new ExprStatement(t.expression_);

}

void DoubleCallExprRemoval::Visit(ConstExpr *const_expression) {
  stream_ << "ConstExpression " << const_expression->Value() << std::endl;
  tos_value_.expression_ = new ConstExpr(const_expression->Value());
}

void DoubleCallExprRemoval::Visit(JumpConditionalSt *jump_conditional_statement) {
  stream_ << "JumpConditionalStatement: ";
  auto l = Accept(jump_conditional_statement->left_);
  auto r = Accept(jump_conditional_statement->right_);
  tos_value_.statement_ = new JumpConditionalSt(jump_conditional_statement->op_,
                                                l.expression_,
                                                r.expression_,
                                                jump_conditional_statement->l_true_,
                                                jump_conditional_statement->l_false_);
}

void DoubleCallExprRemoval::Visit(MoveStatement *move_statement) {
  stream_ << "MoveStatement:" << std::endl;

  auto l = Accept(move_statement->source_).expression_;
  auto r = Accept(move_statement->target_).expression_;
  tos_value_.statement_ = new MoveStatement(l, r);
}

void DoubleCallExprRemoval::Visit(SeqStatement *seq_statement) {
  stream_ << "SeqStatement:" << std::endl;
  auto first = Accept(seq_statement->first_).statement_;
  auto second = Accept(seq_statement->second_).statement_;
  tos_value_.statement_ = new SeqStatement(first, second);

}
void DoubleCallExprRemoval::Visit(LabelStatement *label_statement) {
  stream_ << "LabelStatement: " << std::endl;
  tos_value_.statement_ = new LabelStatement(label_statement->label_);
}
void DoubleCallExprRemoval::Visit(BinopExpr *binop_statement) {
  stream_ << "BinopExpression: " << ToString(binop_statement->op_) << std::endl;
  auto first = Accept(binop_statement->first_).expression_;
  auto second = Accept(binop_statement->second_).expression_;
  tos_value_.expression_ = new BinopExpr(binop_statement->op_, first, second);
}
void DoubleCallExprRemoval::Visit(TempExpr *temp_exression) {
  stream_ << "TempExpression: " << std::endl;
  tos_value_.expression_ = new TempExpr(temp_exression->temp_);
}

void DoubleCallExprRemoval::Visit(MemExpr *mem_expression) {
  stream_ << "MemExpression: " << std::endl;
  auto addr = Accept(mem_expression->expr_).expression_;
  tos_value_.expression_ = new MemExpr(addr);
}

void DoubleCallExprRemoval::Visit(JumpStatement *jump_statement) {
  stream_ << "JumpStatement: " << std::endl;
  tos_value_.statement_ = new JumpStatement(jump_statement->label_);
}

void DoubleCallExprRemoval::Visit(CallExpr *call_expression) {
  stream_ << "CallExpression: " << std::endl;
  auto func = Accept(call_expression->expr_).expression_;
  auto args = Accept(call_expression->args_).expr_list_;
  Temporary temp;

  tos_value_.expression_ = new EseqExpr(new MoveStatement(
      new TempExpr(temp),
      new CallExpr(
          func,
          args
      )), new TempExpr(temp));
}

void DoubleCallExprRemoval::Visit(ExprList *expression_list) {
  stream_ << "ExpressionList: " << std::endl;
  auto expression_list_ = new ExprList();
  for (auto expression: expression_list->exprs_) {
    expression_list_->Add(Accept(expression).expression_);
  }
  tos_value_.expr_list_ = expression_list_;
}

void DoubleCallExprRemoval::Visit(NameExpr *name_expression) {
  stream_ << "NameExpression: " << std::endl;
  tos_value_.expression_ = new NameExpr(name_expression->label_);
}

void DoubleCallExprRemoval::Visit(EseqExpr *eseq_expression) {
  stream_ << "EseqExpression:" << std::endl;
  auto st = Accept(eseq_expression->st_).statement_;
  auto expr = Accept(eseq_expression->expr_).expression_;
  tos_value_.expression_ = new EseqExpr(st, expr);
}



DoubleCallExprRemoval::~DoubleCallExprRemoval() {
  stream_.close();

}
Statement *DoubleCallExprRemoval::GetIrTree() {
  return tos_value_.statement_;
}

}