//
// Created by pavel on 24.07.2020.
//

#pragma once
#include "IrStorage.h"
#include "../nodes/BaseNode.h"

namespace IRT {

class IrNodeInfo {
 public:
  IrNodeInfo() = default;
  IrNodeInfo(BaseNode::IrType type, IrStorage first, IrStorage second) : type_(type), first_(first), second_(second) {}
  IrStorage first_;
  IrStorage second_;
  BaseNode::IrType type_;
};


}

