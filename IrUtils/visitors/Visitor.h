
#pragma once

#include "forward_IR_decl.h"

namespace IRT {

class Visitor {
 public:
  // Expressions:
  virtual void Visit(ConstExpr *class_decl) = 0;
  virtual void Visit(BinopExpr *class_decl) = 0;
  virtual void Visit(CallExpr *class_decl) = 0;
  virtual void Visit(EseqExpr *class_decl) = 0;
  virtual void Visit(MemExpr *class_decl) = 0;
  virtual void Visit(NameExpr *class_decl) = 0;
  virtual void Visit(TempExpr *class_decl) = 0;

  // Statements
  virtual void Visit(ExprStatement *class_decl) = 0;
  virtual void Visit(JumpConditionalSt *class_decl) = 0;
  virtual void Visit(JumpStatement *class_decl) = 0;
  virtual void Visit(LabelStatement *class_decl) = 0;
  virtual void Visit(MoveStatement *class_decl) = 0;
  virtual void Visit(SeqStatement *class_decl) = 0;

  virtual void Visit(ExprList *class_decl) = 0;

};

}