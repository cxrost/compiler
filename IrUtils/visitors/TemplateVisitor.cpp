#include "TemplateVisitor.h"
#include "IrStorage.h"
#include <list>
#include "assembler/instructions/BaseInstruct.h"
#include "IrNodeInfo.h"

template<typename T>
T IRT::TemplateVisitor<T>::Accept(BaseNode * element) {
  element->Accept(this);
  return tos_value_;
}

template IRT::IrStorage IRT::TemplateVisitor<IRT::IrStorage>::Accept(BaseNode* element);
template IRT::IrNodeInfo IRT::TemplateVisitor<IRT::IrNodeInfo>::Accept(BaseNode* element);
template std::list<ASM::BaseInstruct *> IRT::TemplateVisitor<std::list<ASM::BaseInstruct *>>::Accept(BaseNode* element);