//
// Created by akhtyamovpavel on 4/14/20.
//

#include "EseqRemovalVisitor.h"
#include "IncludeNodes.h"

namespace IRT {

IRT::EseqRemovalVisitor::EseqRemovalVisitor(const std::string &filename)
    : stream_(filename), type_checker(filename + "_type_.out") {

}

void IRT::EseqRemovalVisitor::Visit(ExprStatement *stmt) {
  stream_ << "ExpStatement:" << std::endl;
  auto expr = Accept(stmt->expr_).expression_;
  auto expr_info = type_checker.Accept(expr);
  if (expr_info.type_ == BaseNode::IrType::EseqExpr) {
    tos_value_.statement_ = expr_info.first_.statement_;
    return;
  }
  tos_value_.statement_ = new ExprStatement(expr);
}

void IRT::EseqRemovalVisitor::Visit(ConstExpr *const_expression) {
  stream_ << "ConstExpression " << const_expression->Value() << std::endl;
  tos_value_.expression_ = new ConstExpr(const_expression->Value());
}


void IRT::EseqRemovalVisitor::Visit(JumpConditionalSt *jump_conditional_statement) {
  stream_ << "JumpConditionalStatement: ";
  auto l = Accept(jump_conditional_statement->left_);
  auto r = Accept(jump_conditional_statement->right_);

  auto l_data = type_checker.Accept(jump_conditional_statement->left_);
  auto r_data = type_checker.Accept(jump_conditional_statement->right_);

  if (l_data.type_ != BaseNode::IrType::EseqExpr && r_data.type_ != BaseNode::IrType::EseqExpr) {
    tos_value_.statement_ = new JumpConditionalSt(jump_conditional_statement->op_,
                                                  l.expression_,
                                                  r.expression_,
                                                  jump_conditional_statement->l_true_,
                                                  jump_conditional_statement->l_false_);
    return;
  }

  if (l_data.type_ == BaseNode::IrType::EseqExpr && r_data.type_ != BaseNode::IrType::EseqExpr) {
    tos_value_.statement_ = new SeqStatement(l_data.first_.statement_,
                                             new JumpConditionalSt(jump_conditional_statement->op_,
                                                                   l_data.second_.expression_,
                                                                   r.expression_,
                                                                   jump_conditional_statement->l_true_,
                                                                   jump_conditional_statement->l_false_));
    return;
  }

  if (l_data.type_ != BaseNode::IrType::EseqExpr && r_data.type_ == BaseNode::IrType::EseqExpr) {
    Temporary temp;
    tos_value_.statement_ = new SeqStatement(new MoveStatement(new TempExpr(temp), l.expression_),
                                             new SeqStatement(r_data.first_.statement_,
                                                              new JumpConditionalSt(jump_conditional_statement->op_,
                                                                                    new TempExpr(temp),
                                                                                    r_data.second_.expression_,
                                                                                    jump_conditional_statement->l_true_,
                                                                                    jump_conditional_statement->l_false_)));
    return;
  }
  if (l_data.type_ == BaseNode::IrType::EseqExpr && r_data.type_ == BaseNode::IrType::EseqExpr) {
    Temporary temp;
    tos_value_.statement_ = new SeqStatement(l_data.first_.statement_,
                                             new SeqStatement(new MoveStatement(new TempExpr(temp),
                                                                                l_data.second_.expression_),
                                                              new SeqStatement(r_data.first_.statement_,
                                                                               new JumpConditionalSt(
                                                                                   jump_conditional_statement->op_,
                                                                                   new TempExpr(temp),
                                                                                   r_data.second_.expression_,
                                                                                   jump_conditional_statement->l_true_,
                                                                                   jump_conditional_statement->l_false_))));
    return;
  }
  tos_value_.statement_ = new JumpConditionalSt(jump_conditional_statement->op_,
                                                l.expression_,
                                                r.expression_,
                                                jump_conditional_statement->l_true_,
                                                jump_conditional_statement->l_false_);

}

// TODO - ждем массивов
void IRT::EseqRemovalVisitor::Visit(MoveStatement *move_statement) {
  stream_ << "MoveStatement:" << std::endl;
  auto l = Accept(move_statement->source_).expression_;
  auto r = Accept(move_statement->target_).expression_;

  auto l_data = type_checker.Accept(l);
  auto r_data = type_checker.Accept(r);
  if (l_data.type_ != BaseNode::IrType::EseqExpr && r_data.type_ != BaseNode::IrType::EseqExpr) {
    tos_value_.statement_ = new MoveStatement(l, r);
    return;
  }
  // TODO (test)
  if (l_data.type_ == BaseNode::IrType::TempExpr) {
    // std::cerr << "MoveStatement 2 optimization :now we have a test GJ!" << std::endl;
    tos_value_.statement_ =
        new SeqStatement(r_data.first_.statement_,
                         new MoveStatement(l, r_data.second_.expression_));
    return;
  }

  if (l_data.type_ == BaseNode::IrType::MemExpr) {
    Temporary temp;
    auto expr = l_data.first_.expression_;
    tos_value_.statement_ =
        new SeqStatement(new MoveStatement(new TempExpr(temp), expr),
                         new SeqStatement(r_data.first_.statement_,
                                          new MoveStatement(new MemExpr(new TempExpr(temp)),
                                                            r_data.second_.expression_))
        );
    return;
  }
  // TODO : добавить слева ESEQ
  std::cerr << "MoveStatement 2 optimization: alarm" << std::endl;
  tos_value_.statement_ = new MoveStatement(l, r);
}

void IRT::EseqRemovalVisitor::Visit(SeqStatement *seq_statement) {
  stream_ << "SeqStatement:" << std::endl;
  auto first = Accept(seq_statement->first_).statement_;
  auto second = Accept(seq_statement->second_).statement_;
  tos_value_.statement_ = new SeqStatement(first, second);
}

void IRT::EseqRemovalVisitor::Visit(LabelStatement *label_statement) {
  stream_ << "LabelStatement: " << std::endl;
  tos_value_.statement_ = new LabelStatement(label_statement->label_);
}

void IRT::EseqRemovalVisitor::Visit(BinopExpr *binop_statement) {
  stream_ << "BinopExpression: " << std::endl;
  auto first = Accept(binop_statement->first_).expression_;
  auto second = Accept(binop_statement->second_).expression_;

  auto first_data = type_checker.Accept(first);
  auto second_data = type_checker.Accept(second);
  if (first_data.type_ != BaseNode::IrType::EseqExpr && second_data.type_ != BaseNode::IrType::EseqExpr) {
    tos_value_.expression_ = new BinopExpr(binop_statement->op_, first, second);
    return;
  }

  if (first_data.type_ == BaseNode::IrType::EseqExpr && second_data.type_ != BaseNode::IrType::EseqExpr) {
    tos_value_.expression_ = new EseqExpr(first_data.first_.statement_,
                                          new BinopExpr(binop_statement->op_,
                                                        first_data.second_.expression_, second));
    return;
  }
  if (first_data.type_ != BaseNode::IrType::EseqExpr && second_data.type_ == BaseNode::IrType::EseqExpr) {
    Temporary temp;
    tos_value_.expression_ =
        new EseqExpr(new SeqStatement(new MoveStatement(new TempExpr(temp), first), second_data.first_.statement_),
                     new BinopExpr(binop_statement->op_,
                                   new TempExpr(temp),
                                   second_data.second_.expression_));
    return;
  }
  Temporary temp;
  tos_value_.expression_ =
      new EseqExpr(new SeqStatement(first_data.first_.statement_,
                                    new SeqStatement(new MoveStatement(new TempExpr(temp),
                                                                       first_data.second_.expression_),
                                                     second_data.first_.statement_)),
                   new BinopExpr(binop_statement->op_, new TempExpr(temp), second_data.second_.expression_));

}
void IRT::EseqRemovalVisitor::Visit(TempExpr *temp_exression) {
  stream_ << "TempExpression: " << std::endl;
  tos_value_.expression_ = new TempExpr(temp_exression->temp_);
}

// TODO (test) - ждем массивов
void IRT::EseqRemovalVisitor::Visit(MemExpr *mem_expression) {
  stream_ << "MemExpression: " << std::endl;
  auto addr = Accept(mem_expression->expr_).expression_;
  auto addr_data = type_checker.Accept(addr);
  if (addr_data.type_ == BaseNode::IrType::EseqExpr) {
    std::cerr << "MemExpr -> Eseq Found" << std::endl;
    tos_value_.expression_ =
        new EseqExpr(addr_data.first_.statement_, new MemExpr(addr_data.second_.expression_));
    return;
  }

  tos_value_.expression_ = new MemExpr(addr);
}

void IRT::EseqRemovalVisitor::Visit(JumpStatement *jump_statement) {
  stream_ << "JumpStatement: " << std::endl;
  tos_value_.statement_ = new JumpStatement(jump_statement->label_);
}


void IRT::EseqRemovalVisitor::Visit(CallExpr *call_expression) {
  stream_ << "CallExpression: " << std::endl;
  auto func = Accept(call_expression->expr_).expression_;
  auto args = Accept(call_expression->args_).expr_list_;
  if (args != nullptr && !args->exprs_.empty()) {
    auto arg_data = type_checker.Accept(args->exprs_[0]);
    if (arg_data.type_ == BaseNode::IrType::EseqExpr) {
      args->exprs_[0] = arg_data.second_.expression_;
      tos_value_.expression_ = new EseqExpr(arg_data.first_.statement_, new CallExpr(func, args));
    } else {
      tos_value_.expression_ = new CallExpr(func, args);
    }
    return;
  }

  tos_value_.expression_ = new CallExpr(func, args);
}


void IRT::EseqRemovalVisitor::Visit(ExprList *expression_list) {
  stream_ << "ExpressionList: " << std::endl;
  auto expression_list_ = new ExprList();
  for (auto expression: expression_list->exprs_) {
    expression_list_->Add(Accept(expression).expression_);
  }
  if (expression_list_->exprs_.size() <= 1) {
    tos_value_.expr_list_ = expression_list_;
    return;
  }
  auto last_expression_list_ = new ExprList();
  SeqStatement *all_st_tree;
  SeqStatement *lowest;
  for (int i = 0; i < expression_list_->exprs_.size(); ++i) {
    SeqStatement *t_st;
    Temporary temp;
    auto t_data = type_checker.Accept(expression_list_->exprs_[i]);
    t_st = new SeqStatement();
    if (t_data.type_ == BaseNode::IrType::EseqExpr) {
      t_st->first_ =
          new SeqStatement(t_data.first_.statement_, new MoveStatement(new TempExpr(temp), t_data.second_.expression_));
    } else {
      t_st->first_ = new MoveStatement(new TempExpr(temp), expression_list_->exprs_[i]);
    }
    if (i == 0) {
      all_st_tree = t_st;
      lowest = t_st;
      last_expression_list_->Add(new EseqExpr(all_st_tree, new TempExpr(temp)));
    } else if (i == expression_list_->exprs_.size() - 1) {
      lowest->second_ = t_st->first_;
      last_expression_list_->Add(new TempExpr(temp));
    } else {
      lowest->second_ = t_st;
      lowest = t_st;
      last_expression_list_->Add(new TempExpr(temp));
    }
  }
  tos_value_.expr_list_ = last_expression_list_;
}

void IRT::EseqRemovalVisitor::Visit(NameExpr *name_expression) {
  stream_ << "NameExpression: " << std::endl;
  tos_value_.expression_ = new NameExpr(name_expression->label_);
}

// TODO (test)- хз вообще где
void IRT::EseqRemovalVisitor::Visit(EseqExpr *eseq_expression) {
  stream_ << "EseqExpression:" << std::endl;
  auto st = Accept(eseq_expression->st_).statement_;
  auto expr = Accept(eseq_expression->expr_).expression_;
  auto expr_data = type_checker.Accept(expr);
  if (expr_data.type_ == BaseNode::IrType::EseqExpr) {
    std::cerr << "Eseq -> Eseq Found" << std::endl;
    tos_value_.expression_ =
        new EseqExpr(new SeqStatement(st, expr_data.first_.statement_), expr_data.second_.expression_);
    return;
  }
  tos_value_.expression_ = new EseqExpr(st, expr);
}

IRT::EseqRemovalVisitor::~EseqRemovalVisitor() {
  stream_.close();
}

Statement *EseqRemovalVisitor::GetIrTree() {
  return tos_value_.statement_;
}

}