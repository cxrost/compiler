
namespace IRT {
// Expressions
class BaseNode;
class ConstExpr;
class BinopExpr;
class CallExpr;
class EseqExpr;
class MemExpr;
class NameExpr;
class TempExpr;

// Statements
class ExprStatement;
class JumpConditionalSt;
class JumpStatement;
class LabelStatement;
class MoveStatement;
class SeqStatement;


class ExprList;

}
