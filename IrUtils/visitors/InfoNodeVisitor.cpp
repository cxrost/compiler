//
// Created by akhtyamovpavel on 4/14/20.
//

#include "InfoNodeVisitor.h"
#include "IncludeNodes.h"

namespace IRT {

InfoNodeVisitor::InfoNodeVisitor(const std::string &filename) : stream_(filename) {

}

InfoNodeVisitor::InfoNodeVisitor() {

}

void InfoNodeVisitor::Visit(ExprStatement *stmt) {
  // stream_ << "ExpStatement:" << std::endl;
  tos_value_ = IrNodeInfo(BaseNode::IrType::ExprStatement, IrStorage(stmt->expr_), IrStorage());
}

void InfoNodeVisitor::Visit(ConstExpr *const_expression) {
  // stream_ << "ConstExpression " << const_expression->Value() << std::endl;
  tos_value_ = IrNodeInfo(BaseNode::IrType::ConstExpr, IrStorage(), IrStorage());
}

void InfoNodeVisitor::Visit(JumpConditionalSt *jump_conditional_statement) {
  // stream_ << "JumpConditionalStatement: ";
  tos_value_ = IrNodeInfo(BaseNode::IrType::JumpConditionalSt,
                          IrStorage(jump_conditional_statement->left_),
                          IrStorage(jump_conditional_statement->right_));
}

void InfoNodeVisitor::Visit(MoveStatement *move_statement) {
  // stream_ << "MoveStatement:" << std::endl;
  tos_value_ = IrNodeInfo(BaseNode::IrType::MoveStatement,
                          IrStorage(move_statement->source_),
                          IrStorage(move_statement->target_));
}

void InfoNodeVisitor::Visit(SeqStatement *seq_statement) {
  // stream_ << "SeqStatement:" << std::endl;
  tos_value_ =
      IrNodeInfo(BaseNode::IrType::SeqStatement, IrStorage(seq_statement->first_), IrStorage(seq_statement->second_));
}
void InfoNodeVisitor::Visit(LabelStatement *label_statement) {
  // stream_ << "LabelStatement: " << std::endl;
  tos_value_ = IrNodeInfo(BaseNode::IrType::LabelStatement, IrStorage(), IrStorage());
}
void InfoNodeVisitor::Visit(BinopExpr *binop_statement) {
  // stream_ << "BinopExpression: " << std::endl;
  tos_value_ =
      IrNodeInfo(BaseNode::IrType::BinopExpr, IrStorage(binop_statement->first_), IrStorage(binop_statement->second_));
}
void InfoNodeVisitor::Visit(TempExpr *temp_exression) {
  // stream_ << "TempExpression: " << std::endl;
  tos_value_ = IrNodeInfo(BaseNode::IrType::TempExpr, IrStorage(), IrStorage());
}

void InfoNodeVisitor::Visit(MemExpr *mem_expression) {
  // stream_ << "MemExpression: " << std::endl;
  tos_value_ = IrNodeInfo(BaseNode::IrType::MemExpr, IrStorage(mem_expression->expr_), IrStorage());
}

void InfoNodeVisitor::Visit(JumpStatement *jump_statement) {
  // stream_ << "JumpStatement: " << std::endl;
  tos_value_ = IrNodeInfo(BaseNode::IrType::JumpStatement,
                          IrStorage(new LabelStatement(Label(jump_statement->label_))),
                          IrStorage());
}

void InfoNodeVisitor::Visit(CallExpr *call_expression) {
  // stream_ << "CallExpression: " << std::endl;
  tos_value_ =
      IrNodeInfo(BaseNode::IrType::CallExpr, IrStorage(call_expression->expr_), IrStorage(call_expression->args_));
}

void InfoNodeVisitor::Visit(ExprList *expression_list) {
  // stream_ << "ExpressionList: " << std::endl;
  tos_value_ = IrNodeInfo(BaseNode::IrType::ExprList, IrStorage(), IrStorage());
}

void InfoNodeVisitor::Visit(NameExpr *name_expression) {
  // stream_ << "NameExpression: " << std::endl;
  tos_value_ = IrNodeInfo(BaseNode::IrType::NameExpr, IrStorage(), IrStorage());

}

void InfoNodeVisitor::Visit(EseqExpr *eseq_expression) {
  // stream_ << "EseqExpression:" << std::endl;
  tos_value_ =
      IrNodeInfo(BaseNode::IrType::EseqExpr, IrStorage(eseq_expression->st_), IrStorage(eseq_expression->expr_));
}

InfoNodeVisitor::~InfoNodeVisitor() {
  // stream_.close();
}
void InfoNodeVisitor::Set(ExprStatement *stmt, IRT::IrNodeInfo store) {
  stmt->expr_ = store.first_.expression_;
}
void InfoNodeVisitor::Set(ConstExpr *const_expression, IRT::IrNodeInfo store) {
  std::cerr << "Bad Setting Ir node";
}
void InfoNodeVisitor::Set(JumpConditionalSt *jump_conditional_statement, IRT::IrNodeInfo store) {
  jump_conditional_statement->left_ = store.first_.expression_;
  jump_conditional_statement->right_ = store.second_.expression_;
}
void InfoNodeVisitor::Set(MoveStatement *move_statement, IRT::IrNodeInfo store) {
  move_statement->source_ = store.first_.expression_;
  move_statement->target_ = store.second_.expression_;
}
void InfoNodeVisitor::Set(SeqStatement *seq_statement, IRT::IrNodeInfo store) {
  seq_statement->first_ = store.first_.statement_;
  seq_statement->second_ = store.second_.statement_;
}
void InfoNodeVisitor::Set(LabelStatement *label_statement, IRT::IrNodeInfo store) {
  std::cerr << "Bad Setting Ir node";
}
void InfoNodeVisitor::Set(BinopExpr *binop_statement, IRT::IrNodeInfo store) {
  binop_statement->first_ = store.first_.expression_;
  binop_statement->second_ = store.second_.expression_;
}
void InfoNodeVisitor::Set(TempExpr *temp_exression, IRT::IrNodeInfo store) {
  std::cerr << "Bad Setting Ir node";
}
void InfoNodeVisitor::Set(MemExpr *mem_expression, IRT::IrNodeInfo store) {
  mem_expression->expr_ = store.first_.expression_;
}
void InfoNodeVisitor::Set(JumpStatement *jump_statement, IRT::IrNodeInfo store) {
  std::cerr << "Bad Setting Ir node";
}
void InfoNodeVisitor::Set(CallExpr *call_expression, IRT::IrNodeInfo store) {
  call_expression->expr_ = store.first_.expression_;
  call_expression->args_ = store.second_.expr_list_;
}
void InfoNodeVisitor::Set(NameExpr *name_expression, IRT::IrNodeInfo store) {
  std::cerr << "Bad Setting Ir node";
}
void InfoNodeVisitor::Set(EseqExpr *eseq_expression, IRT::IrNodeInfo store) {
  eseq_expression->st_ = store.first_.statement_;
  eseq_expression->expr_ = store.second_.expression_;
}
void InfoNodeVisitor::Set(ExprList *expression_list, IRT::IrNodeInfo store) {

}
std::pair<Label, Label> InfoNodeVisitor::GetLabels(JumpConditionalSt *jump_conditional_statement) {
  return std::make_pair(jump_conditional_statement->l_true_, jump_conditional_statement->l_false_);
}

}