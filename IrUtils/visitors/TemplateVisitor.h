#pragma once

#include <sstream>
#include "IncludeNodes.h"

namespace IRT {

template<typename T>
class TemplateVisitor : public IRT::Visitor {
 public:
  T Accept(BaseNode *element);

 protected:
  T tos_value_;

};

}