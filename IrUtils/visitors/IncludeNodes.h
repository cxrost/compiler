#pragma once

// Expressions
#include "IrUtils/nodes/expressions/ConstExpr.h"
#include "IrUtils/nodes/expressions/BinopExpr.h"
#include "IrUtils/nodes/expressions/CallExpr.h"
#include "IrUtils/nodes/expressions/EseqExpr.h"
#include "IrUtils/nodes/expressions/MemExpr.h"
#include "IrUtils/nodes/expressions/NameExpr.h"
#include "IrUtils/nodes/expressions/TempExpr.h"


// Statements
#include "IrUtils/nodes/statements/ExprStatement.h"
#include "IrUtils/nodes/statements/JumpConditionalSt.h"
#include "IrUtils/nodes/statements/JumpStatement.h"
#include "IrUtils/nodes/statements/LabelStatement.h"
#include "IrUtils/nodes/statements/MoveStatement.h"
#include "IrUtils/nodes/statements/SeqStatement.h"
#include "IrUtils/nodes/statements/Statement.h"


#include "IrUtils/nodes/ExprList.h"