//
// Created by akhtyamovpavel on 4/14/20.
//

#include <grammar/expressions/ExpressionList.h>
#include "PrintVisitor.h"
#include "IncludeNodes.h"

namespace IRT {

PrintVisitor::PrintVisitor(const std::string &filename) {
  stream_.open(filename);
  stream_.close();
  stream_.open(filename, std::fstream::app);

  stream_ << "\n #### BlockVisitor #### \n" << std::endl;
}

void PrintVisitor::Visit(ExprStatement *stmt) {
  PrintTabs();
  stream_ << "ExpStatement:" << std::endl;

  ++num_tabs_;
  stmt->expr_->Accept(this);
  --num_tabs_;

}

void PrintVisitor::Visit(ConstExpr *const_expression) {
  PrintTabs();
  stream_ << "ConstExpression " << const_expression->Value() << std::endl;

}

void PrintVisitor::Visit(JumpConditionalSt *jump_conditional_statement) {
  PrintTabs();
  stream_ << "JumpConditionalStatement: " <<
          ToString(jump_conditional_statement->op_)<< std::endl;
  PrintTabs();
  stream_ << "TrueLabel: " << jump_conditional_statement->l_true_.ToString() << std::endl;
  PrintTabs();
  stream_ << "FalseLabel: " << jump_conditional_statement->l_false_.ToString() << std::endl;
  ++num_tabs_;
  jump_conditional_statement->left_->Accept(this);
  jump_conditional_statement->right_->Accept(this);
  --num_tabs_;
}

void PrintVisitor::Visit(MoveStatement *move_statement) {
  PrintTabs();
  stream_ << "MoveStatement:" << std::endl;
  ++num_tabs_;
  move_statement->source_->Accept(this);
  move_statement->target_->Accept(this);
  --num_tabs_;
}

void PrintVisitor::Visit(SeqStatement *seq_statement) {
  PrintTabs();
  stream_ << "SeqStatement:" << std::endl;
  ++num_tabs_;
  seq_statement->first_->Accept(this);
  seq_statement->second_->Accept(this);
  --num_tabs_;

}
void PrintVisitor::Visit(LabelStatement *label_statement) {
  PrintTabs();
  stream_ << "LabelStatement: " << label_statement->label_.ToString() << std::endl;
}
void PrintVisitor::Visit(BinopExpr *binop_statement) {
  PrintTabs();
  stream_ << "BinopExpression: " << ToString(binop_statement->op_) << std::endl;
  ++num_tabs_;
  binop_statement->first_->Accept(this);
  binop_statement->second_->Accept(this);
  --num_tabs_;
}
void PrintVisitor::Visit(TempExpr *temp_exression) {
  PrintTabs();
  stream_ << "TempExpression: " << temp_exression->temp_.ToString() << std::endl;
}

void PrintVisitor::Visit(MemExpr *mem_expression) {
  PrintTabs();
  stream_ << "MemExpression: " << std::endl;
  ++num_tabs_;
  mem_expression->expr_->Accept(this);
  --num_tabs_;
}

void PrintVisitor::Visit(JumpStatement *jump_statement) {
  PrintTabs();
  stream_ << "JumpStatement: " << jump_statement->label_.ToString() << std::endl;
}

void PrintVisitor::Visit(CallExpr *call_expression) {
  PrintTabs();
  stream_ << "CallExpression: " << std::endl;
  ++num_tabs_;
  call_expression->expr_->Accept(this);
  call_expression->args_->Accept(this);
  --num_tabs_;
}

void PrintVisitor::Visit(ExprList *expression_list) {
  PrintTabs();
  stream_ << "ExpressionList: " << std::endl;
  ++num_tabs_;
  for (auto expression: expression_list->exprs_) {
    expression->Accept(this);
  }
  --num_tabs_;
}

void PrintVisitor::Visit(NameExpr *name_expression) {
  PrintTabs();
  stream_ << "NameExpression: " << name_expression->label_.ToString() << std::endl;
}

PrintVisitor::~PrintVisitor() {
  stream_.close();

}
void PrintVisitor::PrintTabs() {
  for (int i = 0; i < num_tabs_; ++i) {
    stream_ << '\t';
  }
}
void PrintVisitor::Visit(EseqExpr *eseq_expression) {
  PrintTabs();
  stream_ << "EseqExpression:" << std::endl;
  ++num_tabs_;
  eseq_expression->st_->Accept(this);
  eseq_expression->expr_->Accept(this);
  --num_tabs_;

}
void PrintVisitor::PrintString(std::string str) {
  stream_ << "\n #### " + str + " #### \n" << std::endl;
}
}