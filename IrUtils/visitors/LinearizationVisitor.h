//
// Created by akhtyamovpavel on 4/14/20.
//


#pragma once

#include <string>
#include <unordered_map>
#include <fstream>
#include "TemplateVisitor.h"
#include "IrStorage.h"
#include "InfoNodeVisitor.h"
namespace IRT {
class LinearizationVisitor: public TemplateVisitor<IRT::IrStorage> {

 public:
  explicit LinearizationVisitor(const std::string& filename);
  ~LinearizationVisitor();
  void Visit(ExprStatement *stmt) override;
  void Visit(ConstExpr *const_expression) override;
  void Visit(JumpConditionalSt *jump_conditional_statement) override;
  void Visit(MoveStatement *move_statement) override;
  void Visit(SeqStatement *seq_statement) override;
  void Visit(LabelStatement *label_statement) override;
  void Visit(BinopExpr *binop_statement) override;
  void Visit(TempExpr *temp_exression) override;
  void Visit(MemExpr *mem_expression) override;
  void Visit(JumpStatement *jump_statement) override;
  void Visit(CallExpr *call_expression) override;
  void Visit(NameExpr *name_expression) override;
  void Visit(EseqExpr *eseq_expression) override;
  void Visit(ExprList *expression_list) override;


  Statement* GetIrTree();
  std::unordered_map<std::string, std::vector<Statement*>> GetBlocks(Statement* start);
 private:
  std::ofstream stream_;
  int num_tabs_ = 0;
  Statement* tos_last_seq;
  InfoNodeVisitor type_checker;
  void PrintTabs();

};

}

