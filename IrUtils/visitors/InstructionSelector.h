//
// Created by akhtyamovpavel on 4/14/20.
//


#pragma once

#include <string>
#include <fstream>
#include <unordered_map>
#include <list>
#include <assembler/visitors/PrintVisitor.h>
#include "TemplateVisitor.h"
#include "IrStorage.h"
#include "assembler/visitors/ASMincludes.h"
#include "InfoNodeVisitor.h"
namespace IRT {
class InstructionSelector: public TemplateVisitor<std::list<ASM::BaseInstruct *>> {

 public:
  explicit InstructionSelector(const std::string& filename);
  ~InstructionSelector();
  void Visit(ExprStatement *stmt) override;
  void Visit(ConstExpr *const_expression) override;
  void Visit(JumpConditionalSt *jump_conditional_statement) override;
  void Visit(MoveStatement *move_statement) override;
  void Visit(SeqStatement *seq_statement) override;
  void Visit(LabelStatement *label_statement) override;
  void Visit(BinopExpr *binop_statement) override;
  void Visit(TempExpr *temp_exression) override;
  void Visit(MemExpr *mem_expression) override;
  void Visit(JumpStatement *jump_statement) override;
  void Visit(CallExpr *call_expression) override;
  void Visit(NameExpr *name_expression) override;
  void Visit(EseqExpr *eseq_expression) override;
  void Visit(ExprList *expression_list) override;


  std::vector<ASM::BaseInstruct *> GetInstructions(std::unordered_map<std::string, std::vector<Statement *>> traces);
 private:
  std::ofstream stream_;
  int num_tabs_ = 0;
  ASM::PrintVisitor printer;
  InfoNodeVisitor type_checker_;
  std::unordered_map<std::string, std::vector<Statement *>> traces_;
  Temporary res_prev_instruction_;
  void PrintTabs();
};

}

