#pragma once

#include "Statement.h"
#include "../expressions/Expression.h"

namespace IRT {

class MoveStatement : public IRT::Statement {
 public:
  explicit MoveStatement(IRT::Expression* source, IRT::Expression* target);
  ~MoveStatement() override = default;

  void Accept(IRT::Visitor *visitor) override;
  IRT::Expression* source_;
  IRT::Expression* target_;
};

}