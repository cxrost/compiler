#pragma once

#include "Statement.h"
#include "../expressions/Expression.h"
#include "../operators/LogicOperator.h"
#include "../../utils/Label.h"

namespace IRT {

class JumpConditionalSt : public IRT::Statement {
 public:
  explicit JumpConditionalSt(LogicOperator op, IRT::Expression *left, IRT::Expression *right, Label l_true, Label l_false);
  ~JumpConditionalSt() override = default;
  void InverseOp();
  void InverseLabels();
  void Accept(IRT::Visitor *visitor) override;

  IRT::LogicOperator op_;
  IRT::Expression *left_;
  IRT::Expression *right_;
  Label l_true_;
  Label l_false_;

};

}