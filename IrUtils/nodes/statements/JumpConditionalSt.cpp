//
// Created by pavel on 21.07.2020.
//

#include "JumpConditionalSt.h"

void IRT::JumpConditionalSt::Accept(IRT::Visitor *visitor) {
  visitor->Visit(this);
}

IRT::JumpConditionalSt::JumpConditionalSt(IRT::LogicOperator op,
                                          IRT::Expression *left,
                                          IRT::Expression *right,
                                          IRT::Label l_true,
                                          IRT::Label l_false)
    : op_(op), left_(left), right_(right), l_true_(l_true), l_false_(l_false) {

}
void IRT::JumpConditionalSt::InverseOp() {
  auto inversed_op = Inverse(op_);
  op_ = inversed_op;
}
void IRT::JumpConditionalSt::InverseLabels() {
  std::swap(l_true_, l_false_);
}

