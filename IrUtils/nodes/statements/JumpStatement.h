#pragma once

#include "Statement.h"
#include "../../utils/Label.h"

namespace IRT {

class JumpStatement : public IRT::Statement {
 public:
  explicit JumpStatement(Label label);
  ~JumpStatement() override = default;

  void Accept(IRT::Visitor *visitor) override;
  Label label_;
};

}