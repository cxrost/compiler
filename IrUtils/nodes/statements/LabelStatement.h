#pragma once

#include "Statement.h"
#include "../../utils/Label.h"

namespace IRT {

class LabelStatement : public IRT::Statement {
 public:
  explicit LabelStatement(Label label);
  ~LabelStatement() override = default;

  void Accept(IRT::Visitor *visitor) override;
  Label label_;
};

}