#pragma once

#include "Statement.h"
#include "../expressions/Expression.h"

namespace IRT {

class SeqStatement : public IRT::Statement {
 public:
   SeqStatement(IRT::Statement* first, IRT::Statement* second);
   explicit SeqStatement() = default;
  ~SeqStatement() override = default;

  void Accept(IRT::Visitor *visitor) override;
  IRT::Statement* first_{nullptr};
  IRT::Statement* second_{nullptr};
};

}