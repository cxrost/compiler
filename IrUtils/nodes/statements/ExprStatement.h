#pragma once

#include "Statement.h"
#include "../expressions/Expression.h"

namespace IRT {

class ExprStatement : public IRT::Statement {
 public:
  explicit ExprStatement(IRT::Expression* expr);
  ~ExprStatement() override = default;

  void Accept(IRT::Visitor *visitor) override;
  IRT::Expression* expr_;
};

}