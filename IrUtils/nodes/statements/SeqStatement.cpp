//
// Created by pavel on 21.07.2020.
//

#include "SeqStatement.h"

void IRT::SeqStatement::Accept(IRT::Visitor *visitor) {
  visitor->Visit(this);
}

IRT::SeqStatement::SeqStatement(IRT::Statement *first, IRT::Statement *second) : first_(first), second_(second) {

}
