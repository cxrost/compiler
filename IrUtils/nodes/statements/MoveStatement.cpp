//
// Created by pavel on 21.07.2020.
//

#include "MoveStatement.h"
#include "MoveStatement.h"

void IRT::MoveStatement::Accept(IRT::Visitor *visitor) {
  visitor->Visit(this);
}
IRT::MoveStatement::MoveStatement(IRT::Expression *source, IRT::Expression *target) : source_(source), target_(target) {

}
