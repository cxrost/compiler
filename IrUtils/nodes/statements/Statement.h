#pragma once

#include "IrUtils/nodes/BaseNode.h"

namespace IRT {

class Statement : public IRT::BaseNode {
 public:
  ~Statement() override = default;
};

}


