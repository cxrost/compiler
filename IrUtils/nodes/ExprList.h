#pragma once

#include "expressions/Expression.h"
#include "vector"
namespace IRT {

class ExprList : public BaseNode {
 public:
  ExprList() = default;
  ~ExprList() override = default;
  void Add(IRT::Expression* expr);
  void Accept(IRT::Visitor *visitor) override;
  std::vector<IRT::Expression*> exprs_;
};

}