//
// Created by pavel on 21.07.2020.
//

#include "ConstExpr.h"
IRT::ConstExpr::ConstExpr(int value) : value_(value) {

}
int IRT::ConstExpr::Value() const {
  return value_;
}
void IRT::ConstExpr::Accept(IRT::Visitor *visitor) {
  visitor->Visit(this);
}
