//
// Created by pavel on 21.07.2020.
//

#include "BinopExpr.h"
void IRT::BinopExpr::Accept(IRT::Visitor *visitor) {
  visitor->Visit(this);
}
IRT::BinopExpr::BinopExpr(IRT::BinOperator op, IRT::Expression *first, IRT::Expression *second)
    : op_(op), first_(first), second_(second) {
}