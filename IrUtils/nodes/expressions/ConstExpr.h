#pragma once

#include "Expression.h"

namespace IRT {

 class ConstExpr : public IRT::Expression {
 public:
  explicit ConstExpr(int value);
  int Value() const;

  void Accept(IRT::Visitor *visitor) override;
 private:
  int value_;
};

}