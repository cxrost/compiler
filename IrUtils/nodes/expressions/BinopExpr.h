#pragma once

#include "Expression.h"
#include "IrUtils/nodes/operators/BinOperator.h"

namespace IRT {

class BinopExpr : public IRT::Expression {
 public:
  explicit BinopExpr(IRT::BinOperator op, IRT::Expression* first, IRT::Expression* second);
  ~BinopExpr() override = default;

  void Accept(IRT::Visitor *visitor) override;
  IRT::BinOperator op_;
  IRT::Expression* first_;
  IRT::Expression* second_;
};

}