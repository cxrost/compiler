#pragma once

#include "IrUtils/nodes/BaseNode.h"

namespace IRT {

 class Expression : public IRT::BaseNode {
 public:
  ~Expression() override = default;
};

}


