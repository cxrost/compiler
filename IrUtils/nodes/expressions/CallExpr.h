#pragma once

#include "Expression.h"
#include "../ExprList.h"

namespace IRT {

class CallExpr : public IRT::Expression {
 public:
  explicit CallExpr(IRT::Expression* expr, IRT::ExprList* args);
  ~CallExpr() override = default;

  void Accept(IRT::Visitor *visitor) override;
  IRT::Expression* expr_;
  IRT::ExprList* args_;
};

}