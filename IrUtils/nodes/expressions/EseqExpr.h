#pragma once

#include "Expression.h"
#include "../statements/Statement.h"

namespace IRT {

class EseqExpr : public IRT::Expression {
 public:
  explicit EseqExpr(IRT::Statement* st, IRT::Expression* expr);
  ~EseqExpr() override = default;

  void Accept(IRT::Visitor *visitor) override;
  IRT::Statement* st_;
  IRT::Expression* expr_;
};

}