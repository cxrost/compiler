#pragma once

#include "Expression.h"
#include "IrUtils/utils/Temporary.h"

namespace IRT {

class TempExpr : public IRT::Expression {
 public:
  explicit TempExpr(Temporary temp);
  ~TempExpr() override = default;

  void Accept(IRT::Visitor *visitor) override;
  Temporary temp_;
};

}