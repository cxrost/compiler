#pragma once

#include "Expression.h"

namespace IRT {

class MemExpr : public IRT::Expression {
 public:
  explicit MemExpr(IRT::Expression* expr);
  ~MemExpr() override = default;

  void Accept(IRT::Visitor *visitor) override;
  IRT::Expression* expr_;
};

}