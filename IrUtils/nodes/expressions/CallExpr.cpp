//
// Created by pavel on 21.07.2020.
//

#include "CallExpr.h"
void IRT::CallExpr::Accept(IRT::Visitor *visitor) {
  visitor->Visit(this);
}
IRT::CallExpr::CallExpr(IRT::Expression *expr, IRT::ExprList *args) : expr_(expr), args_(args) {

}