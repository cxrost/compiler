#pragma once

#include "Expression.h"
#include "IrUtils/utils/Label.h"

namespace IRT {

class NameExpr : public IRT::Expression {
 public:
  explicit NameExpr(IRT::Label label);
  ~NameExpr() override = default;

  void Accept(IRT::Visitor *visitor) override;
  IRT::Label label_;
};

}