//
// Created by pavel on 21.07.2020.
//

#include "BinOperator.h"

std::string IRT::ToString(BinOperator op) {
  switch (op) {
    case BinOperator::PLUS:
      return "PLUS";
    case BinOperator::MINUS:
      return "MINUS";
    case BinOperator::MUL:
      return "MUL";
    case BinOperator::DIV:
      return "DIV";
    case BinOperator::AND:
      return "AND";
    case BinOperator::OR:
      return "OR";
  }
}