#pragma once
#include <string>

namespace IRT {

enum class LogicOperator {
  LT,
  GT,
  NE,
  EQ,
};

std::string ToString(LogicOperator op);

LogicOperator Inverse(LogicOperator op);

}

