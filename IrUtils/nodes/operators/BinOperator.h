#pragma once
#include <string>
namespace IRT {
enum class BinOperator {
  PLUS,
  MINUS,
  MUL,
  DIV,
  AND,
  OR
};

std::string ToString(BinOperator op);

}
