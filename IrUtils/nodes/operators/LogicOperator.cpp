//
// Created by pavel on 21.07.2020.
//

#include "LogicOperator.h"
std::string IRT::ToString(LogicOperator op) {
  switch (op) {
    case LogicOperator::EQ:return "EQ";
    case LogicOperator::NE:return "NE";
    case LogicOperator::LT:return "LT";
    case LogicOperator::GT:return "GT";
  }
}

IRT::LogicOperator IRT::Inverse(IRT::LogicOperator op) {
  switch (op) {
    case LogicOperator::LT:return LogicOperator::GT;
    case LogicOperator::GT:return LogicOperator::LT;
    case LogicOperator::NE:return LogicOperator::EQ;
    case LogicOperator::EQ:return LogicOperator::NE;
  }
}