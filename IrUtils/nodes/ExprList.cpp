//
// Created by pavel on 21.07.2020.
//

#include "ExprList.h"

void IRT::ExprList::Accept(IRT::Visitor *visitor) {
  visitor->Visit(this);
}
void IRT::ExprList::Add(IRT::Expression *expr) {
  exprs_.emplace_back(expr);
}

