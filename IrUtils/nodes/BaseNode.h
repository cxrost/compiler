#pragma once
#include <iostream>
#include "IrUtils/visitors/Visitor.h"

namespace IRT {

class BaseNode {
 public:
  enum class IrType {
    ConstExpr,
    BinopExpr,
    CallExpr,
    EseqExpr,
    MemExpr,
    NameExpr,
    TempExpr,
    ExprStatement,
    JumpConditionalSt,
    JumpStatement,
    LabelStatement,
    MoveStatement,
    SeqStatement,
    ExprList,
  };

  virtual void Accept(IRT::Visitor* visitor) = 0;
  virtual ~BaseNode() = default;
};


}