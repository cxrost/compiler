//
// Created by pavel on 01.08.2020.
//

#include <unordered_set>
#include <deque>
#include "IrUtils/nodes/statements/Statement.h"
#include "TraceWrapper.h"

void IRT::TraceWrapper::UniteTracesAsDefault(std::string func) {
  std::unordered_set<std::string> used_blocks;
  std::deque<std::string> next_blocks;
  next_blocks.push_back(func);
  while (!next_blocks.empty()) {
    auto start_block = next_blocks.front();
    next_blocks.pop_front();
    if (start_block == "done" || used_blocks.find(start_block) != used_blocks.end()) {
      continue;
    }
    bool free_next_block = true;
    traces_[start_block];
    auto next_block_name = start_block;
    while (free_next_block) {
      used_blocks.insert(next_block_name);
      auto next_block = blocks_[next_block_name];

      for (auto elem : next_block) {
        traces_[start_block].emplace_back(elem);
      }
      if (next_block_name == "done") {
        break;
      }

      auto jump_st = next_block[next_block.size() - 1];
      auto jump_data = type_checker_.Accept(jump_st);
      if (jump_data.type_ == BaseNode::IrType::JumpStatement) {
        auto next_label = dynamic_cast<LabelStatement *>(jump_data.first_.statement_);
        next_block_name = next_label->label_.ToString();
        free_next_block = used_blocks.find(next_block_name) == used_blocks.end();
        continue;
      }
      if (jump_data.type_ == BaseNode::IrType::JumpConditionalSt) {
        auto labels = type_checker_.GetLabels(dynamic_cast<JumpConditionalSt *>(jump_st));
        // check right label
        next_block_name = labels.second.ToString();
        free_next_block = used_blocks.find(next_block_name) == used_blocks.end();
        if (free_next_block) {
          next_blocks.push_back(labels.first.ToString());
          continue;
        }
        // check left label
        next_block_name = labels.first.ToString();
        free_next_block = used_blocks.find(next_block_name) == used_blocks.end();
        if (free_next_block) {
          std::cerr << "We found example with inveresed operation" << std::endl;
          auto real_jump_st = dynamic_cast<JumpConditionalSt *>(jump_st);
          real_jump_st->InverseOp();
          real_jump_st->InverseLabels();
          continue;
        }
      }
    }
  }
}
std::unordered_map<std::string, std::vector<IRT::Statement *>> IRT::TraceWrapper::GetTraces() {
  return traces_;
}
