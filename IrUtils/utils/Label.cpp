//
// Created by pavel on 21.07.2020.
//
#pragma once
#include "Label.h"
IRT::Label::Label() : label_("L_" + std::to_string(counter_++)) {
}
IRT::Label::Label(std::string label) : label_(label){

}

std::string IRT::Label::ToString() const {
  return label_;
}

int IRT::Label::counter_ = 0;

