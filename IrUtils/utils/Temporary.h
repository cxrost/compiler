#pragma once
#include <string>

namespace IRT {
class Temporary {
 public:
  Temporary();
  Temporary(std::string Temporary);

  std::string ToString() const;

 private:
  std::string name_;
  static int counter_;
};
}