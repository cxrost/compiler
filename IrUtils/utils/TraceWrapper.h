
#include "../visitors/IncludeNodes.h"
#include <unordered_map>
#include <utility>
#include <vector>
#include <IrUtils/visitors/InfoNodeVisitor.h>

namespace IRT {

class TraceWrapper {
 public:

  TraceWrapper(std::unordered_map<std::string, std::vector<Statement *>> blocks) : blocks_(std::move(blocks)) {}
  void UniteTracesAsDefault(std::string func);
  std::unordered_map<std::string, std::vector<Statement *>> GetTraces();

  std::unordered_map<std::string, std::vector<Statement *>> traces_;
 private:
  std::unordered_map<std::string, std::vector<Statement *>> blocks_;
  InfoNodeVisitor type_checker_;
};
}