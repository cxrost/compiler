//
// Created by pavel on 21.07.2020.
//

#include "Temporary.h"
IRT::Temporary::Temporary() : name_("%_" + std::to_string(counter_++)) {
}
IRT::Temporary::Temporary(std::string name) : name_(name){

}

std::string IRT::Temporary::ToString() const {
  return name_;
}

int IRT::Temporary::counter_ = 0;

