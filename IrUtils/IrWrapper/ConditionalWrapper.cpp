//
// Created by pavel on 21.07.2020.
//

#include "ConditionalWrapper.h"

#include "../visitors/IncludeNodes.h"


IRT::Expression *IRT::ConditionalWrapper::ToExpr() {
  auto* temp_expression = new TempExpr(Temporary());
  Label label_true;
  Label label_false;
  return new EseqExpr(
      new SeqStatement(
          new MoveStatement(temp_expression, new ConstExpr(1)),
          new SeqStatement(
              ToConditional(label_true, label_false),
              new SeqStatement(
                  new LabelStatement(label_false),
                  new SeqStatement(
                      new MoveStatement(temp_expression, new ConstExpr(0)),
                      new LabelStatement(label_true)
                  )
              )
          )
      ),
      temp_expression
  );
}
IRT::Statement *IRT::ConditionalWrapper::ToStatement() {
  return nullptr;
}
