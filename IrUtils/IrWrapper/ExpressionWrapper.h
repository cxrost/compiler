#pragma once

#include "SubTreeWrapper.h"

namespace IRT {

class ExpressionWrapper : public SubTreeWrapper {
 public:
  ExpressionWrapper(Expression* expr);
  IRT::Expression *ToExpr() override;
  IRT::Statement *ToStatement();
  IRT::Statement *ToConditional(Label l_true, Label l_false);

 private:
  Expression* expr_;
};

}