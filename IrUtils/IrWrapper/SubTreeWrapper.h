#pragma once

#include "../nodes/expressions/Expression.h"
#include "../nodes/statements/Statement.h"
#include "../utils/Label.h"

namespace IRT {

class SubTreeWrapper {
 public:
  virtual IRT::Expression *ToExpr() = 0;
  virtual IRT::Statement *ToStatement() = 0;
  virtual IRT::Statement *ToConditional(Label l_true, Label l_false) = 0;
  virtual ~SubTreeWrapper() = default;

};

}