//
// Created by pavel on 22.07.2020.
//
#include "../nodes/statements//LabelStatement.h"
#include "../nodes/statements//SeqStatement.h"
#include "NegConditionalWrapper.h"

IRT::NegConditionalWrapper::NegConditionalWrapper(IRT::SubTreeWrapper *wr) : wrapper_(wr) {}

IRT::Statement *IRT::NegConditionalWrapper::ToConditional(IRT::Label true_label, IRT::Label false_label) {
  return wrapper_->ToConditional(false_label, true_label);
}

