#pragma once

#include "SubTreeWrapper.h"

namespace IRT {

class StatementWrapper : public SubTreeWrapper {
 public:
  StatementWrapper(Statement* st);
  IRT::Expression *ToExpr() override;
  IRT::Statement *ToStatement();
  IRT::Statement *ToConditional(Label l_true, Label l_false);

 private:
  Statement* statement_;
};

}