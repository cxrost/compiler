//
// Created by pavel on 21.07.2020.
//

#include "../visitors/IncludeNodes.h"
#include "assert.h"

#include "StatementWrapper.h"


IRT::Expression *IRT::StatementWrapper::ToExpr() {
  assert(false);
}
IRT::Statement *IRT::StatementWrapper::ToStatement() {
  return statement_;
}
IRT::Statement *IRT::StatementWrapper::ToConditional(IRT::Label l_true, IRT::Label l_false) {
  assert(false);
}
IRT::StatementWrapper::StatementWrapper(IRT::Statement *st) : statement_(st) {

}
