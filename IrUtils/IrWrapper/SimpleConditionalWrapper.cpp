//
// Created by pavel on 21.07.2020.
//
#include "../visitors/IncludeNodes.h"
#include "SimpleConditionalWrapper.h"

IRT::Statement *IRT::SimpleConditionalWrapper::ToConditional(IRT::Label l_true, IRT::Label l_false) {
  return new IRT::JumpConditionalSt(
      op_,
      left_,
      right_,
      l_true,
      l_false
  );
}
IRT::SimpleConditionalWrapper::SimpleConditionalWrapper(IRT::LogicOperator op,
                                                        IRT::Expression *left,
                                                        IRT::Expression *right) : op_(op), left_(left), right_(right) {

}
