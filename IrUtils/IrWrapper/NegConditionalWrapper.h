//
// Created by pavel on 22.07.2020.
//

#pragma once

#include "ConditionalWrapper.h"
namespace IRT {

class NegConditionalWrapper : public ConditionalWrapper {
 public:
  explicit NegConditionalWrapper(IRT::SubTreeWrapper *wr);
  Statement *ToConditional(Label true_label, Label false_label) override;
 private:
  SubTreeWrapper* wrapper_;
};

}