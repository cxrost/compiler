#pragma once

#include "SubTreeWrapper.h"

namespace IRT {

class ConditionalWrapper : public SubTreeWrapper {
 public:
  IRT::Expression *ToExpr() override;
  IRT::Statement *ToStatement() override;

 private:
  Statement* statement_;
};

}