//
// Created by pavel on 21.07.2020.
//

#include  "../visitors/IncludeNodes.h"

#include "ExpressionWrapper.h"


IRT::Expression *IRT::ExpressionWrapper::ToExpr() {
  return expr_;
}
IRT::Statement *IRT::ExpressionWrapper::ToStatement() {
  return new ExprStatement(expr_);
}
IRT::Statement *IRT::ExpressionWrapper::ToConditional(IRT::Label l_true, IRT::Label l_false) {
  return new IRT::JumpConditionalSt(
      LogicOperator::NE,
      expr_,
      new IRT::ConstExpr(0),
      l_true,
      l_false
      );
}
IRT::ExpressionWrapper::ExpressionWrapper(IRT::Expression *expr) : expr_(expr) {

}
