#pragma once


#include "ConditionalWrapper.h"

namespace IRT {

class SimpleConditionalWrapper : public ConditionalWrapper {
 public:
  IRT::Statement *ToConditional(Label l_true, Label l_false) override ;
  SimpleConditionalWrapper(LogicOperator op_, Expression* left_, Expression* right);

 private:
  LogicOperator op_;
  Expression* left_;
  Expression* right_;
};

}