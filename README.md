This is Compiler for MiniJava

Без докера, ставил flex+bizon прямо в систему
(Точнее ставил и даже деплоил прямо в докере, но это очень неудобно 
с CMakeLists.txt связывалось и я передумал)

Запуск:  
    
    1) с тестами PrintVisitor:  
        аргументы программы: ```./../tests/a.in
                             ./../tests/a.out
                             ./../tests/b.in
                             ./../tests/b.out
                             ./../tests/ClassesAndMethods.in
                             ./../tests/ClassesAndMethods.out
                             ./../tests/TestPrintVisitor.in
                             ./../tests/TestPrintVisitor.out```  
    
    2) тест Interpreter:  
            аргументы программы: ```./../tests/Interpreter.in
                                 ./../tests/Interpreter.out```  
    Калькулятор с циклами готов)
    
    3) тест Scopes:
        аргументы программы: ```./../tests/Scopes/NotDeclaredVar.in
                                ./../tests/Scopes/NotDeclaredVar.out
                                ./../tests/Scopes/DoubleDeclaration.in
                                ./../tests/Scopes/DoubleDeclaration.out
                                ./../tests/Scopes/ShadowingVariables.in
                                ./../tests/Scopes/ShadowingVariables.out
                                ./../tests/Scopes/ClassesAndMethods2.in
                                ./../tests/Scopes/ClassesAndMethods2.out
                                ./../tests/Scopes/ClassesAndMethods.in
                                ./../tests/Scopes/ClassesAndMethods.out
                                ./../tests/Scopes/GoodExample.in
                                ./../tests/Scopes/GoodExample.out```
    
    4) тест Типы:
        аргументы программы: ```./../tests/Types/DoubleType.in
                                ./../tests/Types/DoubleType.out
                                ./../tests/Types/DoubleAttr.in
                                ./../tests/Types/DoubleAttr.out
                                ./../tests/Types/DoubleMethod.in
                                ./../tests/Types/DoubleMethod.out
                                ./../tests/Types/IsNotCallable.in
                                ./../tests/Types/IsNotCallable.out
                                ./../tests/Types/IsNotCallable2.in
                                ./../tests/Types/IsNotCallable2.out
                                ./../tests/Types/IsNotCallable3.in
                                ./../tests/Types/IsNotCallable3.out
                                ./../tests/Types/CheckReturnType.in
                                ./../tests/Types/CheckReturnType.out
                                ./../tests/Types/GoodExample.in
                                ./../tests/Types/GoodExample.out
                                ```  
        Есть проверка типов, но на исполнение это пока не влияет(поддержка классов я понял на след. чекпоинте)  
        Проверка типов функций также присутствует)
    5) тест Frames: ``` ./../tests/Frames/Interpreter.in
                        ./../tests/Frames/Interpreter.out
                        ./../tests/Frames/MethodInvocation.in
                        ./../tests/Frames/MethodInvocation.out
                        ./../tests/Frames/HardMethodInvocation.in
                        ./../tests/Frames/HardMethodInvocation.out
                        ./../tests/Frames/Attributes.in
                        ./../tests/Frames/Attributes.out
                        ./../tests/Frames/AttributesBigClass.in
                        ./../tests/Frames/AttributesBigClass.out
                        ```
         Механизм вызова функций есть.
         С интами все хорошо, но с типами есть пара багов, 
                например, при возвращении аттрибута возвращается ссылка на него.
         Как-то весь код стал похож на закостыленый(.
    5-5) тест CE-errors ``` ./../tests/Locations/Train.in
                            ./../tests/Locations/Train.out
                            ./../tests/Locations/Train2.in
                            ./../tests/Locations/Train2.out
                            ./../tests/Types/DoubleType.in
                            ./../tests/Types/DoubleType.out
                            ./../tests/Types/DoubleAttr.in
                            ./../tests/Types/DoubleAttr.out
                            ./../tests/Types/DoubleMethod.in
                            ./../tests/Types/DoubleMethod.out
                            ./../tests/Types/IsNotCallable.in
                            ./../tests/Types/IsNotCallable.out
                            ./../tests/Types/IsNotCallable2.in
                            ./../tests/Types/IsNotCallable2.out
                            ./../tests/Types/CheckReturnType.in
                            ./../tests/Types/CheckReturnType.out
                            ./../tests/Types/GoodExample.in
                            ./../tests/Types/GoodExample.out
                         ```
    6) тест IR_Builder ```
                            ./../tests/IrBuild/IfExample/IfExample.in
                            ./../tests/IrBuild/IfExample/IfExample.out
                            ./../tests/IrBuild/ObjectsAndFuncCalls/SimpleObjects.in
                            ./../tests/IrBuild/ObjectsAndFuncCalls/SimpleObjects.out
                            ./../tests/IrBuild/ObjectsAndFuncCalls/Objects.in
                            ./../tests/IrBuild/ObjectsAndFuncCalls/Objects.out
                          ```
    Здесь все готово кроме логических операторов и массивов. 
    На самом деле код стал смахивать на копипаст из репозитория курса).
    Но что поделать, если там ТАК классно.
    7) тест IR_Builder ```
                                ./../tests/Canonization/DoubleCallExpr/Example1/Example1.in
                                ./../tests/Canonization/DoubleCallExpr/Example1/Example1.out
                                ./../tests/Canonization/DoubleCallExpr/Example2/Example2.in
                                ./../tests/Canonization/DoubleCallExpr/Example2/Example2.out
                                ./../tests/Canonization/EseqRemove/Example1/Example1.in
                                ./../tests/Canonization/EseqRemove/Example1/Example1.out
                                ./../tests/Canonization/EseqRemove/Example2/Example2.in
                                ./../tests/Canonization/EseqRemove/Example2/Example2.out
                                ./../tests/Canonization/EseqRemove/Example3/Example3.in
                                ./../tests/Canonization/EseqRemove/Example3/Example3.out
                                ./../tests/Canonization/EseqRemove/Example4/Example4.in
                                ./../tests/Canonization/EseqRemove/Example4/Example4.out
                                ./../tests/Canonization/EseqRemove/Example5/Example5.in
                                ./../tests/Canonization/EseqRemove/Example5/Example5.out
                                ./../tests/Canonization/EseqRemove/Seq/Example.in
                                ./../tests/Canonization/EseqRemove/Seq/Example.out
                              ```
        _e_ => _call_
        DoubleCallExpr - избавление от 2 аргументов CallExpr(но в нашем случае там просто сохранение в темпе - не интересно)
        
        _call_ => _eseq_ => _lin_ - это файлы, которые удобно сравнивать, что произошло. Наверно, стоило все-таки сделать визуализацию через .dot, но у меня лапки 
        EseqRemove/Example1 - избавление от Move->Eseq (но без случая когда eseq 1 аргументом - без массивов не получится аналогично и mem->eseq)
        EseqRemove/Example2 - избавление от ExprSt->Eseq 
        EseqRemove/Example3 - избавление от CallExpr + ExprList->Eseq 
        EseqRemove/Example4 - избавление от Binop->Eseq (все вариации) 
        EseqRemove/Example5 - избавление от CJump->Eseq (все вариации) 
        EseqRemove/Seq - просто тест на престроение дерева в бамбук seqst.
        
         
        
        // нам вроде как не нужен Jump->Eseq, там только label - аргумент
        // нам вроде как не нужен CallExpr->Eseq, не может иметь 1 аргумента как мы на семинаре задачу разбирали
        // нам вроде как не нужен Eseq->Eseq не должен появляться если мы в binop и cjump от них избавимся(<---это не точно, просто  не придумал теста)
    7) тест Arrays ```
                                    ./../tests/UploadArrays/Grammar/first.in
                                    ./../tests/UploadArrays/Grammar/first.out
                                    ./../tests/UploadArrays/Grammar/second.in
                                    ./../tests/UploadArrays/Grammar/second.out
                                    ./../tests/UploadArrays/Grammar/third.in
                                    ./../tests/UploadArrays/Grammar/third.out
                                  ```
    Первые 2 на грамматику (проверка типов и CE)
    Последний на построение IR дерева с массивами.
    8) тест Blocks + Traces```
    ./../tests/Block/IfExample/IfExample.in 
    ./../tests/Block/IfExample/IfExample.out
    ```
    _lin_ => _blocks_ - один тест с несколькими вариантами CJump'ов (while, if)
    В одном файле последовательно сначало разбиение на блоки, затем на трейсы.
    (#### BlockVisitor #### ===> #### TraceVisitor ####)
    Не избовляюсь от лишних Jump'ов, сделаю это на переводе в ассемблер(логично оставить подструктуру блоков в трейсах).
    
    8) тест ASM```
        ./../tests/Block/IfExample/IfExample.in 
        ./../tests/Block/IfExample/IfExample.out
        ./../tests/Block/Objects/Objects.in 
        ./../tests/Block/Objects/Objects.out
        ./../tests/Block/CallExample/CallExample.in
        ./../tests/Block/CallExample/CallExample.out
        ```
        _blocks_ => _asm_ - перевод ir-кустов в псевдо-ассемблер: Jouette/Schizo
        Есть полное покрытие, но далеко не оптимальное(самое поверхностное из возможных)
        Каждая инструкция отделена переносом строки(думаю удобнее сравнивать ir и asm в двух разных файлах)
        
    
Баги:

Недоработки:
1) Пока тип void == int, т.е. можно спокойно писать void[] a;



