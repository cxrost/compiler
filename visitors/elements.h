#pragma once



// Utils
#include <vector>
#include <string>

// Classes
#include "classes/MainClass.h"
#include "classes/Program.h"


// Declarations
#include "declarations/Declaration.h"
#include "declarations/VariableDeclaration.h"
#include "declarations/MethodDeclaration.h"

// DeclTypes
#include "types/DeclType.h"
#include "types/IntegerType.h"
#include "types/BoolType.h"
#include "types/VoidType.h"
#include "types/ClassType.h"
#include "types/ArrayType.h"

// Expressions
#include "expressions/ExpressionList.h"
#include "expressions/Expression.h"
#include "base_elements/BaseElement.h"
#include "expressions/IntegerArgumentExpressions/NumberExpression.h"
#include "expressions/IdentExpression.h"
#include "expressions/IntegerArgumentExpressions/AddExpression.h"
#include "expressions/IntegerArgumentExpressions/SubExpression.h"
#include "expressions/IntegerArgumentExpressions/MulExpression.h"
#include "expressions/IntegerArgumentExpressions/LessExpression.h"
#include "expressions/IntegerArgumentExpressions/EquelExpression.h"
#include "expressions/IntegerArgumentExpressions/NegExpression.h"
#include "expressions/BracedExpression.h"
#include "expressions/IntegerArgumentExpressions/BoolConstantExpression.h"
#include "expressions/MethodInvocation.h"
#include "expressions/ArrayElement.h"
#include "expressions/ArrayLength.h"
#include "expressions/CreateArray.h"
#include "expressions/CreateObject.h"
#include "expressions/ThisExpression.h"

// Statements
#include "statements/Statement.h"
#include "statements/Assignment.h"
#include "statements/StatementList.h"
#include "statements/Println.h"
#include "statements/LocalVariableDeclaration.h"
#include "statements/ReturnStatement.h"
#include "statements/AssertStatement.h"
#include "statements/ScopeStatement.h"
#include "statements/ConditionalStatement.h"
#include "statements/ComplexConditionalStatement.h"
#include "statements/WhileStatement.h"
#include "statements/MethodInvocationStatement.h"
#include "statements/ArrayDecl.h"

// Formals
#include "formals/Formal.h"
#include "formals/FormalList.h"

// Lvalue
#include "lvalue/SimpleLvalue.h"
#include "lvalue/ArrayElementReference.h"
