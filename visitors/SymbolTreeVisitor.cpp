
#include "SymbolTreeVisitor.h"
#include "elements.h"
#include <sstream>

SymbolTreeVisitor::SymbolTreeVisitor(const std::string& filename, std::shared_ptr<ScopeLayerTree> tree, std::shared_ptr<TypeWrapper> tw)
    : stream_(filename), tree_(std::move(tree)), current_layer(tree_->GetRoot()), type_storage(std::move(tw)) {}

SymbolTreeVisitor::~SymbolTreeVisitor() {
  stream_.close();
}

// Classes:
void SymbolTreeVisitor::Visit(Program *program) {
  Accept(program->class_decls);
//   Accept(program->main);
}

void SymbolTreeVisitor::Visit(MainClass *main_class) {
//  stream_ << "ClassDeclaration: "  << std::endl;
//  auto class_layer = new ScopeLayer;
//  current_layer->AddChild(class_layer);
//  current_layer = class_layer;
//  Accept(main_class->st_list);
//  current_layer = class_layer->GetParent();
}

void SymbolTreeVisitor::Visit(ClassDeclaration *class_decl) {
  stream_ << "ClassDeclaration: " << std::endl;
  auto class_layer = new ScopeLayer(current_layer);
  current_layer->AddChild(class_layer);
  current_layer = class_layer;
  try {
    current_class = type_storage->GetClassDesc(class_decl->name);
  } catch (std::exception& e) {
    throw std::runtime_error(ToStr(class_decl->loc) + e.what());
  }
  for (auto attr : current_class->attributes_type) {
    try {
      current_layer->DeclareVariable(attr.first, std::make_shared<Object>(attr.second));
    } catch (std::exception& e) {
      throw std::runtime_error(ToStr(class_decl->loc) + e.what());
    }
  }
  Accept(class_decl->decls);
  stream_ << "program: " << std::endl;
  current_layer = class_layer->GetParent();
}


void SymbolTreeVisitor::Visit(ClassDeclarationList *class_decls) {
  for (auto class_decl : class_decls->class_decls) {
    Accept(class_decl);
  }
}

// Declarations:
void SymbolTreeVisitor::Visit(DeclarationList *decls) {
  for (auto decl : decls->decls) {
    Accept(decl);
  }
}

void SymbolTreeVisitor::Visit(VariableDeclaration *decl) {
  if (!current_method) {
    return;
  }
  tos_value_ = Accept(decl->type);
  stream_ << "VariableDeclaration: " << std::endl;
  try {
    current_layer->DeclareVariable(decl->identifier, tos_value_);
  } catch (std::exception& e) {
    throw std::runtime_error(ToStr(decl->loc) + e.what());
    // TODO
  }

}

void SymbolTreeVisitor::Visit(MethodDeclaration *decl) {
  stream_ << "MethodDeclaration: " +  decl->name << std::endl;
  auto method_layer = new ScopeLayer(current_layer);
  current_layer->AddChild(method_layer);
  current_layer = method_layer;

  try {
    current_method = current_class->GetMethodDesc(decl->name);
  } catch (std::exception& e) {
    throw std::runtime_error(ToStr(decl->loc) + e.what());
    // TODO
  }
  Accept(decl->type);
  Accept(decl->formals);
  Accept(decl->st_list);
  tree_->AddMapping(current_class->class_name, decl->name, method_layer);
  current_layer = method_layer->GetParent();
  current_method = nullptr;
}

// Types
void SymbolTreeVisitor::Visit(IntegerType *type) {
  tos_value_ = std::make_shared<Object>(std::make_shared<Integer>());
}

void SymbolTreeVisitor::Visit(BoolType *type) {
  tos_value_ = std::make_shared<Object>(std::make_shared<Integer>());
}

// TODO
void SymbolTreeVisitor::Visit(VoidType *type) {
}

void SymbolTreeVisitor::Visit(ClassType *type) {
  try {
    type_storage->GetClassDesc(type->name);
  } catch (std::exception& e) {
    throw std::runtime_error(ToStr(type->loc) + e.what());
  }
  tos_value_ = std::make_shared<Object>(std::make_shared<ComplexType>(type->name));
}


void SymbolTreeVisitor::Visit(ArrayType *type) {
  stream_ << "Array Type: " << std::endl;
  auto subtype = Accept(type->simple_type);
  tos_value_ = std::make_shared<Object>(std::make_shared<Array>(subtype->type));
}

// Expressions:
void SymbolTreeVisitor::Visit(NumberExpression *expression) {
  tos_value_ = std::make_shared<Object>(std::make_shared<Integer>());
}

void SymbolTreeVisitor::Visit(IdentExpression *expression) {
  stream_ << "IdentExpression: " << std::endl;
  try {
    tos_value_ = current_layer->Get(expression->identifier);
  } catch (std::exception& e) {
    throw std::runtime_error(ToStr(expression->loc) + e.what());
  }
}

void SymbolTreeVisitor::Visit(AddExpression *expression) {
  tos_value_ = Accept(expression->first);
  try {
    type_storage->IsArithmetic(tos_value_->type);
  } catch (std::exception& e) {
    throw std::runtime_error(ToStr(expression->first->loc) + e.what());
  }

  try {
    type_storage->IsArithmetic(Accept(expression->second)->type);
  } catch (std::exception& e) {
    throw std::runtime_error(ToStr(expression->second->loc) + e.what());
  }
}

void SymbolTreeVisitor::Visit(SubExpression *expression) {
  tos_value_ = Accept(expression->first);
  try {
    type_storage->IsArithmetic(tos_value_->type);
  } catch (std::exception& e) {
    throw std::runtime_error(ToStr(expression->first->loc) + e.what());
  }

  try {
    type_storage->IsArithmetic(Accept(expression->second)->type);
  } catch (std::exception& e) {
    throw std::runtime_error(ToStr(expression->second->loc) + e.what());
  }
}

void SymbolTreeVisitor::Visit(MulExpression *expression) {
  tos_value_ = Accept(expression->first);
  try {
    type_storage->IsArithmetic(tos_value_->type);
  } catch (std::exception& e) {
    throw std::runtime_error(ToStr(expression->first->loc) + e.what());
  }

  try {
    type_storage->IsArithmetic(Accept(expression->second)->type);
  } catch (std::exception& e) {
    throw std::runtime_error(ToStr(expression->second->loc) + e.what());
    // TODO
  }

}

void SymbolTreeVisitor::Visit(LessExpression *expression) {
  tos_value_ = Accept(expression->first);
  try {
    type_storage->IsArithmetic(tos_value_->type);
  } catch (std::exception& e) {
    throw std::runtime_error(ToStr(expression->first->loc) + e.what());
  }

  try {
    type_storage->IsArithmetic(Accept(expression->second)->type);
  } catch (std::exception& e) {
    throw std::runtime_error(ToStr(expression->second->loc) + e.what());
  }
}

void SymbolTreeVisitor::Visit(EquelExpression *expression) {
  tos_value_ = Accept(expression->first);
  try {
    type_storage->IsArithmetic(tos_value_->type);
  } catch (std::exception& e) {
    throw std::runtime_error(ToStr(expression->first->loc) + e.what());
  }

  try {
    type_storage->IsArithmetic(Accept(expression->second)->type);
  } catch (std::exception& e) {
    throw std::runtime_error(ToStr(expression->second->loc) + e.what());
  }
}

void SymbolTreeVisitor::Visit(NegExpression *expression) {
  tos_value_ = Accept(expression->expression);
  try {
    type_storage->IsArithmetic(tos_value_->type);
  } catch (std::exception& e) {
    throw std::runtime_error(ToStr(expression->loc) + e.what());
  }
}

void SymbolTreeVisitor::Visit(BracedExpression *expression) {
  tos_value_ = Accept(expression->expression);
//  try {
//    type_storage->IsArithmetic(tos_value_->type);
//  } catch (std::exception& e) {
//    throw std::runtime_error(ToStr(expression->loc) + e.what());
//  }
}

void SymbolTreeVisitor::Visit(BoolConstantExpression *expression) {
  tos_value_ = std::make_shared<Object>(std::make_shared<Integer>());
}

// TODO : проверка совпадения списка аргументов
void SymbolTreeVisitor::Visit(MethodInvocation *expr) {
  auto temp = current_method;
  auto this_expr = Accept(expr->this_expr);
  std::string type_name;
  try {
    type_name = type_storage->IsCallable(this_expr->type);
  } catch (std::exception& e) {
    throw std::runtime_error(ToStr(expr->loc) + e.what());
  }
  current_method = type_storage->GetClassDesc(type_name)->GetMethodDesc(expr->name);
  if (expr->args == nullptr) {
    if (!current_method->args.empty()) {
      throw std::runtime_error(ToStr(expr->loc) + " The number of function arguments does not match: expected 0, existed " + std::to_string(current_method->args.size()));
    }
  } else {
    Accept(expr->args);
  }

  tos_value_ = std::make_shared<Object>(current_method->return_type);
  current_method = temp;
}

void SymbolTreeVisitor::Visit(ExpressionList *exprs) {
  if (exprs->expr_list.size() != current_method->args.size()) {
    throw std::runtime_error(ToStr(exprs->loc) + " The number of function {" +current_method->class_name +
    "::" + current_method->method_name + "} " + "arguments does not match: expected "
    + std::to_string(exprs->expr_list.size()) + ", existed "
    + std::to_string(current_method->args.size()));
  }
  for (int i =0; i < exprs->expr_list.size(); ++i) {
    try {
      type_storage->Compare(Accept(exprs->expr_list[i])->type, current_method->args[i]);
    } catch (...) {
      throw std::runtime_error(ToStr(exprs->expr_list[i]->loc) + " There is no such overload of function");
    }
  }
}

// TODO : пока просто тип под масиивом
void SymbolTreeVisitor::Visit(ArrayElement *expression) {
  try {
    type_storage->IsArithmetic(Accept(expression->index)->type);
  } catch (std::exception& e) {
    throw std::runtime_error(ToStr(expression->loc) + e.what());
  }
  try {
    tos_value_ = std::make_shared<Object>(type_storage->GetArraySubType(Accept(expression->obj)->type));
  } catch (std::exception& e) {
    throw std::runtime_error(ToStr(expression->loc) + e.what());
  }
}

// TODO
void SymbolTreeVisitor::Visit(ArrayLength *expression) {
  tos_value_ = Accept(expression->obj);
  try {
    type_storage->GetArraySubType(tos_value_->type);
  } catch (std::exception& e) {
    throw std::runtime_error(ToStr(expression->loc) + e.what());
  }
  tos_value_ = std::make_shared<Object>(Integer());
}

// TODO : проверка типа
void SymbolTreeVisitor::Visit(CreateArray *expression) {
  auto sub_type = Accept(expression->type);
  try {
    type_storage->IsArithmetic(Accept(expression->size)->type);
  } catch (std::exception& e) {
    throw std::runtime_error(ToStr(expression->size->loc) + e.what());
    // TODO
  }
  tos_value_ = std::make_shared<Object>(std::make_shared<Array>(sub_type->type));
}

void SymbolTreeVisitor::Visit(CreateObject *expression) {
  tos_value_ = Accept(expression->type);
  try {
    type_storage->GetClass(tos_value_->type);
  } catch (std::exception& e) {
    throw std::runtime_error(ToStr(expression->loc) + e.what());
    // TODO
  }
}

// TODO
void SymbolTreeVisitor::Visit(ThisExpression *expression) {
  tos_value_ = std::make_shared<Object>(std::make_shared<ComplexType>(current_class->class_name));
}

// Statements
void SymbolTreeVisitor::Visit(Assignment *assignment) {
  auto a = std::shared_ptr<Type>();
  auto b = std::shared_ptr<Type>();
  try {
    a = Accept(assignment->lvalue)->type;
  } catch (std::exception& e) {
    throw std::runtime_error(ToStr(assignment->lvalue->loc) + e.what());
  }
  try {
    b = Accept(assignment->expr)->type;
  } catch (std::exception& e) {
    throw std::runtime_error(ToStr(assignment->expr->loc) + e.what());
  }
  try {
    type_storage->Compare(a, b);
  } catch (...) {
    throw std::runtime_error(ToStr(assignment->expr->loc) + "Assignment with different types: " +
        type_storage->GetTypeByString(a) + "<:>" +
        type_storage->GetTypeByString(b)
    );
  }


}

void SymbolTreeVisitor::Visit(StatementList *statement_list) {
  for (auto statement : statement_list->statements) {
    Accept(statement);
  }
}

void SymbolTreeVisitor::Visit(Println *println) {
  try {
    type_storage->IsArithmetic(Accept(println->expr)->type);
  } catch (std::exception& e) {
    throw std::runtime_error(ToStr(println->loc) + e.what());
  }
}

void SymbolTreeVisitor::Visit(LocalVariableDeclaration *decl) {
  stream_ << "LocalVariableDeclaration: " << std::endl;
  Accept(decl->var_decl);

}

void SymbolTreeVisitor::Visit(ReturnStatement *decl) {
  try {
    type_storage->Compare(Accept(decl->expr)->type, current_method->return_type);
  } catch (...) {
    throw std::runtime_error(ToStr(decl->loc) + "Return value have to be the same type of it declared");
  }
}

void SymbolTreeVisitor::Visit(AssertStatement *decl) {
  try {
    type_storage->IsArithmetic(Accept(decl->expr)->type);
  } catch (std::exception& e) {
    throw std::runtime_error(ToStr(decl->loc) + e.what());
    // TODO
  }
}

void SymbolTreeVisitor::Visit(ScopeStatement *decl) {

  stream_ << "ScopeStatement: " << std::endl;
  auto new_layer = new ScopeLayer(current_layer);
  current_layer->AddChild(new_layer);
  current_layer = new_layer;
  Accept(decl->st_list);
  current_layer = new_layer->GetParent();

}

void SymbolTreeVisitor::Visit(ConditionalStatement *decl) {
  stream_ << "ConditionalStatement: " << std::endl;
  Accept(decl->expr);

  auto new_layer = new ScopeLayer(current_layer);
  current_layer->AddChild(new_layer);
  current_layer = new_layer;

  Accept(decl->statement);

  current_layer = new_layer->GetParent();
}

void SymbolTreeVisitor::Visit(ComplexConditionalStatement *decl) {
  stream_ << "ComplexConditionalStatement: " << std::endl;
  Accept(decl->expr);

  auto new_layer = new ScopeLayer(current_layer);
  current_layer->AddChild(new_layer);
  current_layer = new_layer;

  Accept(decl->true_st);

  current_layer = new_layer->GetParent();

  new_layer = new ScopeLayer(current_layer);
  current_layer->AddChild(new_layer);
  current_layer = new_layer;

  Accept(decl->false_st);

  current_layer = new_layer->GetParent();

}

void SymbolTreeVisitor::Visit(WhileStatement *decl) {
  stream_ << "WhileStatement: " << std::endl;
  Accept(decl->expr);

  auto new_layer = new ScopeLayer(current_layer);
  current_layer->AddChild(new_layer);
  current_layer = new_layer;

  Accept(decl->statement);

  current_layer = new_layer->GetParent();
}

void SymbolTreeVisitor::Visit(MethodInvocationStatement *decl) {
  tos_value_ = Accept(decl->expr);
}

void SymbolTreeVisitor::Visit(ArrayDecl *decl) {
  if (!current_method) {
    return;
  }
  stream_ << "Array VariableDeclaration: " << std::endl;
  tos_value_ = std::make_shared<Object>(std::make_shared<Array>(decl->type));
  try {
    current_layer->DeclareVariable(decl->identifier, tos_value_);
  } catch (std::exception& e) {
    throw std::runtime_error(ToStr(decl->loc) + e.what());
    // TODO
  }
}

// Formals
void SymbolTreeVisitor::Visit(FormalList *formals) {
  for (auto formal : formals->formal_list) {
    Accept(formal);
  }

}

void SymbolTreeVisitor::Visit(Formal *formal) {
  stream_ << "Formal: " << std::endl;
  tos_value_ = Accept(formal->type);
  try {
    current_layer->DeclareVariable(formal->name, tos_value_);
  } catch (std::exception& e) {
    throw std::runtime_error(ToStr(formal->loc) + e.what());
  }
}

// Lvalue
void SymbolTreeVisitor::Visit(SimpleLvalue *lvalue) {
  stream_ << "SimpleLvalue: " << std::endl;
  try {
    tos_value_ = current_layer->Get(lvalue->ident);
  } catch (std::exception& e) {
    throw std::runtime_error(ToStr(lvalue->loc) + e.what());
  }
}

// TODO : вообще убрать, мы сюда никогда не попадаем
void SymbolTreeVisitor::Visit(Lvalue *lvalue) {
}

// TODO
void SymbolTreeVisitor::Visit(ArrayElementReference *lvalue) {
  stream_ << "ArrayElementReference: " << std::endl;
  try {
    type_storage->IsArithmetic(Accept(lvalue->index)->type);
  } catch (std::exception& e) {
    throw std::runtime_error(e.what());
  }
  try {
     auto a = current_layer->Get(lvalue->name);
     tos_value_ = std::make_shared<Object>(type_storage->GetArraySubType(a->type));
  } catch (std::exception& e) {
    throw std::runtime_error(e.what());
  }

}

