
#include "Interpreter.h"

#include <utility>
#include "elements.h"

Interpreter::Interpreter(const std::string& filename, std::shared_ptr<ScopeLayerTree> tree)
    : stream_(filename), tree_(std::move(tree)), current_layer(tree_->GetRoot()) {}

Interpreter::~Interpreter() {
  stream_.close();
}

// Classes:
void Interpreter::Visit(Program* program) {
  Accept(program->class_decls);
//  tos_value_ = Accept(program->main);
}

void Interpreter::Visit(MainClass* main_class) {
  stream_ << "ClassDeclaration: " << (current_layer == nullptr) << std::endl;
  auto class_layer = new ScopeLayer;
  current_layer->AddChild(class_layer);
  current_layer = class_layer;
  Accept(main_class->st_list);
  current_layer = class_layer->GetParent();
}

// TODO
void Interpreter::Visit(ClassDeclaration* class_decl) {
  stream_ << "ClassDeclaration: " << std::endl;
  auto class_layer = new ScopeLayer(current_layer);
  current_layer->AddChild(class_layer);
  current_layer = class_layer;
  Accept(class_decl->decls);
  stream_ << "program: " << (current_layer == nullptr) << std::endl;
  current_layer = class_layer->GetParent();
}

// TODO
void Interpreter::Visit(ClassDeclarationList* class_decls) {
  for (auto class_decl : class_decls->class_decls) {
    Accept(class_decl);
  }
}

// Declarations:
// TODO
void Interpreter::Visit(DeclarationList* decls) {
  for (auto decl : decls->decls)  {
    Accept(decl);
  }
}


void Interpreter::Visit(VariableDeclaration* decl) {
  Accept(decl->type);
  current_layer->DeclareVariable(decl->identifier, std::make_shared<Object>(Integer()));
}

// TODO
void Interpreter::Visit(MethodDeclaration* decl) {
  stream_ << "MethodDeclaration: " << std::endl;
  auto method_layer = new ScopeLayer(current_layer);
  current_layer->AddChild(method_layer);
  current_layer = method_layer;
  Accept(decl->type);
  Accept(decl->formals);
  Accept(decl->st_list);
  current_layer = method_layer->GetParent();
  current_layer = method_layer->GetParent();
}

// Types
void Interpreter::Visit(IntegerType* type) {
}

void Interpreter::Visit(BoolType* type) {
}

void Interpreter::Visit(VoidType* type) {
}

void Interpreter::Visit(ClassType* type) {
}

void Interpreter::Visit(ArrayType* type) {
  Accept(type->simple_type);
}



// Expressions:
void Interpreter::Visit(NumberExpression* expression) {
  tos_value_ = std::make_shared<Object>(Integer());
//  tos_value_->Set(expression->value);
}

void Interpreter::Visit(IdentExpression* expression) {
  stream_ << "IdentExpression: " << std::endl;
  tos_value_ = current_layer->Get(expression->identifier);
}

void Interpreter::Visit(AddExpression *expression) {
  tos_value_ = std::make_shared<Object>(Integer());
//  tos_value_->Set(Accept(expression->first)->ToInt() + Accept(expression->second)->ToInt());
}

void Interpreter::Visit(SubExpression* expression) {
  tos_value_ = std::make_shared<Object>(Integer());
//  tos_value_->Set(Accept(expression->first)->ToInt() - Accept(expression->second)->ToInt());
}

void Interpreter::Visit(MulExpression* expression) {
  tos_value_ = std::make_shared<Object>(Integer());
//  tos_value_->Set(Accept(expression->first)->ToInt() * Accept(expression->second)->ToInt());
}

void Interpreter::Visit(LessExpression* expression) {
  tos_value_ = std::make_shared<Object>(Integer());
//  tos_value_->Set(Accept(expression->first)->ToInt() < Accept(expression->second)->ToInt());
}

void Interpreter::Visit(EquelExpression* expression) {
  tos_value_ = std::make_shared<Object>(Integer());
//  tos_value_->Set(Accept(expression->first)->ToInt() == Accept(expression->second)->ToInt());
}

void Interpreter::Visit(NegExpression* expression) {
  tos_value_ = std::make_shared<Object>(Integer());
//  tos_value_->Set(!Accept(expression->expression)->ToInt());
}

void Interpreter::Visit(BracedExpression* expression) {
  tos_value_ = Accept(expression->expression);
}

void Interpreter::Visit(BoolConstantExpression* expression) {
  tos_value_ = std::make_shared<Object>(Integer());
//  tos_value_->Set(expression->value);
}

// TODO
void Interpreter::Visit(MethodInvocation* expr) {
  Accept(expr->this_expr);
  if (expr->args != nullptr) {
    Accept(expr->args);
  }
}

void Interpreter::Visit(ExpressionList* exprs) {
  for (auto expr : exprs->expr_list) {
    Accept(expr);
  }
}

// TODO
void Interpreter::Visit(ArrayElement* expression) {
  Accept(expression->obj);
  Accept(expression->index);
}

// TODO
void Interpreter::Visit(ArrayLength* expression) {
  Accept(expression->obj);
}

// TODO
void Interpreter::Visit(CreateArray* expression) {
  Accept(expression->type);
  Accept(expression->size);
}

// TODO
void Interpreter::Visit(CreateObject* expression) {
  Accept(expression->type);
}

// TODO
void Interpreter::Visit(ThisExpression* expression) {

}



// Statements
void Interpreter::Visit(Assignment* assignment) {
  auto lvalue = Accept(assignment->lvalue);
  auto value = Accept(assignment->expr);
  *lvalue = *value;
}

void Interpreter::Visit(StatementList* statement_list) {
  for (auto statement : statement_list->statements) {
    Accept(statement);
  }
}

void Interpreter::Visit(Println* println) {
  stream_ << "Println: " << std::endl;
  tos_value_ = Accept(println->expr);
  std::cout << tos_value_->ToInt() << "\n";
}

// TODO 1
void Interpreter::Visit(LocalVariableDeclaration* decl) {
  stream_ << "LocalVariableDeclaration: " << std::endl;
  Accept(decl->var_decl);
  --num_tabs_;
}

// TODO
void Interpreter::Visit(ReturnStatement* decl) {

  stream_ << "ReturnStatement: " << std::endl;
  Accept(decl->expr);
  --num_tabs_;
}

// TODO
void Interpreter::Visit(AssertStatement* decl) {

  stream_ << "AssertStatement: " << std::endl;
  tos_value_ = Accept(decl->expr);
  --num_tabs_;
}

// TODO 2
void Interpreter::Visit(ScopeStatement* decl) {
  stream_ << "ScopeStatement: " << std::endl;
  auto new_layer = new ScopeLayer(current_layer);
  current_layer->AddChild(new_layer);
  current_layer = new_layer;
  Accept(decl->st_list);
  current_layer = new_layer->GetParent();
}

// TODO 1
void Interpreter::Visit(ConditionalStatement* decl) {
  stream_ << "ConditionalStatement: " << std::endl;
  if (Accept(decl->expr)->ToInt()) {
    auto new_layer = new ScopeLayer(current_layer);
    current_layer->AddChild(new_layer);
    current_layer = new_layer;

    Accept(decl->statement);

    current_layer = new_layer->GetParent();
  }
}

// TODO 1
void Interpreter::Visit(ComplexConditionalStatement* decl) {
  stream_ << "ComplexConditionalStatement: " << std::endl;
  tos_value_ = Accept(decl->expr);
  if (tos_value_->ToInt()) {
    auto new_layer = new ScopeLayer(current_layer);
    current_layer->AddChild(new_layer);
    current_layer = new_layer;

    Accept(decl->true_st);

    current_layer = new_layer->GetParent();
  } else {
    auto new_layer = new ScopeLayer(current_layer);
    current_layer->AddChild(new_layer);
    current_layer = new_layer;

    Accept(decl->false_st);

    current_layer = new_layer->GetParent();
  }
  --num_tabs_;
}

// TODO 1
void Interpreter::Visit(WhileStatement* decl) {
  stream_ << "WhileStatement: " << std::endl;
  while (Accept(decl->expr)->ToInt()) {
    auto new_layer = new ScopeLayer(current_layer);
    current_layer->AddChild(new_layer);
    current_layer = new_layer;

    Accept(decl->statement);

    current_layer = new_layer->GetParent();
  }
  --num_tabs_;
}

// TODO
void Interpreter::Visit(MethodInvocationStatement* decl) {

  stream_ << "MethodInvocationStatement: " << std::endl;
  Accept(decl->expr);
  --num_tabs_;
}


// Formals
// TODO
void Interpreter::Visit(FormalList* formals) {
  stream_ << "FormalList: "  << std::endl;
  for (auto formal : formals->formal_list) {
    Accept(formal);
  }
  --num_tabs_;
}

// TODO
void Interpreter::Visit(Formal* formal) {

  stream_ << "Formal: " << formal->name << std::endl;
  current_layer->DeclareVariable(formal->name, std::make_shared<Object>());
  Accept(formal->type);
  --num_tabs_;
}


// Lvalue
void Interpreter::Visit(SimpleLvalue* lvalue) {

  stream_ << "SimpleLvalue: " << lvalue->ident << std::endl;
  tos_value_ = current_layer->Get(lvalue->ident);
  --num_tabs_;
}

// TODO 1
void Interpreter::Visit(Lvalue* lvalue) {

  stream_ << "Lvalue: " << std::endl;
  --num_tabs_;
}

// TODO
void Interpreter::Visit(ArrayElementReference* lvalue) {
  stream_ << "ArrayElementReference: " << lvalue->name << std::endl;
  current_layer->Get(lvalue->name);
  Accept(lvalue->index);
  --num_tabs_;
}
