#pragma once

#include <iostream>
#include "forward_decl.h"

class Visitor {
 public:
  // Classes:
  virtual void Visit(ClassDeclaration* class_decl) = 0;
  virtual void Visit(ClassDeclarationList* class_decls) = 0;
  virtual void Visit(Program* program) = 0;
  virtual void Visit(MainClass* main_class) = 0;


  // Declarations:
  virtual void Visit(DeclarationList* decls) = 0;
  virtual void Visit(VariableDeclaration* decl) = 0;
  virtual void Visit(MethodDeclaration* decl) = 0;

  // Types
  virtual void Visit(IntegerType* type) = 0;
  virtual void Visit(BoolType* type) = 0;
  virtual void Visit(VoidType* type) = 0;
  virtual void Visit(ClassType* type) = 0;
  virtual void Visit(ArrayType* type) = 0;

  // Expressions
  virtual void Visit(NumberExpression* expression) = 0;
  virtual void Visit(IdentExpression* expression) = 0;
  virtual void Visit(AddExpression* expression) = 0;
  virtual void Visit(SubExpression* expression) = 0;
  virtual void Visit(MulExpression* expression) = 0;
  virtual void Visit(LessExpression* expression) = 0;
  virtual void Visit(EquelExpression* expression) = 0;
  virtual void Visit(NegExpression* expression) = 0;
  virtual void Visit(BracedExpression* expression) = 0;
  virtual void Visit(BoolConstantExpression* expression) = 0;
  virtual void Visit(MethodInvocation* expression) = 0;
  virtual void Visit(ExpressionList* expression) = 0;
  virtual void Visit(ArrayElement* expression) = 0;
  virtual void Visit(ArrayLength* expression) = 0;
  virtual void Visit(CreateArray* expression) = 0;
  virtual void Visit(CreateObject* expression) = 0;
  virtual void Visit(ThisExpression* expression) = 0;

  // Statements
  virtual void Visit(Assignment* assignment) = 0;
  virtual void Visit(StatementList* assignment_list) = 0;
  virtual void Visit(Println* println) = 0;
  virtual void Visit(LocalVariableDeclaration* decl) = 0;
  virtual void Visit(ReturnStatement* decl) = 0;
  virtual void Visit(AssertStatement* decl) = 0;
  virtual void Visit(ScopeStatement* decl) = 0;
  virtual void Visit(ConditionalStatement* decl) = 0;
  virtual void Visit(ComplexConditionalStatement* decl) = 0;
  virtual void Visit(WhileStatement* decl) = 0;
  virtual void Visit(MethodInvocationStatement* decl) = 0;
  virtual void Visit(ArrayDecl* expression) = 0;

  // Formals
  virtual void Visit(FormalList* println) = 0;
  virtual void Visit(Formal* println) = 0;

  // Lvalue
  virtual void Visit(SimpleLvalue* lvalue) = 0;
  virtual void Visit(Lvalue* lvalue) = 0;
  virtual void Visit(ArrayElementReference* lvalue) = 0;
  };
