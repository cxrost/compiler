
#include "IrBuildVisitor.h"

#include <utility>
#include <IrUtils/IrWrapper/StatementWrapper.h>
#include <IrUtils/visitors/IncludeNodes.h>
#include <IrUtils/IrWrapper/ExpressionWrapper.h>
#include <IrUtils/IrWrapper/SimpleConditionalWrapper.h>
#include <utils/Frames/Adresses/AddressWithOffset.h>
#include <IrUtils/IrWrapper/NegConditionalWrapper.h>
#include "elements.h"

IrBuildVisitor::IrBuildVisitor(const std::string &filename,
                               ScopeLayer *root_scope,
                               std::shared_ptr<ScopeLayerTree> tree,
                               std::shared_ptr<TypeWrapper> tw)
    : filename_(filename),
      root_scope_(root_scope),
      current_layer_(root_scope_),
      type_storage_(std::move(tw)),
      scope_tree_(std::move(tree)) {
  stream_.open(filename/* , std::fstream::app */);
  stream_ << "\n #### IrBuildVisitor #### \n" << std::endl;
}

IrBuildVisitor::~IrBuildVisitor() {
  stream_.close();
}

// Classes:
void IrBuildVisitor::Visit(Program *program) {
  Accept(program->class_decls);
}

void IrBuildVisitor::Visit(MainClass *main_class) {
  // Legacy
}

void IrBuildVisitor::Visit(ClassDeclaration *class_decl) {
  stream_ << "ClassDeclaration: " << std::endl;
  current_class_ = class_decl->name;
  Accept(class_decl->decls);
}

void IrBuildVisitor::Visit(ClassDeclarationList *class_decls) {
  for (auto cur_class : class_decls->class_decls) {
    Accept(cur_class);
  }
}

// Declarations:
void IrBuildVisitor::Visit(DeclarationList *decls) {
  for (auto decl : decls->decls) {
    Accept(decl);
  }
}

void IrBuildVisitor::Visit(VariableDeclaration *decl) {
  if (current_frame_) { // Not to add attr
    current_frame_->AddVariable(decl->identifier);
  }
  tos_value_ = nullptr;
}

// TODO
void IrBuildVisitor::Visit(MethodDeclaration *decl) {
  stream_ << "MethodDeclaration: " << decl->name << std::endl;
  current_frame_ = new IRT::FrameTranslator(decl->name);
  frames_[current_class_ + "::" + decl->name] = current_frame_;
  // Accept(decl->type);

  current_frame_->AddArgumentAddress("this");
  int i = 0;
  auto this_addr = current_frame_->GetAddress("this");
  for (const auto &attr : type_storage_->GetClassDesc(current_class_)->attributes) {
    // TODO
    current_frame_->AddPseudoArgument(attr, new IRT::AddressWithOffset(this_addr, i * 4 /* magic const */));
    ++i;
  }
  // Allocate place for args
  Accept(decl->formals);

  current_frame_->AddReturnAddress();

  auto body = Accept(decl->st_list);
  if (body) {
    stream_ << "OKKK: " << decl->name << std::endl;
    tos_value_ = new IRT::StatementWrapper(
        new IRT::SeqStatement(
            new IRT::LabelStatement(IRT::Label(current_class_ + "::" + decl->name)),
            body->ToStatement() // TODO: set return value 0 by default
        )
    );
  } else {
    // generating return 0
    tos_value_ = new IRT::StatementWrapper(
        new IRT::SeqStatement(
            new IRT::LabelStatement(IRT::Label(current_class_ + "::" + decl->name)),
            new IRT::MoveStatement(
                current_frame_->GetReturnValueAddress()->ToExpr(),
                new IRT::ConstExpr(0)
            )
        )
    );
    stream_ << "NOOKKK: " << decl->name << std::endl;
  }
  methods_mapping_[current_class_ + "::" + decl->name] = tos_value_->ToStatement();
  current_frame_ = nullptr;
}

// Types
void IrBuildVisitor::Visit(IntegerType *type) {
  tos_type_ = std::make_shared<Integer>(Integer());
}

void IrBuildVisitor::Visit(BoolType *type) {
  tos_type_ = std::make_shared<Integer>(Integer());
}

void IrBuildVisitor::Visit(VoidType *type) {
  tos_type_ = nullptr;
}

void IrBuildVisitor::Visit(ClassType *type) {
  tos_type_ = std::make_shared<ComplexType>(ComplexType(type->name));
}

// TODO
void IrBuildVisitor::Visit(ArrayType *type) {
  stream_ << "Array Type: " << std::endl;
  Accept(type->simple_type);
  auto subtype = tos_type_;
  tos_type_ = std::make_shared<Array>(Array(subtype));
}

// Expressions:
void IrBuildVisitor::Visit(NumberExpression *expression) {
  stream_ << "NumberExpression: " << std::endl;
  tos_value_ = new IRT::ExpressionWrapper(
      new IRT::ConstExpr(expression->value)
  );
}

void IrBuildVisitor::Visit(IdentExpression *expression) {
  stream_ << "IdentExpression: " << std::endl;
  auto address = current_frame_->GetAddress(expression->identifier);
  tos_value_ = new IRT::ExpressionWrapper(address->ToExpr());
}

void IrBuildVisitor::Visit(AddExpression *expression) {
  auto lhs = Accept(expression->first);
  auto rhs = Accept(expression->second);

  tos_value_ = new IRT::ExpressionWrapper(
      new IRT::BinopExpr(
          IRT::BinOperator::PLUS,
          lhs->ToExpr(),
          rhs->ToExpr()
      )
  );
}

void IrBuildVisitor::Visit(SubExpression *expression) {
  auto lhs = Accept(expression->first);
  auto rhs = Accept(expression->second);

  tos_value_ = new IRT::ExpressionWrapper(
      new IRT::BinopExpr(
          IRT::BinOperator::MINUS,
          lhs->ToExpr(),
          rhs->ToExpr()
      )
  );
}

void IrBuildVisitor::Visit(MulExpression *expression) {
  auto lhs = Accept(expression->first);
  auto rhs = Accept(expression->second);

  tos_value_ = new IRT::ExpressionWrapper(
      new IRT::BinopExpr(
          IRT::BinOperator::MUL,
          lhs->ToExpr(),
          rhs->ToExpr()
      )
  );
}

void IrBuildVisitor::Visit(LessExpression *expression) {
  auto lhs = Accept(expression->first);
  auto rhs = Accept(expression->second);

  tos_value_ = new IRT::SimpleConditionalWrapper(
      IRT::LogicOperator::LT,
      lhs->ToExpr(),
      rhs->ToExpr()
  );
}

void IrBuildVisitor::Visit(EquelExpression *expression) {
  auto lhs = Accept(expression->first);
  auto rhs = Accept(expression->second);

  tos_value_ = new IRT::SimpleConditionalWrapper(
      IRT::LogicOperator::EQ,
      lhs->ToExpr(),
      rhs->ToExpr()
  );
}

void IrBuildVisitor::Visit(NegExpression *expression) {
  auto val = Accept(expression->expression);
  tos_value_ = new IRT::NegConditionalWrapper(val);
}

void IrBuildVisitor::Visit(BracedExpression *expression) {
}

void IrBuildVisitor::Visit(BoolConstantExpression *expression) {
  tos_value_ = new IRT::ExpressionWrapper(
      new IRT::ConstExpr(expression->value)
  );
}

void IrBuildVisitor::Visit(MethodInvocation *expr) {
  stream_ << "MethodInvocation: " << std::endl;
  auto irt_expressions = new IRT::ExprList();
  irt_expressions->Add(Accept(expr->this_expr)->ToExpr());
  if (expr->args) {
    for (auto it: expr->args->expr_list) {
      irt_expressions->Add(Accept(it)->ToExpr());
    }
  }

  tos_value_ = new IRT::ExpressionWrapper(
      new IRT::CallExpr(
          new IRT::NameExpr(IRT::Label(expr->name)),
          irt_expressions
      )
  );
}

void IrBuildVisitor::Visit(ExpressionList *exprs) {
  // Never be here
  for (auto expr : exprs->expr_list) {
    Accept(expr);
  }
}


void IrBuildVisitor::Visit(ArrayElement *expression) {
  auto addr = Accept(expression->obj)->ToExpr();
  auto index = Accept(expression->index)->ToExpr();
  tos_value_ = new IRT::ExpressionWrapper(new IRT::MemExpr(new IRT::BinopExpr(
      IRT::BinOperator::PLUS,
      addr,
      new IRT::BinopExpr(
          IRT::BinOperator::MUL,
          new IRT::ConstExpr(4), /* word_size */
          new IRT::BinopExpr(
              IRT::BinOperator::PLUS,
              new IRT::ConstExpr(1),
              index)))));
}

void IrBuildVisitor::Visit(ArrayLength *expression) {
  auto addr = Accept(expression->obj)->ToExpr();
  tos_value_ = new IRT::ExpressionWrapper(new IRT::MemExpr(addr));
}

void IrBuildVisitor::Visit(CreateArray *expression) {
  auto irt_expressions = new IRT::ExprList();
  auto size_expr = Accept(expression->size);
  IRT::Temporary size_temp;
  IRT::Temporary addr;
  auto st_set_size = new IRT::MoveStatement(new IRT::TempExpr(size_temp), size_expr->ToExpr());

  irt_expressions->Add(
      new IRT::BinopExpr(
          IRT::BinOperator::MUL,
          new IRT::ConstExpr(4), /* word_size */
          new IRT::BinopExpr(
              IRT::BinOperator::PLUS,
              new IRT::ConstExpr(1),
              new IRT::TempExpr(size_temp)
          )));
  auto st_set_addr = new IRT::MoveStatement(new IRT::TempExpr(addr),
                                            new IRT::CallExpr(
                                                new IRT::NameExpr(IRT::Label("malloc")),
                                                irt_expressions
                                            ));
  tos_value_ = new IRT::ExpressionWrapper(new IRT::EseqExpr(new IRT::SeqStatement(
      st_set_size,
      new IRT::SeqStatement(st_set_addr,
                            new IRT::MoveStatement(new IRT::MemExpr(new IRT::TempExpr(addr)),
                                                   new IRT::TempExpr(size_temp)))),
                                                            new IRT::TempExpr(addr)));
}

// Пересчитать size!
void IrBuildVisitor::Visit(CreateObject *expression) {
  auto irt_expressions = new IRT::ExprList();
  Accept(expression->type);
  int64_t size =
      type_storage_->Size(std::make_shared<ComplexType>(ComplexType(type_storage_->IsCallable(tos_type_)))) * 4;
  irt_expressions->Add(new IRT::ConstExpr(size));
  tos_value_ = new IRT::ExpressionWrapper(
      new IRT::CallExpr(
          new IRT::NameExpr(IRT::Label("malloc")),
          irt_expressions
      )
  );
}

void IrBuildVisitor::Visit(ThisExpression *expression) {
  tos_value_ = new IRT::ExpressionWrapper(
      current_frame_->GetAddress("this")->ToExpr()
  );
}

// Statements
void IrBuildVisitor::Visit(Assignment *assignment) {
  auto l = Accept(assignment->lvalue);
  auto rvalue = Accept(assignment->expr);
  tos_value_ = new IRT::StatementWrapper(new IRT::MoveStatement(
      l->ToExpr(),
      rvalue->ToExpr())
  );

}

void IrBuildVisitor::Visit(StatementList *statement_list) {
  if (statement_list->statements.empty()) {
    tos_value_ = nullptr;
    return;
  }
  if (statement_list->statements.size() == 1) {
    tos_value_ = Accept(statement_list->statements[0]);
  } else {
    std::vector<IRT::Statement *> statements;
    statements.reserve(statement_list->statements.size());
    for (auto statement: statement_list->statements) {
      auto stmt = Accept(statement);
      if (stmt) {
        statements.push_back(stmt->ToStatement());
      }
    }
    IRT::Statement *suffix = statements.back();
    for (int index = static_cast<int>(statements.size()) - 2; index >= 0; --index) {
      suffix = new IRT::SeqStatement(
          statements.at(index),
          suffix
      );
    }
    tos_value_ = new IRT::StatementWrapper(suffix);
  }
}

void IrBuildVisitor::Visit(Println *println) {
  stream_ << "Println: " << std::endl;
  auto irt_expressions = new IRT::ExprList();
  irt_expressions->Add(Accept(println->expr)->ToExpr());
  // + special arg "%d";

  tos_value_ = new IRT::ExpressionWrapper(
      new IRT::CallExpr(
          new IRT::NameExpr(IRT::Label("printf")),
          irt_expressions
      )
  );
}

void IrBuildVisitor::Visit(LocalVariableDeclaration *decl) {
  stream_ << "LocalVariableDeclaration: " << std::endl;
  tos_value_ = Accept(decl->var_decl);
  --num_tabs_;
}

// TODO
void IrBuildVisitor::Visit(ReturnStatement *decl) {
  stream_ << "ReturnStatement: " << std::endl;
  // Не понятно, почему не возвращаемся, сейчас мы построим IR,
  // который после return будет дальше исполнять функцию
  // требуется безусловный переход
  auto return_expr = Accept(decl->expr);
  tos_value_ = new IRT::StatementWrapper(
      new IRT::MoveStatement(
          current_frame_->GetReturnValueAddress()->ToExpr(),
          return_expr->ToExpr()
      )
  );
  --num_tabs_;
}

// TODO
void IrBuildVisitor::Visit(AssertStatement *decl) {
  // Call assert(expr) ?!
  stream_ << "AssertStatement: " << std::endl;
  tos_value_ = Accept(decl->expr);
  --num_tabs_;
}

void IrBuildVisitor::Visit(ScopeStatement *decl) {
  stream_ << "ScopeStatement: " << std::endl;
  current_frame_->SetupScope();

  tos_value_ = Accept(decl->st_list);
  current_frame_->TearDownScope();
}

void IrBuildVisitor::Visit(ConditionalStatement *decl) {
  stream_ << "ConditionalStatement: " << std::endl;
  auto if_cond_expression = Accept(decl->expr);
  current_frame_->SetupScope();
  auto stmt = Accept(decl->statement);
  current_frame_->TearDownScope();
  IRT::Label label_true;
  IRT::Label label_join;
  IRT::Label *result_true = &label_join;
  IRT::Statement *suffix = new IRT::LabelStatement(label_join);
  if (stmt) {
    result_true = &label_true;
    suffix = new IRT::SeqStatement(
        new IRT::LabelStatement(label_true),
        new IRT::SeqStatement(stmt->ToStatement(), suffix)
    );
  }
  tos_value_ = new IRT::StatementWrapper(
      new IRT::SeqStatement(
          if_cond_expression->ToConditional(*result_true, label_join),
          suffix
      )
  );

}

void IrBuildVisitor::Visit(ComplexConditionalStatement *decl) {
  stream_ << "ComplexConditionalStatement: " << std::endl;
  auto if_cond_expression = Accept(decl->expr);
  current_frame_->SetupScope();
  auto true_stmt = Accept(decl->true_st);
  current_frame_->TearDownScope();
  current_frame_->SetupScope();
  auto false_stmt = Accept(decl->false_st);
  current_frame_->TearDownScope();

  IRT::Label label_true;
  IRT::Label label_false;
  IRT::Label label_join;

  IRT::Statement *suffix = new IRT::LabelStatement(label_join);

  IRT::Label *result_true = &label_join;
  IRT::Label *result_false = &label_join;

  if (false_stmt) {
    result_false = &label_false;
    suffix = new IRT::SeqStatement(
        new IRT::LabelStatement(label_false),
        new IRT::SeqStatement(false_stmt->ToStatement(), suffix)
    );

    if (true_stmt) {
      suffix = new IRT::SeqStatement(
          new IRT::JumpStatement(label_join),
          suffix
      );
    }
  }

  if (true_stmt) {
    result_true = &label_true;
    suffix = new IRT::SeqStatement(
        new IRT::LabelStatement(label_true),
        new IRT::SeqStatement(true_stmt->ToStatement(), suffix)
    );
  }

  tos_value_ = new IRT::StatementWrapper(
      new IRT::SeqStatement(
          if_cond_expression->ToConditional(*result_true, *result_false),
          suffix
      )
  );
  --num_tabs_;
}

void IrBuildVisitor::Visit(WhileStatement *decl) {
  stream_ << "WhileStatement: " << std::endl;
  auto if_cond_expression = Accept(decl->expr);
  current_frame_->SetupScope();
  auto stmt = Accept(decl->statement);
  current_frame_->TearDownScope();

  IRT::Label label_if;
  IRT::Label label_true;
  IRT::Label label_join;

  IRT::Statement *suffix = new IRT::LabelStatement(label_join);
  IRT::Label *result_true = &label_if;

  if (stmt) {
    result_true = &label_true;
    suffix = new IRT::SeqStatement(
        new IRT::LabelStatement(label_true),
        new IRT::SeqStatement(stmt->ToStatement(),
                              new IRT::SeqStatement(new IRT::JumpStatement(label_if), suffix))
    );
  }

  tos_value_ = new IRT::StatementWrapper(
      new IRT::SeqStatement(
          new IRT::LabelStatement(label_if),
          new IRT::SeqStatement(
              if_cond_expression->ToConditional(*result_true, label_join), suffix)
      )
  );

  --num_tabs_;
}

void IrBuildVisitor::Visit(MethodInvocationStatement *decl) {
  stream_ << "MethodInvocationStatement: " << std::endl;
  tos_value_ = Accept(decl->expr);
  --num_tabs_;
}

void IrBuildVisitor::Visit(ArrayDecl *expression) {
  stream_ << "ArrayDecl: " << std::endl;
  current_frame_->AddVariable(expression->identifier);
  tos_value_ = nullptr;
}

// Formals
void IrBuildVisitor::Visit(FormalList *formals) {
  stream_ << "FormalList: " << std::endl;
  for (auto formal : formals->formal_list) {
    Accept(formal);
  }
  --num_tabs_;
}

void IrBuildVisitor::Visit(Formal *formal) {
  stream_ << "Formal: " << formal->name << std::endl;
  current_frame_->AddArgumentAddress(formal->name);
  --num_tabs_;
}

// Lvalue
void IrBuildVisitor::Visit(SimpleLvalue *lvalue) {
  stream_ << "SimpleLvalue: " << lvalue->ident << std::endl;
  tos_value_ = new IRT::ExpressionWrapper(current_frame_->GetAddress(lvalue->ident)->ToExpr());
  --num_tabs_;
}

void IrBuildVisitor::Visit(Lvalue *lvalue) {
  stream_ << "Lvalue: " << std::endl;
  --num_tabs_;
}

void IrBuildVisitor::Visit(ArrayElementReference *lvalue) {
  stream_ << "ArrayElementReference: " << lvalue->name << std::endl;
  auto addr = current_frame_->GetAddress(lvalue->name)->ToExpr();
  auto index = Accept(lvalue->index)->ToExpr();
  tos_value_ = new IRT::ExpressionWrapper(new IRT::MemExpr(new IRT::BinopExpr(
      IRT::BinOperator::PLUS,
      addr,
      new IRT::BinopExpr(
          IRT::BinOperator::MUL,
          new IRT::ConstExpr(4), /* word_size */
          new IRT::BinopExpr(
              IRT::BinOperator::PLUS,
              new IRT::ConstExpr(1),
              index)))));
}
