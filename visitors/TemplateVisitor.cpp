#include "TemplateVisitor.h"
#include <memory>
#include "elements.h"
#include "utils.h"
#include "IrUtils/IrWrapper/SubTreeWrapper.h"

template<typename T>
T TemplateVisitor<T>::Accept(BaseElement * element) {
  element->Accept(this);
  return tos_value_;
}

template std::shared_ptr<Object> TemplateVisitor<std::shared_ptr<Object>>::Accept(BaseElement* element);
template std::shared_ptr<Type> TemplateVisitor<std::shared_ptr<Type>>::Accept(BaseElement* element);
template std::string TemplateVisitor<std::string>::Accept(BaseElement* element);
template IRT::SubTreeWrapper* TemplateVisitor<IRT::SubTreeWrapper*>::Accept(BaseElement* element);