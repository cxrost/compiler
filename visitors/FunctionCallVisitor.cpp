
#include "FunctionCallVisitor.h"

#include <utility>
#include "elements.h"

FunctionCallVisitor::FunctionCallVisitor(const std::string& filename, ScopeLayer* root_scope, std::shared_ptr<ScopeLayerTree> tree, std::shared_ptr<TypeWrapper> tw)
    : filename_(filename), root_scope_(root_scope), current_layer(root_scope_), type_storage_(std::move(tw)), scope_tree_(std::move(tree)){
  stream_.open(filename, std::fstream::app);
  stream_ << "\n #### FunctionCallVisitor #### \n" << std::endl;
}

FunctionCallVisitor::~FunctionCallVisitor() {
  stream_.close();
}

// Classes:
void FunctionCallVisitor::Visit(Program* program) {
}

void FunctionCallVisitor::Visit(MainClass* main_class) {
}

void FunctionCallVisitor::Visit(ClassDeclaration* class_decl) {
}

void FunctionCallVisitor::Visit(ClassDeclarationList* class_decls) {
}

// Declarations:
void FunctionCallVisitor::Visit(DeclarationList* decls) {
}


void FunctionCallVisitor::Visit(VariableDeclaration* decl) {
  tos_value_ = Accept(decl->type);
  size_t index = frame.AllocVariable();
  table_.CreateVariable(decl->identifier);
  table_.Put(decl->identifier, index);
}

// TODO
void FunctionCallVisitor::Visit(MethodDeclaration* decl) {
  stream_ << "MethodDeclaration: " << std::endl;

//  auto method_layer = new ScopeLayer(current_layer);
//  current_layer->AddChild(method_layer);
//  current_layer = method_layer;
//  Accept(decl->type);
  Accept(decl->formals);
  Accept(decl->st_list);
//  current_layer = method_layer->GetParent();
//  current_layer = method_layer->GetParent();
}

// Types
void FunctionCallVisitor::Visit(IntegerType* type) {
  tos_value_ = std::make_shared<Object>(Integer());
}

void FunctionCallVisitor::Visit(BoolType* type) {
  tos_value_ = std::make_shared<Object>(Integer());
}

void FunctionCallVisitor::Visit(VoidType* type) {
}

void FunctionCallVisitor::Visit(ClassType* type) {
  tos_value_ = std::make_shared<Object>(ComplexType(type->name));
}

void FunctionCallVisitor::Visit(ArrayType* type) {
  Accept(type->simple_type);
}



// Expressions:
void FunctionCallVisitor::Visit(NumberExpression* expression) {
  tos_value_ = std::make_shared<Object>(Integer());
  data = expression->value;
  tos_value_->SetAddr(&data);
}

void FunctionCallVisitor::Visit(IdentExpression* expression) {
  stream_ << "IdentExpression: " << std::endl;
  if (type_storage_->GetClass(frame.Get(table_.Get("this"))->type)->HasAttribute(expression->identifier)) {
    int index = table_.Get("this");
    tos_value_ = frame.Get(index);
    // std::cout << "this mem: "<< (int64_t*)*(int64_t*)*(int64_t*)*(int64_t*)*tos_value_->data_;

    // data = (int64_t)type_storage_->GetAttr(tos_value_, expression->identifier);
    int64_t* d = type_storage_->GetAttr(tos_value_, expression->identifier);
    tos_value_ = std::make_shared<Object>(type_storage_->GetClass(frame.Get(table_.Get("this"))->type)->attributes_type[expression->identifier]);
    tos_value_->SetAddr(d);
  } else {
    int index = table_.Get(expression->identifier);
    auto  f = frame.Get(index);
    if (f->type->GetType() == Type::COMPLEXTYPE) {
      int64_t* d = f->data_;
      tos_value_->type = f->type;
      tos_value_->SetAddr(d);
    } else {
      tos_value_ = f;
    }
  }

  // tos_value_ = current_layer->Get(expression->identifier);
}

void FunctionCallVisitor::Visit(AddExpression *expression) {
  int64_t a = Accept(expression->first)->ToInt();
  int64_t b = Accept(expression->second)->ToInt();
  tos_value_ = std::make_shared<Object>(Integer());
  data = a + b;
  tos_value_->SetAddr(&data);
}

void FunctionCallVisitor::Visit(SubExpression* expression) {
  int64_t a = Accept(expression->first)->ToInt();
  int64_t b = Accept(expression->second)->ToInt();
  tos_value_ = std::make_shared<Object>(Integer());
  data = a - b;
  tos_value_->SetAddr(&data);
}

void FunctionCallVisitor::Visit(MulExpression* expression) {
  int64_t a = Accept(expression->first)->ToInt();
  int64_t b = Accept(expression->second)->ToInt();
  tos_value_ = std::make_shared<Object>(Integer());
  data = a * b;
  tos_value_->SetAddr(&data);
}

void FunctionCallVisitor::Visit(LessExpression* expression) {
  int64_t a = Accept(expression->first)->ToInt();
  int64_t b = Accept(expression->second)->ToInt();
  tos_value_ = std::make_shared<Object>(Integer());
  data = a < b;
  tos_value_->SetAddr(&data);
}

void FunctionCallVisitor::Visit(EquelExpression* expression) {
  int64_t a = Accept(expression->first)->ToInt();
  int64_t b = Accept(expression->second)->ToInt();
  tos_value_ = std::make_shared<Object>(Integer());
  data = a == b;
  tos_value_->SetAddr(&data);
}

void FunctionCallVisitor::Visit(NegExpression* expression) {
  int64_t a = Accept(expression->expression)->ToInt();
  tos_value_ = std::make_shared<Object>(Integer());
  data = !a;
  tos_value_->SetAddr(&data);
}

void FunctionCallVisitor::Visit(BracedExpression* expression) {
  tos_value_ = Accept(expression->expression);
}

void FunctionCallVisitor::Visit(BoolConstantExpression* expression) {
  tos_value_ = std::make_shared<Object>(Integer());
  data = expression->value;
  tos_value_->SetAddr(&data);
}

// TODO
void FunctionCallVisitor::Visit(MethodInvocation* expr) {
  auto obj = Accept(expr->this_expr);
  auto type_name = type_storage_->IsCallable(obj->type);
  auto method_desc = type_storage_->GetMethod(type_name, expr->name);
  ScopeLayer* func_scope = scope_tree_->GetFunctionScopeByName(type_name, expr->name);
  FunctionCallVisitor new_visitor(filename_, func_scope, scope_tree_, type_storage_);

  std::vector<std::shared_ptr<Object>> params;
  auto param_data = new int64_t[20];
  //std::cout << "Prepare" << std::endl;
  //std::cout << "this param: " << param_data << " obj: " << obj->data_ << std::endl;
  auto temp = std::make_shared<Object>(obj->type);
  temp->SetAddr(param_data);
  *temp->data_ = (int64_t)obj->data_;
  params.emplace_back(temp);
  if (expr->args != nullptr) {
    int64_t i = 1;
    for (auto arg : expr->args->expr_list) {
      tos_value_ = Accept(arg);
      auto temp2 = std::make_shared<Object>(tos_value_->type);
      temp2->SetAddr(param_data + i);
      *temp2->data_ = *tos_value_->data_;
      //std::cout << "Params " << *(temp2->data_ - 1) << " " << *(temp2->data_) << std::endl;
      params.emplace_back(temp2);
      ++i;
    }
  }
  //std::cout << "Invoke" << std::endl;
  new_visitor.frame.SetParams(params);
  new_visitor.Visit(method_desc->decl);

  // std::cout << "Exit" << std::endl;

  if (new_visitor.frame.returned) {
    tos_value_->SetAddr(&data);
    *tos_value_->data_ = *new_visitor.frame.GetReturnValue()->data_;
    tos_value_->type = new_visitor.frame.GetReturnValue()->type;
  }
  delete[] param_data;
}

void FunctionCallVisitor::Visit(ExpressionList* exprs) {
  for (auto expr : exprs->expr_list) {
    Accept(expr);
  }
}

// TODO
void FunctionCallVisitor::Visit(ArrayElement* expression) {
  Accept(expression->obj);
  Accept(expression->index);
}

// TODO
void FunctionCallVisitor::Visit(ArrayLength* expression) {
  Accept(expression->obj);
}

// TODO
void FunctionCallVisitor::Visit(CreateArray* expression) {
  Accept(expression->type);
  Accept(expression->size);
}

// TODO
void FunctionCallVisitor::Visit(CreateObject* expression) {
  tos_value_ = Accept(expression->type);
  int64_t * a = type_storage_->Allocate(tos_value_);
  // std::cout << "this memory: " << a << std::endl;
  data = (int64_t)a;
  tos_value_->SetAddr(&data);
  k = 1;
}

// TODO
void FunctionCallVisitor::Visit(ThisExpression* expression) {
  tos_value_ = frame.Get(table_.Get("this"));
}




// Statements
void FunctionCallVisitor::Visit(Assignment* assignment) {
  k = 0;
  auto value = Accept(assignment->expr);
  int64_t v = 0;
  v = *value->data_;
//  std::cout << "v: " << (int64_t*)v << std::endl;
//  std::cout << "data: " << (int64_t*)&data << std::endl;
  auto lvalue = Accept(assignment->lvalue);
  int64_t* l = nullptr;
  k = 0;

  // std::cout << "l: " << (int64_t*)*l << " " << l <<  std::endl;
  *lvalue->data_ = v;
  lvalue->type = value->type;
//  std::cout << "Lvalue " << *(int64_t*)v << std::endl;
}

void FunctionCallVisitor::Visit(StatementList* statement_list) {
  for (auto statement : statement_list->statements) {
    if (!frame.returned) {
      Accept(statement);
    }
  }
}

void FunctionCallVisitor::Visit(Println* println) {
  stream_ << "Println: " << std::endl;
  tos_value_ = Accept(println->expr);
  std::cout << tos_value_->ToInt() << "\n";
}

// TODO 1
void FunctionCallVisitor::Visit(LocalVariableDeclaration* decl) {
  stream_ << "LocalVariableDeclaration: " << std::endl;
  Accept(decl->var_decl);
  --num_tabs_;
}

void FunctionCallVisitor::Visit(ReturnStatement* decl) {
  stream_ << "ReturnStatement: " << std::endl;
  auto a = Accept(decl->expr);
  frame.SetReturnValue(a);
  --num_tabs_;
}

// TODO
void FunctionCallVisitor::Visit(AssertStatement* decl) {

  stream_ << "AssertStatement: " << std::endl;
  tos_value_ = Accept(decl->expr);
  --num_tabs_;
}

// TODO 2
void FunctionCallVisitor::Visit(ScopeStatement* decl) {
  stream_ << "ScopeStatement: " << std::endl;

//  auto new_layer = new ScopeLayer(current_layer);
//  current_layer->AddChild(new_layer);
//  current_layer = new_layer;

  frame.AllocScope();
  table_.BeginScope();


  Accept(decl->st_list);

  frame.DeallocScope();
  table_.EndScope();

//  current_layer = new_layer->GetParent();
}

// TODO 1
void FunctionCallVisitor::Visit(ConditionalStatement* decl) {
  stream_ << "ConditionalStatement: " << std::endl;
  if (Accept(decl->expr)->ToInt()) {
//    auto new_layer = new ScopeLayer(current_layer);
//    current_layer->AddChild(new_layer);
//    current_layer = new_layer;

    frame.AllocScope();
    table_.BeginScope();

    Accept(decl->statement);

    frame.DeallocScope();
    table_.EndScope();

//    current_layer = new_layer->GetParent();
  }
}

// TODO 1
void FunctionCallVisitor::Visit(ComplexConditionalStatement* decl) {
  stream_ << "ComplexConditionalStatement: " << std::endl;
  tos_value_ = Accept(decl->expr);
  if (tos_value_->ToInt()) {
//    auto new_layer = new ScopeLayer(current_layer);
//    current_layer->AddChild(new_layer);
//    current_layer = new_layer;

    frame.AllocScope();
    table_.BeginScope();

    Accept(decl->true_st);

    frame.DeallocScope();
    table_.EndScope();

//    current_layer = new_layer->GetParent();
  } else {
//    auto new_layer = new ScopeLayer(current_layer);
//    current_layer->AddChild(new_layer);
//    current_layer = new_layer;

    frame.AllocScope();
    table_.BeginScope();

    Accept(decl->false_st);

    frame.DeallocScope();
    table_.EndScope();

//    current_layer = new_layer->GetParent();
  }
  --num_tabs_;
}

// TODO 1
void FunctionCallVisitor::Visit(WhileStatement* decl) {
  stream_ << "WhileStatement: " << std::endl;
  while (Accept(decl->expr)->ToInt()) {
//    auto new_layer = new ScopeLayer(current_layer);
//    current_layer->AddChild(new_layer);
//    current_layer = new_layer;

    frame.AllocScope();
    table_.BeginScope();

    Accept(decl->statement);

    frame.DeallocScope();
    table_.EndScope();

//    current_layer = new_layer->GetParent();
  }
  --num_tabs_;
}

// TODO
void FunctionCallVisitor::Visit(MethodInvocationStatement* decl) {

  stream_ << "MethodInvocationStatement: " << std::endl;
  Accept(decl->expr);
  --num_tabs_;
}

void FunctionCallVisitor::Visit(ArrayDecl* decl) {
  stream_ << "Array Declaration: " << std::endl;
  std::cerr << "NotSupported" << std::endl;
  --num_tabs_;
}


// Formals
// TODO
void FunctionCallVisitor::Visit(FormalList* formals) {
  stream_ << "FormalList: "  << std::endl;
  table_.CreateVariable("this");
  table_.Put("this",  -1);
  for (int i = 0; i < formals->formal_list.size(); ++i) {
     table_.CreateVariable(formals->formal_list[i]->name);
     table_.Put(formals->formal_list[i]->name, -i - 2);
  }
  --num_tabs_;
}

// TODO
void FunctionCallVisitor::Visit(Formal* formal) {

  stream_ << "Formal: " << formal->name << std::endl;
  std::cout << "nado Formal";
//  current_layer->DeclareVariable(formal->name, std::make_shared<Object>(Integer(0)));
  Accept(formal->type);
  --num_tabs_;
}


// Lvalue
void FunctionCallVisitor::Visit(SimpleLvalue* lvalue) {
  stream_ << "SimpleLvalue: " << lvalue->ident << std::endl;
  if (type_storage_->GetClass(frame.Get(table_.Get("this"))->type)->HasAttribute(lvalue->ident)) {
    int index = table_.Get("this");
    tos_value_ = frame.Get(index);
    // std::cout << "Yes" << sizeof(int64_t) << sizeof(int64_t*) << std::endl;
    // tos_value_ = type_storage_->GetAttr(tos_value_, lvalue->ident);
    // data = (int64_t)type_storage_->GetAttr(tos_value_, lvalue->ident);
    int64_t * d = type_storage_->GetAttr(tos_value_, lvalue->ident);
    tos_value_ = std::make_shared<Object>(type_storage_->GetClass(frame.Get(table_.Get("this"))->type)->attributes_type[lvalue->ident]);
    // tos_value_->SetAddr(&data);
    tos_value_->SetAddr(d);
    k = 1;
  } else {
    int index = table_.Get(lvalue->ident);
    auto  f = frame.Get(index);
    if (f->type->GetType() == Type::COMPLEXTYPE) {
      int64_t* d = f->data_;
      tos_value_->type = f->type;
      tos_value_->SetAddr(d);
    } else {
      tos_value_ = f;
    }
  }
//  tos_value_ = current_layer->Get(lvalue->ident);
  --num_tabs_;
}

// TODO 1
void FunctionCallVisitor::Visit(Lvalue* lvalue) {
  stream_ << "Lvalue: " << std::endl;
  --num_tabs_;
}

// TODO
void FunctionCallVisitor::Visit(ArrayElementReference* lvalue) {
  stream_ << "ArrayElementReference: " << lvalue->name << std::endl;

  frame.Get(table_.Get(lvalue->name));
  Accept(lvalue->index);
  --num_tabs_;
}
void FunctionCallVisitor::SetParams(const std::vector<std::shared_ptr<Object>>& values)  {
  frame.SetParams(values);
}
