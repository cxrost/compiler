#pragma once

#include "TemplateVisitor.h"
#include "iostream"
#include <unordered_map>
#include <memory>
#include "fstream"
#include "types/Object.h"
#include "utils/ScopeLayerTree.h"
#include "utils/Frames/Frame.h"
#include "utils/Frames/FuctionTable.h"
#include <utils/TypeWrapper.h>

class FunctionCallVisitor : public TemplateVisitor<std::shared_ptr<Object>> {
 public:
  FunctionCallVisitor(const std::string& filename, ScopeLayer* root_scope, std::shared_ptr<ScopeLayerTree> tree, std::shared_ptr<TypeWrapper> tw);
  void SetParams(const std::vector<std::shared_ptr<Object>> &values);
  void PrintTabs(long long chg);
  ~FunctionCallVisitor();


  // Classes:
  void Visit(ClassDeclaration* class_decl) override ;
  void Visit(ClassDeclarationList* class_decls) override ;
  void Visit(Program* program) override;
  void Visit(MainClass* main_class) override;

  // Declarations:
  void Visit(DeclarationList* decls);
  void Visit(VariableDeclaration* decl);
  void Visit(MethodDeclaration* decl) override;


  // Types
  void Visit(IntegerType* type);
  void Visit(BoolType* type) override;
  void Visit(VoidType* type) override;
  void Visit(ClassType* type) override;
  void Visit(ArrayType* type) override;




  // Expressions:
  void Visit(NumberExpression* expression) override;
  void Visit(IdentExpression* expression) override;
  void Visit(AddExpression* expression) override;
  void Visit(SubExpression* expression) override;
  void Visit(MulExpression* expression) override;
  void Visit(LessExpression* expression) override;
  void Visit(EquelExpression* expression) override;
  void Visit(NegExpression* expression) override;
  void Visit(BracedExpression* expression) override;
  void Visit(BoolConstantExpression* expression) override;
  void Visit(MethodInvocation* expression) override;
  void Visit(ExpressionList* expression) override;
  void Visit(ArrayElement* expression) override;
  void Visit(ArrayLength* expression) override;
  void Visit(CreateArray* expression) override;
  void Visit(CreateObject* expression) override;
  void Visit(ThisExpression* expression) override;



  // Statements
  void Visit(Assignment* assignment) override;
  void Visit(StatementList* assignment_list) override;
  void Visit(Println* println) override;
  void Visit(LocalVariableDeclaration* decl) override;
  void Visit(ReturnStatement* st) override;
  void Visit(AssertStatement* st) override;
  void Visit(ScopeStatement* st) override;
  void Visit(ConditionalStatement* st) override;
  void Visit(ComplexConditionalStatement* st) override;
  void Visit(WhileStatement* st) override;
  void Visit(MethodInvocationStatement* st) override;
  void Visit(ArrayDecl* st) override;


  // Formals
  void Visit(FormalList* println) override;
  void Visit(Formal* println) override;

  // Lvalue
  void Visit(SimpleLvalue* lvalue) override;
  void Visit(Lvalue* lvalue) override;
  void Visit(ArrayElementReference* lvalue) override;



 private:
  // std::unordered_map<std::string, std::shared_ptr<Object>> variables_;
  Frame frame;
  FunctionTable table_;
  ScopeLayer* root_scope_;
  ScopeLayer* current_layer;
  std::shared_ptr<ScopeLayerTree> scope_tree_;
  std::ofstream stream_;
  std::string filename_;
  std::shared_ptr<TypeWrapper> type_storage_;
  int64_t data;
  int k = 0;
  long long num_tabs_ = 0;
};
