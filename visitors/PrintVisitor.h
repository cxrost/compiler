#pragma once

#include "Visitor.h"

#include "iostream"
#include "fstream"

class PrintVisitor : public Visitor {
 public:
  PrintVisitor(const std::string& filename);
  void PrintTabs(long long chg);
  ~PrintVisitor();
  // Classes:
  void Visit(ClassDeclaration* class_decl) override ;
  void Visit(ClassDeclarationList* class_decls) override ;
  void Visit(Program* program) override;
  void Visit(MainClass* main_class) override;

  // Declarations:
  void Visit(DeclarationList* decls);
  void Visit(VariableDeclaration* decl);
  void Visit(MethodDeclaration* decl) override;


  // Types
  void Visit(IntegerType* type);
  void Visit(BoolType* type) override;
  void Visit(VoidType* type) override;
  void Visit(ClassType* type) override;
  void Visit(ArrayType* type) override;




  // Expressions:
  void Visit(NumberExpression* expression) override;
  void Visit(IdentExpression* expression) override;
  void Visit(AddExpression* expression) override;
  void Visit(SubExpression* expression) override;
  void Visit(MulExpression* expression) override;
  void Visit(LessExpression* expression) override;
  void Visit(EquelExpression* expression) override;
  void Visit(NegExpression* expression) override;
  void Visit(BracedExpression* expression) override;
  void Visit(BoolConstantExpression* expression) override;
  void Visit(MethodInvocation* expression) override;
  void Visit(ExpressionList* expression) override;
  void Visit(ArrayElement* expression) override;
  void Visit(ArrayLength* expression) override;
  void Visit(CreateArray* expression) override;
  void Visit(CreateObject* expression) override;
  void Visit(ThisExpression* expression) override;


  // Statements
  void Visit(Assignment* assignment) override;
  void Visit(StatementList* assignment_list) override;
  void Visit(Println* println) override;
  void Visit(LocalVariableDeclaration* decl) override;
  void Visit(ReturnStatement* st) override;
  void Visit(AssertStatement* st) override;
  void Visit(ScopeStatement* st) override;
  void Visit(ConditionalStatement* st) override;
  void Visit(ComplexConditionalStatement* st) override;
  void Visit(WhileStatement* st) override;
  void Visit(MethodInvocationStatement* st) override;


  // Formals
  void Visit(FormalList* println) override;
  void Visit(Formal* println) override;
  
  // Lvalue
  void Visit(SimpleLvalue* lvalue) override;
  void Visit(Lvalue* lvalue) override;
  void Visit(ArrayElementReference* lvalue) override;



 private:
  std::ofstream stream_;
  long long num_tabs_ = 0;
};
