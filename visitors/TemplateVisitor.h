#pragma once

#include <sstream>
#include "Visitor.h"
#include "elements.h"

template<typename T>
class TemplateVisitor : public Visitor {
 public:
  T Accept(BaseElement* element);

  static std::string ToStr(yy::parser::location_type loc) {
    std::stringstream ss;
    ss << loc;
    return ss.str() + "\n";
  }
 protected:
  T tos_value_;

};

