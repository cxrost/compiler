
#include "TypeVisitor.h"

#include <utility>
#include "elements.h"

TypeVisitor::TypeVisitor(const std::string& filename, std::shared_ptr<TypeWrapper> type_storage)
    : stream_(filename), type_storage(std::move(type_storage)) {}

TypeVisitor::~TypeVisitor() {
  stream_.close();
}

// Classes:
void TypeVisitor::Visit(Program *program) {
  Accept(program->class_decls);
  type_storage->CheckCorrectness();
}

void TypeVisitor::Visit(MainClass *main_class) {
}

void TypeVisitor::Visit(ClassDeclaration *class_decl) {
  stream_ << "ClassDeclaration: " << std::endl;
  try {
    type_storage->AddClass(class_decl->name);
  } catch (std::exception& e) {
    throw std::runtime_error(ToStr(class_decl->loc) + e.what());
  }
  current_class = class_decl->name;
  Accept(class_decl->decls);
}


void TypeVisitor::Visit(ClassDeclarationList *class_decls) {
  for (auto class_decl : class_decls->class_decls) {
    Accept(class_decl);
  }
}

// Declarations:
void TypeVisitor::Visit(DeclarationList *decls) {
  for (auto decl : decls->decls) {
    Accept(decl);
  }
}

void TypeVisitor::Visit(VariableDeclaration *decl) {
  stream_ << "VariableDeclaration: " << std::endl;
  tos_value_ = Accept(decl->type);
  try {
    type_storage->AddAttrToClass(current_class, decl->identifier, tos_value_);
  } catch (std::exception& e) {
    throw std::runtime_error(ToStr(decl->loc) + e.what());
  }
}

void TypeVisitor::Visit(MethodDeclaration *decl) {
  stream_ << "MethodDeclaration: " << std::endl;
  current_func = decl->name;
  // std::cout << decl->name << "\n" << current_class << "\n";
  try {
    type_storage->AddMethodToClass(current_class, current_func);
  } catch (std::exception& e) {
    throw std::runtime_error(ToStr(decl->loc) + e.what());
  }
  Accept(decl->formals);
  type_storage->AddReturnTypeToMethod(current_class, current_func, Accept(decl->type));
  type_storage->AddStartPoint(current_class, current_func, decl);
}

// Types
void TypeVisitor::Visit(IntegerType *type) {
  tos_value_ = std::make_shared<Integer>();
}

void TypeVisitor::Visit(BoolType *type) {
  tos_value_ = std::make_shared<Integer>();
}

// TODO
void TypeVisitor::Visit(VoidType *type) {
  tos_value_ = std::make_shared<Integer>();
}


void TypeVisitor::Visit(ClassType *type) {
  tos_value_ = std::make_shared<ComplexType>(type->name);
}

// TODO
void TypeVisitor::Visit(ArrayType *type) {
  auto t = Accept(type->simple_type);
  tos_value_ = std::make_shared<Array>(t);
}

// Expressions:
void TypeVisitor::Visit(NumberExpression *expression) {
}

void TypeVisitor::Visit(IdentExpression *expression) {
}

void TypeVisitor::Visit(AddExpression *expression) {
}

void TypeVisitor::Visit(SubExpression *expression) {
}

void TypeVisitor::Visit(MulExpression *expression) {
}

void TypeVisitor::Visit(LessExpression *expression) {
}

void TypeVisitor::Visit(EquelExpression *expression) {
}

void TypeVisitor::Visit(NegExpression *expression) {
}

void TypeVisitor::Visit(BracedExpression *expression) {
}

void TypeVisitor::Visit(BoolConstantExpression *expression) {
}

void TypeVisitor::Visit(MethodInvocation *expr) {
}

void TypeVisitor::Visit(ExpressionList *exprs) {
}

void TypeVisitor::Visit(ArrayElement *expression) {
}

void TypeVisitor::Visit(ArrayLength *expression) {
}

void TypeVisitor::Visit(CreateArray *expression) {
}

void TypeVisitor::Visit(CreateObject *expression) {
}

void TypeVisitor::Visit(ThisExpression *expression) {
}


// Statements
void TypeVisitor::Visit(Assignment *assignment) {
}

void TypeVisitor::Visit(StatementList *statement_list) {
}

void TypeVisitor::Visit(Println *println) {
}

void TypeVisitor::Visit(LocalVariableDeclaration *decl) {
}

void TypeVisitor::Visit(ReturnStatement *decl) {
}

void TypeVisitor::Visit(AssertStatement *decl) {
}

void TypeVisitor::Visit(ScopeStatement *decl) {
}

void TypeVisitor::Visit(ConditionalStatement *decl) {
}

void TypeVisitor::Visit(ComplexConditionalStatement *decl) {
}

void TypeVisitor::Visit(WhileStatement *decl) {
}

void TypeVisitor::Visit(MethodInvocationStatement *decl) {
}
void TypeVisitor::Visit(ArrayDecl *decl) {
}

// Formals
void TypeVisitor::Visit(FormalList *formals) {
  for (auto formal : formals->formal_list) {
    Accept(formal);
  }
}

void TypeVisitor::Visit(Formal *formal) {
  tos_value_ = Accept(formal->type);
  type_storage->AddArgToMethod(current_class, current_func, tos_value_);
}

// Lvalue
void TypeVisitor::Visit(SimpleLvalue *lvalue) {
}

void TypeVisitor::Visit(Lvalue *lvalue) {
}

void TypeVisitor::Visit(ArrayElementReference *lvalue) {
}
