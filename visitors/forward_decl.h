#pragma once

class Type;
class Object;
class Integer;
class ComplexType;



class BaseElement;

// Classes:
class ClassDeclaration;
class ClassDeclarationList;
class ClassDeclarationList;
class MainClass;
class Program;


// Declarations:
class Declaration;
class DeclarationList;
class VariableDeclaration;
class LocalVariableDeclaration;
class MethodDeclaration;

// Types:
class DeclType;
class SimpleType;
class IntegerType;
class BoolType;
class VoidType;
class ClassType;
class ArrayType;

// Expressions:
class Expression;
class NumberExpression;
class IdentExpression;
class AddExpression;
class SubExpression;
class MulExpression;
class LessExpression;
class EquelExpression;
class NegExpression;
class BracedExpression;
class BoolConstantExpression;
class MethodInvocation;
class ExpressionList;
class ArrayElement;
class ArrayLength;
class CreateArray;
class CreateObject;
class ThisExpression;
class ArrayDecl;
class AttrExpr;


// Statements
class Statement;
class StatementList;
class Assignment;
class Println;
class ReturnStatement;
class AssertStatement;
class ScopeStatement;
class ConditionalStatement;
class ComplexConditionalStatement;
class WhileStatement;
class MethodInvocationStatement;

// Formals
class FormalList;
class Formal;

// Lvalue
class Lvalue;
class SimpleLvalue;
class ArrayElementReference;

