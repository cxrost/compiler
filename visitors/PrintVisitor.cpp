
#include <grammar/declarations/VariableDeclaration.h>
#include "PrintVisitor.h"
#include "elements.h"

PrintVisitor::PrintVisitor(const std::string& filename) : stream_(filename) {}

PrintVisitor::~PrintVisitor() {
  stream_.close();
}

void PrintVisitor::PrintTabs(long long chg) {
  num_tabs_ += chg;
  for (long long i = 0; i < num_tabs_; ++i) {
    stream_ << '\t';
  }
}


// Classes:
void PrintVisitor::Visit(Program* program) {
  PrintTabs(1);
  stream_ << "Program: " << std::endl;
  program->class_decls->Accept(this);

//  program->main->Accept(this);
  --num_tabs_;
}

void PrintVisitor::Visit(MainClass* main_class) {
  PrintTabs(1);
  stream_ << "MainClass: " << std::endl;
  main_class->st_list->Accept(this);
  --num_tabs_;
}

void PrintVisitor::Visit(ClassDeclaration* class_decl) {
  PrintTabs(1);
  stream_ << "ClassDeclaration: " << class_decl->name << std::endl;
  class_decl->decls->Accept(this);
  --num_tabs_;
}

void PrintVisitor::Visit(ClassDeclarationList* class_decls) {
  stream_ << "We came: " << std::endl;
  PrintTabs(1);
  stream_ << "ClassDeclarationList: " << std::endl;
  for (auto class_decl : class_decls->class_decls) {
    class_decl->Accept(this);
  }
  --num_tabs_;
}

// Declarations:
void PrintVisitor::Visit(DeclarationList* decls) {
  PrintTabs(1);
  stream_ << "DeclarationList: " << std::endl;
  for (auto decl : decls->decls)  {
    decl->Accept(this);
  }
  --num_tabs_;
}

void PrintVisitor::Visit(VariableDeclaration* decl) {
  PrintTabs(1);
  stream_ << "VariableDeclaration: " << decl->identifier << std::endl;
  decl->type->Accept(this);
  --num_tabs_;
}

void PrintVisitor::Visit(MethodDeclaration* decl) {
  PrintTabs(1);
  stream_ << "MethodDeclaration: " << decl->name << std::endl;
  decl->type->Accept(this);
  decl->formals->Accept(this);
  decl->st_list->Accept(this);
  --num_tabs_;
}

// Types
void PrintVisitor::Visit(IntegerType* type) {
  PrintTabs(1);
  stream_ << "IntegerType: " << std::endl;
  --num_tabs_;
}

void PrintVisitor::Visit(BoolType* type) {
  PrintTabs(1);
  stream_ << "BoolType: " << std::endl;
  --num_tabs_;
}

void PrintVisitor::Visit(VoidType* type) {
  PrintTabs(1);
  stream_ << "VoidType: " << std::endl;
  --num_tabs_;
}

void PrintVisitor::Visit(ClassType* type) {
  PrintTabs(1);
  stream_ << "ClassType: " << type->name << std::endl;
  --num_tabs_;
}

void PrintVisitor::Visit(ArrayType* type) {
  PrintTabs(1);
  stream_ << "ArrayType: "  << std::endl;
  type->simple_type->Accept(this);
  --num_tabs_;
}



// Expressions:
void PrintVisitor::Visit(NumberExpression* expression) {
  PrintTabs(1);
  stream_ << "NumberExpression: " << expression->value << std::endl;
  --num_tabs_;
}

void PrintVisitor::Visit(IdentExpression* expression) {
  PrintTabs(1);
  stream_ << "IdentExpression: " << expression->identifier << std::endl;
  --num_tabs_;
}

void PrintVisitor::Visit(AddExpression *expression) {
  PrintTabs(1);
  stream_ << "AddExpression: " << std::endl;
  expression->first->Accept(this);
  expression->second->Accept(this);
  --num_tabs_;
}

void PrintVisitor::Visit(SubExpression* expression) {
  PrintTabs(1);
  stream_ << "SubExpression: " << std::endl;
  expression->first->Accept(this);
  expression->second->Accept(this);
  --num_tabs_;
}

void PrintVisitor::Visit(MulExpression* expression) {
  PrintTabs(1);
  stream_ << "MulExpression: " << std::endl;
  expression->first->Accept(this);
  expression->second->Accept(this);
  --num_tabs_;
}

void PrintVisitor::Visit(LessExpression* expression) {
  PrintTabs(1);
  stream_ << "LessExpression: " << std::endl;
  expression->first->Accept(this);
  expression->second->Accept(this);
  --num_tabs_;
}

void PrintVisitor::Visit(EquelExpression* expression) {
  PrintTabs(1);
  stream_ << "EquelExpression: " << std::endl;
  expression->first->Accept(this);
  expression->second->Accept(this);
  --num_tabs_;
}

void PrintVisitor::Visit(NegExpression* expression) {
  PrintTabs(1);
  stream_ << "NegExpression: " << std::endl;
  expression->expression->Accept(this);
  --num_tabs_;
}

void PrintVisitor::Visit(BracedExpression* expression) {
  PrintTabs(1);
  stream_ << "BracedExpression: " << std::endl;
  expression->expression->Accept(this);
  --num_tabs_;
}

void PrintVisitor::Visit(BoolConstantExpression* expression) {
  PrintTabs(1);
  stream_ << "BoolConstantExpression: " << expression->value << std::endl;
  --num_tabs_;
}

void PrintVisitor::Visit(MethodInvocation* expr) {
  PrintTabs(1);
  stream_ << "MethodInvocation: " << expr->name << std::endl;
  expr->this_expr->Accept(this);
  expr->args->Accept(this);
  --num_tabs_;
}

void PrintVisitor::Visit(ExpressionList* exprs) {
  PrintTabs(1);
  stream_ << "ExpressionList: " << std::endl;
  for (auto expr : exprs->expr_list) {
    expr->Accept(this);
  }
  --num_tabs_;
}

void PrintVisitor::Visit(ArrayElement* expression) {
  PrintTabs(1);
  stream_ << "ArrayElement: " << std::endl;
  expression->obj->Accept(this);
  expression->index->Accept(this);
  --num_tabs_;
}

void PrintVisitor::Visit(ArrayLength* expression) {
  PrintTabs(1);
  stream_ << "ArrayLength(): " << std::endl;
  expression->obj->Accept(this);
  --num_tabs_;
}

void PrintVisitor::Visit(CreateArray* expression) {
  PrintTabs(1);
  stream_ << "CreateArray: " << std::endl;
  expression->type->Accept(this);
  expression->size->Accept(this);
  --num_tabs_;
}

void PrintVisitor::Visit(CreateObject* expression) {
  PrintTabs(1);
  stream_ << "CreateObject: " << std::endl;
  expression->type->Accept(this);
  --num_tabs_;
}

void PrintVisitor::Visit(ThisExpression* expression) {
  PrintTabs(1);
  stream_ << "ThisExpression: " << std::endl;
  --num_tabs_;
}



// Statements:
void PrintVisitor::Visit(Assignment* assignment) {
  PrintTabs(1);
  stream_ << "Assignment: " << std::endl;
  assignment->lvalue->Accept(this);
  assignment->expr->Accept(this);
  --num_tabs_;
}

void PrintVisitor::Visit(StatementList* statement_list) {
  PrintTabs(1);
  stream_ << "StatementList: " << std::endl;
  for (auto statement : statement_list->statements) {
    statement->Accept(this);
  }
  --num_tabs_;
}

void PrintVisitor::Visit(Println* println) {
  PrintTabs(1);
  stream_ << "Println: " << std::endl;
  println->expr->Accept(this);
  --num_tabs_;
}

void PrintVisitor::Visit(LocalVariableDeclaration* decl) {
  PrintTabs(1);
  stream_ << "LocalVariableDeclaration: " << std::endl;
  decl->var_decl->Accept(this);
  --num_tabs_;
}

void PrintVisitor::Visit(ReturnStatement* decl) {
  PrintTabs(1);
  stream_ << "ReturnStatement: " << std::endl;
  decl->expr->Accept(this);
  --num_tabs_;
}

void PrintVisitor::Visit(AssertStatement* decl) {
  PrintTabs(1);
  stream_ << "AssertStatement: " << std::endl;
  decl->expr->Accept(this);
  --num_tabs_;
}

void PrintVisitor::Visit(ScopeStatement* decl) {
  PrintTabs(1);
  stream_ << "ScopeStatement: " << std::endl;
  decl->st_list->Accept(this);
  --num_tabs_;
}

void PrintVisitor::Visit(ConditionalStatement* decl) {
  PrintTabs(1);
  stream_ << "ConditionalStatement: " << std::endl;
  decl->expr->Accept(this);
  decl->statement->Accept(this);
  --num_tabs_;
}

void PrintVisitor::Visit(ComplexConditionalStatement* decl) {
  PrintTabs(1);
  stream_ << "ComplexConditionalStatement: " << std::endl;
  decl->expr->Accept(this);
  decl->true_st->Accept(this);
  decl->false_st->Accept(this);
  --num_tabs_;
}

void PrintVisitor::Visit(WhileStatement* decl) {
  PrintTabs(1);
  stream_ << "WhileStatement: " << std::endl;
  decl->expr->Accept(this);
  decl->statement->Accept(this);
  --num_tabs_;
}

void PrintVisitor::Visit(MethodInvocationStatement* decl) {
  PrintTabs(1);
  stream_ << "MethodInvocationStatement: " << std::endl;
  decl->expr->Accept(this);
  --num_tabs_;
}


// Formals
void PrintVisitor::Visit(FormalList* formals) {
  PrintTabs(1);
  stream_ << "FormalList: "  << std::endl;
  for (auto formal : formals->formal_list) {
    formal->Accept(this);
  }
  --num_tabs_;
}

void PrintVisitor::Visit(Formal* formal) {
  PrintTabs(1);
  stream_ << "Formal: " << formal->name << std::endl;
  formal->type->Accept(this);
  --num_tabs_;
}


// Lvalue
void PrintVisitor::Visit(SimpleLvalue* lvalue) {
  PrintTabs(1);
  stream_ << "SimpleLvalue: " << lvalue->ident << std::endl;
  --num_tabs_;
}

void PrintVisitor::Visit(Lvalue* lvalue) {
  PrintTabs(1);
  stream_ << "Lvalue: " << std::endl;
  --num_tabs_;
}

void PrintVisitor::Visit(ArrayElementReference* lvalue) {
  PrintTabs(1);
  stream_ << "ArrayElementReference: " << lvalue->name << std::endl;
  lvalue->index->Accept(this);
  --num_tabs_;
}
