#include <visitors/IrBuildVisitor.h>
#include <IrUtils/visitors/PrintVisitor.h>
#include <IrUtils/visitors/DoubleCallExprRemoval.h>
#include <IrUtils/visitors/EseqRemovalVisitor.h>
#include <IrUtils/visitors/LinearizationVisitor.h>
#include <IrUtils/utils//TraceWrapper.h>
#include <IrUtils/visitors/InstructionSelector.h>
#include "driver.hh"

Driver::Driver() :
    trace_parsing(false),
    trace_scanning(false),
    scanner(*this), parser(scanner, *this) {
}

void Driver::PrintAST(const std::string &filename) {
  // PrintVisitor visitor(filename);
  // visitor.Visit(program);
  auto tw = std::make_shared<TypeWrapper>();
  auto scope_tree = std::make_shared<ScopeLayerTree>();
  std::cout << "Begin" << std::endl;
  TypeVisitor v1(filename, tw);
  v1.Visit(program);
  std::cout << "Types" << std::endl;
  SymbolTreeVisitor v2(filename, scope_tree, tw);
  v2.Visit(program);

  std::cout << "Not Interpreting" << std::endl;
  auto main_func = tw->GetMain();
  ScopeLayer *func_scope = scope_tree->GetFunctionScopeByName(main_func->class_name, main_func->method_name);

  //  FunctionCallVisitor v3(filename, func_scope, scope_tree, tw);
//  std::vector<std::shared_ptr<Object>> params;
//  params.emplace_back(std::make_shared<Object>(ComplexType(main_func->class_name)));
//  v3.SetParams(params);
//  v3.Visit(main_func->decl);

//  Interpreter v4(filename);
//  v4.Visit(main_func->decl);

  IrBuildVisitor v5(filename, func_scope, scope_tree, tw);
  v5.Visit(program);
  auto ir_map = v5.GetMapping();

  for (const auto &func : ir_map) {
    std::cout << func.first << std::endl;
//    IRT::PrintVisitor v6(filename + "_e_" + func.first + "_ir.out");
//    func.second->Accept(&v6);

    IRT::DoubleCallExprRemoval v_opt1(filename + "_call_" + func.first + "_ir.out");
    func.second->Accept(&v_opt1);
    auto tree_optimized1 = v_opt1.GetIrTree();
//    IRT::PrintVisitor v8(filename + "_call_" + func.first + "_ir.out");
//    tree_optimized1->Accept(&v8);

    IRT::EseqRemovalVisitor v_opt2(filename + "_eseq_" + func.first + "_ir.out");
    tree_optimized1->Accept(&v_opt2);
    auto tree_optimized2 = v_opt2.GetIrTree();
//    IRT::PrintVisitor v9(filename + "_eseq_" + func.first + "_ir.out");
//    tree_optimized2->Accept(&v9);

    IRT::LinearizationVisitor v_opt3(filename + "_lin_" + func.first + "_ir.out");
    tree_optimized2->Accept(&v_opt3);
    auto tree_optimized3 = v_opt3.GetIrTree();
    IRT::PrintVisitor v10(filename + "_lin_" + func.first + "_ir.out");
    tree_optimized3->Accept(&v10);
    auto blocks = v_opt3.GetBlocks(tree_optimized3);
    IRT::PrintVisitor v11(filename + "_blocks_" + func.first + "_ir.out");
    v11.PrintString("BlockVisitor");
    for (auto block : blocks) {
      v11.PrintString(block.first);
      for (auto sub_block : block.second) {
        sub_block->Accept(&v11);
      }
    }
    std::cout << "Traces" << std::endl;
    v11.PrintString("TraceVisitor");
    IRT::TraceWrapper trace_wr(blocks);
    trace_wr.UniteTracesAsDefault(func.first);
    auto traces = trace_wr.GetTraces();
    for (const auto& trace : traces) {
      v11.PrintString(trace.first);
      for (auto sub_block : trace.second) {
        sub_block->Accept(&v11);
      }
    }
    IRT::InstructionSelector selector(filename + "_asm_" + func.first + "_ir.out");
    selector.GetInstructions(traces);
  }
}

int Driver::parse(const std::string &f) {
  file = f;
  location.initialize(&file);
  scan_begin();
  parser.set_debug_level(trace_parsing);
  int res = parser();
  scan_end();
  return res;
}

void Driver::scan_begin() {
  scanner.set_debug(trace_scanning);
  if (file.empty() || file == "-") {
  } else {
    stream.open(file);
    std::cout << file << std::endl;
    scanner.yyrestart(&stream);
  }
}

void Driver::scan_end() {
  stream.close();
}

void Driver::InitMainFrame(Frame &frame) {
}

