#pragma once

#include "Type.h"
#include <string>
#include <utility>
#include <vector>
#include <iostream>
#include <memory>

class Method : public Type {
 public:
  explicit Method(std::string class_name, std::string method_name)
      : class_name(std::move(class_name)), method_name(std::move(method_name)), Type(COMPLEXTYPE) {
  }

  int64_t ToInt(int64_t* data) override {
    std::cerr << "Bad Covert";
    return -1;
  }

  std::string class_name;
  std::string method_name;
};

