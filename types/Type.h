#pragma once
#include <iostream>
#include <memory>
// #include "Object.h"

class Type : public std::enable_shared_from_this<Type> {
 public:
  enum RealType {
    TYPE,
    INTEGER,
    COMPLEXTYPE,
    ARRAY,
    METHOD
  };
  Type() : real_type(TYPE) {}

  virtual int64_t ToInt(int64_t* data);
  virtual RealType GetType() {
    return real_type;
  }
  virtual ~Type() = default;

 protected:
  Type::RealType real_type;


  explicit Type(RealType real_type) : real_type(real_type) {}
};
