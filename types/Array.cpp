//
// Created by pavel on 29.07.2020.
//

#include "Array.h"

std::shared_ptr<Type> ConvertToTypeT(std::string simple_type) {
  if (simple_type == "int") {
    return std::make_shared<Integer>();
  }
  if (simple_type == "bool") {
    return std::make_shared<Integer>();
  }
  if (simple_type == "void") {
    throw std::runtime_error("No type void[]");
  }
  return std::make_shared<ComplexType>(simple_type);
}