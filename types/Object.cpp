//
// Created by pavel on 22.05.2020.
//

#include "Object.h"
int64_t Object::ToInt() {
  type->ToInt(data_);
}


void Object::SetAddr(int64_t* data) {
  // type->SetAddr(data, &data_);
  data_ = data;
}

int64_t* Object::GetAddr(int64_t offset) {
  return data_ + offset;
}

Object::Object(std::shared_ptr<Type> type) : type(std::move(type)) {}
Object::Object(std::shared_ptr<Array> type) : type(std::move(type)){}
Object::Object(std::shared_ptr<Integer> type) : type(std::move(type)) {}
Object::Object(std::shared_ptr<ComplexType> type) : type(std::move(type)) {}
Object::Object(Type type) : type(std::make_shared<Type>(std::move(type))) {}
Object::Object(Integer type) : type(std::make_shared<Integer>(std::move(type))) {}
Object::Object(ComplexType type) : type(std::make_shared<ComplexType>(std::move(type))) {}
Object::Object(Array type) : type(std::make_shared<Array>(std::move(type))) {}
Object::Object() : type(std::make_shared<Type>()) {}


