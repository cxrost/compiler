//
// Created by pavel on 21.05.2020.
//

#include "Integer.h"
Type::RealType Integer::GetType() {
  return real_type;
}
int64_t Integer::ToInt(int64_t *data) {
  return *data;
}

Integer::Integer() : Type(RealType::INTEGER) {}
