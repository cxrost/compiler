#pragma once

#include "Type.h"
#include "Integer.h"
#include "ComplexType.h"
#include <string>
#include <utility>
#include <vector>
#include <iostream>
#include <memory>

std::shared_ptr<Type> ConvertToTypeT(std::string simple_type);

class Array : public Type {
 public:
  explicit Array(std::string simple_type) : type(ConvertToTypeT(simple_type)), Type(ARRAY) {
  }
  explicit Array(std::shared_ptr<Type> type) : type(std::move(type)), Type(ARRAY) {
  }

  int64_t ToInt(int64_t* data) override {
    std::cerr << "Bad Covert";
    return -1;
  }

  std::shared_ptr<Type> type;
};

