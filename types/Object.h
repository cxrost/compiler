#pragma once

#include "Type.h"
#include "Integer.h"
#include "ComplexType.h"
#include "Array.h"
#include "memory"

class Object {
 public:
  explicit Object(std::shared_ptr<Type> type);
  explicit Object(std::shared_ptr<Array> type);
  explicit Object(std::shared_ptr<Integer> type);
  explicit Object(std::shared_ptr<ComplexType> type);
  explicit Object(Type type);
  explicit Object(Integer type);
  explicit Object(ComplexType type);
  explicit Object(Array type);
  explicit Object();


  int64_t ToInt();

  void SetAddr(int64_t* data);
  int64_t* GetAddr(int64_t offset);

  int64_t* data_;
  std::shared_ptr<Type> type;
};
