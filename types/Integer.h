#pragma once

#include "Type.h"

class Integer : public Type {
 public:

  int64_t ToInt(int64_t* data) override;

  RealType GetType() override;

  explicit Integer();
  int value_;
};

