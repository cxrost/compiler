%skeleton "lalr1.cc"
%require "3.5"

%defines
%define api.token.constructor
%define api.value.type variant
%define parse.assert

%code requires {
    #include <string>
    #include "visitors/forward_decl.h"

    class Scanner;
    class Driver;
}

// %param { Driver &drv }

%define parse.trace
%define parse.error verbose

%code {
    #include "driver.hh"
    #include "location.hh"
    #include "visitors/elements.h"

    static yy::parser::symbol_type yylex(Scanner &scanner, Driver& driver) {
        return scanner.ScanToken();
    }

    void Create_UB(std::string reason) {
    	std::cout << "Sorry, on this step \"" << reason << "\" is not availible" << std::endl;
    }
}

%lex-param { Scanner &scanner }
%lex-param { Driver &driver }
%parse-param { Scanner &scanner }
%parse-param { Driver &driver }

%locations

%define api.token.prefix {TOK_}

%token
    END 0 "end of file"
    ASSIGN "="
    MINUS "-"
    PLUS "+"
    STAR "*"
    PERCENT "%"
    LOGIC_AND "&&"
    LOGIC_OR "||"
    LESS_THAN "<"
    GREATER_THAN ">"
    EQUEL "=="
    NEG "!"

    SLASH "/"
    LPAREN "("
    RPAREN ")"
    LPAREN_FIG "{"
    RPAREN_FIG "}"
    LPAREN_SQR "["
    RPAREN_SQR "]"
    SEMICOLON ";"
    COMMA ","
    POINT "."



    KEY_W_CLASS "class"
    KEY_W_IF "if"
    KEY_W_ELSE "else"
    KEY_W_WHILE "while"
    KEY_W_PUBLIC "public"
    KEY_W_STATIC "static"
    KEY_W_MAIN "main"
    KEY_W_PRINT "System.out.println"
    KEY_W_ASSERT "assert"
    KEY_W_RETURN "return"
    KEY_W_EXTENDS "extends"
    KEY_W_LENGTH "length"
    KEY_W_NEW "new"

    KEY_W_VOID "void"
    KEY_W_INT "int"
    KEY_W_BOOLEAN "boolean"
    KEY_W_TRUE "true"
    KEY_W_FALSE "false"
    KEY_W_THIS "this"
;

%token <std::string> IDENTIFIER "identifier"
%token <int> NUMBER "number"


%nterm <Program*> program
// %nterm <MainClass*> main_class
%nterm <ClassDeclarationList*> class_declarations
%nterm <ClassDeclaration*> class_declaration
%nterm <Declaration*> declaration
%nterm <DeclarationList*> declarations
%nterm <Declaration*> variable_declaration
%nterm <Declaration*> local_variable_declaration
%nterm <Declaration*> method_declaration


%nterm <DeclType*> type
%nterm <DeclType*> simple_type
%nterm <DeclType*> array_type
%nterm <DeclType*> type_identifier

%nterm <ExpressionList*> exprs
%nterm <Expression*> expr
%nterm <Expression*> method_invocation

%nterm <StatementList*> statement_list
%nterm <Statement*> statement


%nterm <FormalList*> formals
%nterm <Formal*> formal

%nterm <Lvalue*> lvalue


%printer { yyo << $$; } <*>;

%%
%start program;


program: class_declarations { $$ = new Program($1); driver.program = $$;}

/*main_class: "class" "identifier" "{"
			"public" "static" "void" "main" "(" ")"
			"{" statement_list "}"   "}"
			 { $$ = new MainClass($11); }
*/

class_declarations: %empty { $$ = new ClassDeclarationList(); }
		    | class_declarations class_declaration { $1->AddClassDeclaration($2); $$ = $1; }
		    ;

class_declaration:
    "class" "identifier" "extends" "identifier" "{" declarations "}"
    { $$ = new ClassDeclaration($2, $6); }
    | "class" "identifier" "{" declarations "}" { $$ = new ClassDeclaration($2, $4); $$->SetLocation(driver.location); }
    ;


declarations:
    %empty { $$ = new DeclarationList(); $$->SetLocation(driver.location); }
    | declarations declaration { $1->AddDeclaration($2); $$ = $1; $$->SetLocation(driver.location); }
    ;


declaration:
    variable_declaration { $$ = $1; $$->SetLocation(driver.location); }
    | method_declaration { $$ = $1; $$->SetLocation(driver.location); }
    ;


method_declaration:
    "public" "static" type "main" "("  ")" "{" statement_list "}"
    { $$ = new MethodDeclaration($3, "main", new FormalList(), $8);  $$->SetLocation(driver.location);}
    | "public" type "identifier" "(" formals ")" "{" statement_list "}"
    { $$ = new MethodDeclaration($2, $3, $5, $8); $$->SetLocation(driver.location); }
    | "public" type "identifier" "(" ")" "{" statement_list "}"
    { $$ = new MethodDeclaration($2, $3, new FormalList(), $7); $$->SetLocation(driver.location); }
    ;


variable_declaration: type "identifier" ";" { $$ = new VariableDeclaration($1, $2);  $$->SetLocation(driver.location); }


formals:
    formal { $$ = new FormalList($1);  $$->SetLocation(driver.location);  }
    | formals "," formal { $1->AddFormal($3); $$ = $1;  $$->SetLocation(driver.location);  }
    ;


formal: type "identifier" { $$ = new Formal($1, $2);  $$->SetLocation(driver.location);  }


type:
    simple_type { $$ = $1;  $$->SetLocation(driver.location);  }
    | array_type { $$ = $1;  $$->SetLocation(driver.location);  }
    ;

simple_type:
    "int" { $$ = new IntegerType();  $$->SetLocation(driver.location);  }
    | "boolean" { $$ = new BoolType();  $$->SetLocation(driver.location);  }
    | "void" { $$ = new VoidType();  $$->SetLocation(driver.location);  }
    | type_identifier { $$ = $1;  $$->SetLocation(driver.location);  }
    ;


array_type: simple_type "[" "]" { $$ = new ArrayType($1);  $$->SetLocation(driver.location);  }


type_identifier: "identifier" { $$ = new ClassType($1);  $$->SetLocation(driver.location);  }


statement_list:
    %empty { $$ = new StatementList();  $$->SetLocation(driver.location);  }
    | statement_list statement { $1->AddStatement($2); $$ = $1;  $$->SetLocation(driver.location);  }
    ;

statement:
    lvalue "=" expr ";" { $$ = new Assignment($1, $3);  $$->SetLocation(driver.location);  }
    | "System.out.println" "(" expr ")" ";" { $$ = new Println($3);  $$->SetLocation(driver.location);  }
    | "return" expr ";" { $$ = new ReturnStatement($2);  $$->SetLocation(driver.location);  }
    | "assert" "(" expr ")" { $$ = new AssertStatement($3);  $$->SetLocation(driver.location);  }
    | local_variable_declaration { $$ = new LocalVariableDeclaration($1); $$->SetLocation(driver.location); }
    | "identifier" "[" "]" "identifier" ";" { $$ = new ArrayDecl($1, $4); std::cout << "Found Array";  $$->SetLocation(driver.location); }
    | "{" statement_list "}" { $$ = new ScopeStatement($2);  $$->SetLocation(driver.location);  }
    | "if"  "(" expr ")" statement { $$ = new ConditionalStatement($3, $5);  $$->SetLocation(driver.location);  }
    | "if"  "(" expr ")" statement "else" statement { $$ = new ComplexConditionalStatement($3, $5, $7);  $$->SetLocation(driver.location);  }
    | "while"  "(" expr ")" statement { $$ = new WhileStatement($3, $5);  $$->SetLocation(driver.location);  }
    | method_invocation ";" { $$ = new MethodInvocationStatement($1);  $$->SetLocation(driver.location);  }
    ;


local_variable_declaration: variable_declaration { $$ = $1;  $$->SetLocation(driver.location);  };

method_invocation:
    expr "." "identifier" "(" ")" { $$ = new MethodInvocation($1, $3);  $$->SetLocation(driver.location);  }
    | expr "." "identifier" "(" exprs ")" { $$ = new MethodInvocation($1, $3, $5);  $$->SetLocation(driver.location);  }
    ;

exprs:
    expr { $$ = new ExpressionList($1);  $$->SetLocation(driver.location);  }
    | exprs "," expr { $1->AddExpression($3); $$ = $1;  $$->SetLocation(driver.location);  }
    ;

lvalue:
    "identifier" { $$ = new SimpleLvalue($1); $$->SetLocation(driver.location);  }
    | "identifier" "[" expr "]" { $$ = new ArrayElementReference($1, $3);  $$->SetLocation(driver.location);  }
    ;


%left "<" "==";
%left "+" "-";
%left "*" "/";
%left "!";


expr:
    "number" { $$ = new NumberExpression($1);  $$->SetLocation(driver.location);  }
    | "identifier" { driver.variables[$1]; $$ = new IdentExpression($1);  $$->SetLocation(driver.location);  }
    | expr "+" expr { $$ = new AddExpression($1, $3);  $$->SetLocation(driver.location);  }
    | expr "-" expr { $$ = new SubExpression($1, $3);  $$->SetLocation(driver.location);  }
    | expr "*" expr { $$ = new MulExpression($1, $3);  $$->SetLocation(driver.location);  }
    | expr "<" expr { $$ = new LessExpression($1, $3);  $$->SetLocation(driver.location);  }
    | expr "==" expr { $$ = new EquelExpression($1, $3);  $$->SetLocation(driver.location);  }

//    | expr "&&" expr { $$ = $1 && $3;  $$->SetLocation(driver.location);  }
//    | expr "||" expr { $$ = $1 || $3;  $$->SetLocation(driver.location);  }
//    | expr ">" expr { $$ = $1 > $3;  $$->SetLocation(driver.location);  }
//    | expr "/" expr { $$ = $1 / $3;  $$->SetLocation(driver.location);  }
//    | expr "%" expr { $$ = $1 % $3;  $$->SetLocation(driver.location);  }

    | "!" expr   { $$ = new NegExpression($2);  $$->SetLocation(driver.location);  }
    | "(" expr ")" { $$ = new BracedExpression($2);  $$->SetLocation(driver.location);  }
    | "true"  { $$ = new BoolConstantExpression(1);  $$->SetLocation(driver.location);  }
    | "false"  { $$ = new BoolConstantExpression(0);  $$->SetLocation(driver.location);  }


    | expr "[" expr "]" { $$ = new ArrayElement($1, $3);  $$->SetLocation(driver.location);  }
    | expr "." "length" { $$ = new ArrayLength($1);  $$->SetLocation(driver.location);  }
    | "new" simple_type "[" expr "]" { $$ = new CreateArray($2, $4);  $$->SetLocation(driver.location);  }
    | "new" type_identifier "(" ")"  { $$ = new CreateObject($2);  $$->SetLocation(driver.location);  }
    | "this"  { $$ = new ThisExpression();  $$->SetLocation(driver.location);  }
    | method_invocation  { $$ = $1;  $$->SetLocation(driver.location);  }

    ;
%%

void
yy::parser::error(const location_type& l, const std::string& m)
{
  std::cerr << l << ": " << m << '\n';
}
