#include "ExpressionList.h"

ExpressionList::ExpressionList(Expression* expr) {
  AddExpression(expr);
}

void ExpressionList::AddExpression(Expression *expr) {
  expr_list.emplace_back(expr);
}

void ExpressionList::Accept(Visitor *visitor) {
  visitor->Visit(this);
}
