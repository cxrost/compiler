#pragma once

#include "expressions/Expression.h"

class BoolConstantExpression : public Expression {
 public:
  BoolConstantExpression(int value);
  void Accept(Visitor* visitor) override;
  int value;
};
