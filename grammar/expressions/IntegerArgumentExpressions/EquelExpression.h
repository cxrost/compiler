#pragma once

#include "expressions/Expression.h"

class EquelExpression : public Expression {
 public:
  EquelExpression(Expression* e1, Expression* e2);
  void Accept(Visitor* visitor) override;
  Expression* first;
  Expression* second;
};
