#pragma once

#include "expressions/Expression.h"

class NegExpression : public Expression {
 public:
  NegExpression(Expression* e);
  void Accept(Visitor* visitor) override;
  Expression* expression;
};
