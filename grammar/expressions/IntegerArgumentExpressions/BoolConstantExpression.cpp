#include "BoolConstantExpression.h"

BoolConstantExpression::BoolConstantExpression(int value) : value(value) {}


void BoolConstantExpression::Accept(Visitor *visitor) {
  visitor->Visit(this);
}
