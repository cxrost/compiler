#include "SubExpression.h"

SubExpression::SubExpression(Expression *e1, Expression *e2) : first(e1), second(e2) {}


void SubExpression::Accept(Visitor *visitor) {
  visitor->Visit(this);
}
