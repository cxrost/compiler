#include "EquelExpression.h"

EquelExpression::EquelExpression(Expression *e1, Expression *e2) : first(e1), second(e2) {}


void EquelExpression::Accept(Visitor *visitor) {
  visitor->Visit(this);
}
