#include "LessExpression.h"

LessExpression::LessExpression(Expression *e1, Expression *e2) : first(e1), second(e2) {}


void LessExpression::Accept(Visitor *visitor) {
  visitor->Visit(this);
}
