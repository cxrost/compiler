#pragma once

#include "expressions/Expression.h"

class NumberExpression : public Expression {
 public:
  explicit NumberExpression(int value);
  void Accept(Visitor* visitor) override;
  int value;
};
