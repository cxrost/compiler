#pragma once

#include "expressions/Expression.h"

class SubExpression : public Expression {
 public:
  SubExpression(Expression* e1, Expression* e2);
  void Accept(Visitor* visitor) override;
  Expression* first;
  Expression* second;
};
