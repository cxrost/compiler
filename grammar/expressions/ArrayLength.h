#pragma once

#include "Expression.h"

class ArrayLength : public Expression {
 public:
  ArrayLength(Expression* obj);
  void Accept(Visitor* visitor) override;
  Expression* obj;
};
