#pragma once

#include "Expression.h"
#include <string>

class IdentExpression : public Expression {
 public:
  explicit IdentExpression(std::string identifier);
  void Accept(Visitor* visitor) override;
  std::string identifier;
};
