#pragma once

#include "Expression.h"

class MethodInvocation : public Expression {
 public:
  MethodInvocation(Expression* this_expr, std::string identifier, ExpressionList* args);
  MethodInvocation(Expression* this_expr, std::string identifier);
  void Accept(Visitor* visitor) override;
  Expression* this_expr;
  std::string name;
  ExpressionList* args{nullptr};
};
