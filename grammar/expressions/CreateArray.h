#pragma once

#include "Expression.h"

class CreateArray : public Expression {
 public:
  CreateArray(DeclType* type, Expression * size);
  void Accept(Visitor* visitor) override;
  DeclType* type;
  Expression* size;
};
