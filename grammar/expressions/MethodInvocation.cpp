#include "MethodInvocation.h"

MethodInvocation::MethodInvocation(Expression *this_expr, std::string identifier)
    : this_expr(this_expr), name(identifier) {}

MethodInvocation::MethodInvocation(Expression *this_expr, std::string identifier, ExpressionList* args)
    : this_expr(this_expr), name(identifier), args(args) {}


void MethodInvocation::Accept(Visitor *visitor) {
  visitor->Visit(this);
}
