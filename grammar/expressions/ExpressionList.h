#pragma once

#include "base_elements/BaseElement.h"

class ExpressionList : public BaseElement {
 public:
  ExpressionList(Expression* expr);
  void AddExpression(Expression* expr);
  void Accept(Visitor* visitor) override;
  std::vector<Expression*> expr_list;
};
