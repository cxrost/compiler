#include "ArrayElement.h"

ArrayElement::ArrayElement(Expression *obj, Expression *index) : obj(obj), index(index) {}

void ArrayElement::Accept(Visitor *visitor) {
  visitor->Visit(this);
}
