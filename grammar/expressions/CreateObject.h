#pragma once

#include "Expression.h"

class CreateObject : public Expression {
 public:
  CreateObject(DeclType* type);
  void Accept(Visitor* visitor) override;
  DeclType* type;
};
