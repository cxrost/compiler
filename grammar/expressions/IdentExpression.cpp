#include "IdentExpression.h"

IdentExpression::IdentExpression(std::string identifier) : identifier(identifier) {}

void IdentExpression::Accept(Visitor *visitor) {
  visitor->Visit(this);
}
