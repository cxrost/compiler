#pragma once

#include "Expression.h"

class BracedExpression : public Expression {
 public:
  BracedExpression(Expression* e);
  void Accept(Visitor* visitor) override;
  Expression* expression;
};
