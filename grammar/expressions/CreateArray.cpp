#include "CreateArray.h"

CreateArray::CreateArray(DeclType *type, Expression *size) : type(type), size(size) {}

void CreateArray::Accept(Visitor *visitor) {
  visitor->Visit(this);
}
