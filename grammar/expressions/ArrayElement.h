#pragma once

#include "Expression.h"

class ArrayElement : public Expression {
 public:
  ArrayElement(Expression* obj, Expression* index);
  void Accept(Visitor* visitor) override;
  Expression* obj;
  Expression* index;
};
