#include "CreateObject.h"

CreateObject::CreateObject(DeclType *type) : type(type) {}

void CreateObject::Accept(Visitor *visitor) {
  visitor->Visit(this);
}
