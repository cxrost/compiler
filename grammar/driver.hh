#pragma once

#include <map>
#include <string>
#include <fstream>
#include "grammar/scanner.h"
#include "grammar/parser.hh"
#include "visitors/PrintVisitor.h"
#include "visitors/Interpreter.h"
#include "visitors/FunctionCallVisitor.h"
#include "visitors/SymbolTreeVisitor.h"
#include "visitors/TypeVisitor.h"

class Driver {
public:
  Driver();
  void PrintAST(const std::string& filename);


  std::map<std::string, int> variables;
  int result;
  int parse(const std::string& f);
  std::string file;
  bool trace_parsing;

  void scan_begin();
  void scan_end();

  bool trace_scanning;
  yy::location location;

  friend class Scanner;
  Scanner scanner;
  yy::parser parser;

  Program* program;
private:
  void InitMainFrame(Frame& frame);
  std::ifstream stream;

};
