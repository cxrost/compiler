%{
    #include <cerrno>
    #include <climits>
    #include <cstdlib>
    #include <cstring> // strerror
    #include <string>
    #include <iostream>
    #include "driver.hh"
    #include "parser.hh"
%}

%option noyywrap nounput noinput batch debug

%option c++
%option yyclass="Scanner"

%{
  // A number symbol corresponding to the value in S.
  yy::parser::symbol_type make_NUMBER(
    const std::string &s,
    const yy::parser::location_type& loc
  );
%}

id    [a-zA-Z][a-zA-Z_0-9]*
int   [0-9]+
blank [ \t\r]

%{
  // Code run each time a pattern is matched.
  # define YY_USER_ACTION  loc.columns (yyleng);
%}
%%
%{
  // A handy shortcut to the location held by the driver.
  yy::location& loc = driver.location;
  // Code run each time yylex is called.
  loc.step();
%}

{blank}+   loc.step ();
\n+        loc.lines (yyleng); loc.step ();

"-"        return yy::parser::make_MINUS  (loc);
"+"        return yy::parser::make_PLUS   (loc);
"*"        return yy::parser::make_STAR   (loc);
"/"        return yy::parser::make_SLASH  (loc);
"%"        return yy::parser::make_PERCENT  (loc);
"&&"       return yy::parser::make_LOGIC_AND  (loc);
"||"       return yy::parser::make_LOGIC_OR  (loc);
"<"        return yy::parser::make_LESS_THAN  (loc);
">"        return yy::parser::make_GREATER_THAN  (loc);
"=="       return yy::parser::make_EQUEL  (loc);
"!"        return yy::parser::make_NEG  (loc);

"("        return yy::parser::make_LPAREN (loc);
")"        return yy::parser::make_RPAREN (loc);
"{"        return yy::parser::make_LPAREN_FIG (loc);
"}"        return yy::parser::make_RPAREN_FIG (loc);
"["        return yy::parser::make_LPAREN_SQR (loc);
"]"        return yy::parser::make_RPAREN_SQR (loc);
"="        return yy::parser::make_ASSIGN  (loc);
";"	   return yy::parser::make_SEMICOLON (loc);
","	   return yy::parser::make_COMMA (loc);
"."	   return yy::parser::make_POINT (loc);

"class"	   return yy::parser::make_KEY_W_CLASS (loc);
"if"	   return yy::parser::make_KEY_W_IF (loc);
"else"	   return yy::parser::make_KEY_W_ELSE (loc);
"while"	   return yy::parser::make_KEY_W_WHILE (loc);
"public"   return yy::parser::make_KEY_W_PUBLIC (loc);
"static"   return yy::parser::make_KEY_W_STATIC (loc);
"main"     return yy::parser::make_KEY_W_MAIN (loc);
"System.out.println" return yy::parser::make_KEY_W_PRINT (loc);
"assert"   return yy::parser::make_KEY_W_ASSERT (loc);
"return"   return yy::parser::make_KEY_W_RETURN (loc);
"extends"  return yy::parser::make_KEY_W_EXTENDS (loc);
"length"   return yy::parser::make_KEY_W_LENGTH (loc);
"new"      return yy::parser::make_KEY_W_NEW (loc);

"void"     return yy::parser::make_KEY_W_VOID (loc);
"int"      return yy::parser::make_KEY_W_INT (loc);
"boolean"  return yy::parser::make_KEY_W_BOOLEAN (loc);
"true"     return yy::parser::make_KEY_W_TRUE (loc);
"false"    return yy::parser::make_KEY_W_FALSE (loc);
"this"     return yy::parser::make_KEY_W_THIS (loc);

{int}      return make_NUMBER(yytext, loc);
{id}       return yy::parser::make_IDENTIFIER(yytext, loc);
.          {
                throw yy::parser::syntax_error(loc, "invalid character: " + std::string(yytext));
           }
<<EOF>>    return yy::parser::make_END (loc);
%%

yy::parser::symbol_type make_NUMBER(
  const std::string &s,
  const yy::parser::location_type& loc
) {
  errno = 0;
  long n = strtol(s.c_str(), NULL, 10);
  if (! (INT_MIN <= n && n <= INT_MAX && errno != ERANGE))
    throw yy::parser::syntax_error(loc, "integer is out of range: " + s);
  return yy::parser::make_NUMBER((int) n, loc);
}