#pragma once

#include "Statement.h"

class ConditionalStatement : public Statement {
 public:
  ConditionalStatement(Expression *expr, Statement *statement);
  void Accept(Visitor *visitor) override;
  Expression *expr;
  Statement *statement;
};
