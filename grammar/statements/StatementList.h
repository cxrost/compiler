#pragma once

#include "Statement.h"
#include <vector>

class StatementList : public Statement {
 public:
  StatementList() = default;
  void AddStatement(Statement* st);
  void Accept(Visitor* visitor) override;
  std::vector<Statement*> statements;
};
