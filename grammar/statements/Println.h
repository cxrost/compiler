#pragma once

#include "Statement.h"

class Println : public Statement {
 public:
  Println(Expression* expr);
  void Accept(Visitor* visitor) override;
  Expression* expr;
};
