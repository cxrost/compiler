#include "AssertStatement.h"

AssertStatement::AssertStatement(Expression *expr) : expr(expr) {}


void AssertStatement::Accept(Visitor *visitor) {
  visitor->Visit(this);
}
