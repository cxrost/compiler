#pragma once

#include "Statement.h"

class AssertStatement : public Statement {
 public:
  AssertStatement(Expression* expr);
  void Accept(Visitor* visitor) override;
  Expression* expr;
};
