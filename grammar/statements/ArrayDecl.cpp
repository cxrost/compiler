#include "ArrayDecl.h"

ArrayDecl::ArrayDecl(std::string type, std::string identifier) : type(type), identifier(identifier) {}

void ArrayDecl::Accept(Visitor *visitor) {
  visitor->Visit(this);
}
