#pragma once

#include "Statement.h"

class Assignment : public Statement {
 public:
  Assignment(Lvalue* lvalue, Expression* expr);
  void Accept(Visitor* visitor) override;
  Lvalue* lvalue;
  Expression* expr;
};
