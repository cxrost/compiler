#include "ReturnStatement.h"

ReturnStatement::ReturnStatement(Expression *expr) : expr(expr) {}

void ReturnStatement::Accept(Visitor *visitor) {
  visitor->Visit(this);
}
