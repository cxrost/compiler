#include "WhileStatement.h"

WhileStatement::WhileStatement(Expression *expr, Statement* statement) : expr(expr), statement(statement) {}

void WhileStatement::Accept(Visitor *visitor) {
  visitor->Visit(this);
}
