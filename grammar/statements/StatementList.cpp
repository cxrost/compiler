#include "StatementList.h"

void StatementList::AddStatement(Statement *st) {
  statements.emplace_back(st);
}

void StatementList::Accept(Visitor *visitor) {
  visitor->Visit(this);
}
