#pragma once

#include "Statement.h"

class ComplexConditionalStatement : public Statement {
 public:
  ComplexConditionalStatement(Expression* expr, Statement* true_st, Statement* false_st);
  void Accept(Visitor* visitor) override;
  Expression* expr;
  Statement* true_st;
  Statement* false_st;
};
