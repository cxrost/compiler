

#include "Statement.h"

class ArrayDecl : public Statement {
 public:
  ArrayDecl(std::string type, std::string identifier);
  void Accept(Visitor* visitor) override;
  std::string type;
  std::string identifier;
};
