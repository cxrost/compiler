#include "MethodInvocationStatement.h"

MethodInvocationStatement::MethodInvocationStatement(Expression *expr) : expr(expr) {}

void MethodInvocationStatement::Accept(Visitor *visitor) {
  visitor->Visit(this);
}
