#pragma once

#include "Statement.h"

class ReturnStatement : public Statement {
 public:
  ReturnStatement(Expression* expr);
  void Accept(Visitor* visitor) override;
  Expression* expr;
};
