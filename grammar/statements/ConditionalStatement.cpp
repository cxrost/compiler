#include "ConditionalStatement.h"

ConditionalStatement::ConditionalStatement(Expression *expr, Statement *statement)
: expr(expr), statement(statement) {}

void ConditionalStatement::Accept(Visitor *visitor) {
  visitor->Visit(this);
}
