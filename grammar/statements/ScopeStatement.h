#pragma once

#include "Statement.h"

class ScopeStatement : public Statement {
 public:
  ScopeStatement(StatementList* st_list);
  void Accept(Visitor* visitor) override;
  StatementList* st_list;
};
