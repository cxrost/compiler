#pragma once

#include "Statement.h"

class WhileStatement : public Statement {
 public:
  WhileStatement(Expression* expr, Statement* statement);
  void Accept(Visitor* visitor) override;
  Expression* expr;
  Statement *statement;
};
