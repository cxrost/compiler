#include "Assignment.h"

Assignment::Assignment(Lvalue *lvalue, Expression *expr) : lvalue(lvalue), expr(expr) {}


void Assignment::Accept(Visitor *visitor) {
  visitor->Visit(this);
}
