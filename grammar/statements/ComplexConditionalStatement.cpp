#include "ComplexConditionalStatement.h"

ComplexConditionalStatement::ComplexConditionalStatement(Expression *expr, Statement *true_st, Statement *false_st)
    : expr(expr), true_st(true_st), false_st(false_st) {}

void ComplexConditionalStatement::Accept(Visitor *visitor) {
  visitor->Visit(this);
}
