#include "ScopeStatement.h"

ScopeStatement::ScopeStatement(StatementList *st_list) : st_list(st_list) {}

void ScopeStatement::Accept(Visitor *visitor) {
  visitor->Visit(this);
}
