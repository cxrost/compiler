#pragma once


#include "Statement.h"

class LocalVariableDeclaration : public Statement {
 public:
  LocalVariableDeclaration(Declaration* var_decl);
  void Accept(Visitor* visitor) override;
  Declaration* var_decl;
};
