#pragma once

#include "Statement.h"

class MethodInvocationStatement : public Statement {
 public:
  MethodInvocationStatement(Expression* expr);
  void Accept(Visitor* visitor) override;
  Expression* expr;
};
