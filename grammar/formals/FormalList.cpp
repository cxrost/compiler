#include "FormalList.h"

FormalList::FormalList(Formal *formal) {
  AddFormal(formal);
}

void FormalList::AddFormal(Formal* formal) {
  formal_list.emplace_back(formal);
}

void FormalList::Accept(Visitor *visitor) {
  visitor->Visit(this);
}
