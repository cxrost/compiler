#pragma once

#include "base_elements/BaseElement.h"

class FormalList : public BaseElement {
 public:
  FormalList() = default;
  FormalList(Formal* formal);
  void AddFormal(Formal* formal);
  void Accept(Visitor* visitor) override;
  std::vector<Formal*> formal_list;
};
