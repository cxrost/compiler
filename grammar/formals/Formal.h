#pragma once

#include "base_elements/BaseElement.h"

class Formal : public BaseElement {
 public:
  Formal(DeclType* type, std::string identifier);
  void Accept(Visitor* visitor) override;
  DeclType* type;
  std::string name;
};
