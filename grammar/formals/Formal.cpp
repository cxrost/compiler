#include "Formal.h"

Formal::Formal(DeclType *type, std::string identifier) : type(type), name(identifier) {}


void Formal::Accept(Visitor *visitor) {
  visitor->Visit(this);
}
