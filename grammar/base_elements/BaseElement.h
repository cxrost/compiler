#pragma once

#include "visitors/Visitor.h"
#include <vector>
#include <string>
#include <scanner.h>

class BaseElement {
 public:
  virtual void Accept(Visitor* visitor) = 0;
  virtual ~BaseElement() = default;
  virtual void SetLocation(yy::parser::location_type loc_) {
    loc = loc_;
  }
  yy::parser::location_type loc;
};
