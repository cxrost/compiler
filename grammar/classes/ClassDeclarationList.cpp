#include "ClassDeclarationList.h"

void ClassDeclarationList::AddClassDeclaration(ClassDeclaration* class_decl) {
  class_decls.emplace_back(class_decl);
}


void ClassDeclarationList::Accept(Visitor *visitor) {
  visitor->Visit(this);
}
