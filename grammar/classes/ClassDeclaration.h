#pragma once

#include <string>
#include "declarations/DeclarationList.h"

class ClassDeclaration : public BaseElement {
 public:
  ClassDeclaration(std::string name, DeclarationList* decls);
  void Accept(Visitor* visitor) override;
  DeclarationList* decls;
  std::string name;
};
