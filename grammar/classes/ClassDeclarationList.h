#pragma once

#include "ClassDeclaration.h"
#include <vector>

class ClassDeclarationList : public BaseElement {
 public:
  void AddClassDeclaration(ClassDeclaration* class_decl);
  void Accept(Visitor* visitor) override;
  std::vector<ClassDeclaration*> class_decls;
};
