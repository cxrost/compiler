#pragma once

#include "classes/ClassDeclarationList.h"
#include "base_elements/BaseElement.h"

class Program : public BaseElement {
 public:
  Program(ClassDeclarationList* class_decls);
  void Accept(Visitor* visitor) override;
  ClassDeclarationList* class_decls;
};
