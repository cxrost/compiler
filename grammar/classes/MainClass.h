#pragma once

#include "base_elements/BaseElement.h"

class MainClass : public BaseElement {
 public:
  MainClass(StatementList* st_list);
  void Accept(Visitor* visitor) override;
  StatementList* st_list;
};
