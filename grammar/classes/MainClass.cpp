#include "MainClass.h"

MainClass::MainClass(StatementList* st_list) : st_list(st_list) {}

void MainClass::Accept(Visitor *visitor) {
  visitor->Visit(this);
}
