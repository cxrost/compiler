#include "ClassDeclaration.h"

ClassDeclaration::ClassDeclaration(std::string name, DeclarationList *decls) : name(name), decls(decls) {}

void ClassDeclaration::Accept(Visitor *visitor) {
  visitor->Visit(this);
}
