#include "Program.h"

Program::Program(ClassDeclarationList* class_decls) : class_decls(class_decls) {}

void Program::Accept(Visitor *visitor) {
  visitor->Visit(this);
}
