#include "ArrayElementReference.h"

ArrayElementReference::ArrayElementReference(std::string identifier, Expression *index)
    : name(identifier), index(index) {}

void ArrayElementReference::Accept(Visitor *visitor) {
  visitor->Visit(this);
}
