#pragma once

#include <string>
#include "Lvalue.h"

class SimpleLvalue : public Lvalue {
 public:
  SimpleLvalue(std::string ident);
  void Accept(Visitor* visitor) override;
  std::string ident;
};
