#include "SimpleLvalue.h"

SimpleLvalue::SimpleLvalue(std::string ident) : ident(std::move(ident)) {}


void SimpleLvalue::Accept(Visitor *visitor) {
  visitor->Visit(this);
}
