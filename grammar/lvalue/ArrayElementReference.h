#pragma once

#include "Lvalue.h"

class ArrayElementReference : public Lvalue {
 public:
  ArrayElementReference(std::string identifier, Expression* index);
  void Accept(Visitor* visitor) override;
  std::string name;
  Expression* index;
};
