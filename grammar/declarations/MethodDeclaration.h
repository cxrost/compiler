#pragma once

#include <formals/FormalList.h>
#include "Declaration.h"

class MethodDeclaration : public Declaration {
 public:
  MethodDeclaration(DeclType* type, std::string name, FormalList* formals, StatementList* st_list);
  void Accept(Visitor* visitor) override;
  DeclType* type;
  std::string name;
  FormalList* formals;
  StatementList* st_list;
};
