#include "MethodDeclaration.h"

MethodDeclaration::MethodDeclaration(DeclType *type, std::string name, FormalList *formals, StatementList *st_list)
    : type(type), name(name), formals(formals), st_list(st_list) {}

void MethodDeclaration::Accept(Visitor *visitor) {
  visitor->Visit(this);
}

