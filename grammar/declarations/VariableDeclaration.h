#pragma once

#include "declarations/Declaration.h"

class VariableDeclaration : public Declaration {
 public:
  VariableDeclaration(DeclType* type, std::string identifier);
  void Accept(Visitor* visitor) override;
  DeclType* type;
  std::string identifier;
};
