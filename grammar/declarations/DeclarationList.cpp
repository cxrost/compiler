#include "DeclarationList.h"

void DeclarationList::AddDeclaration(Declaration *decl) {
  decls.emplace_back(decl);
}


void DeclarationList::Accept(Visitor *visitor) {
  visitor->Visit(this);
}
