#pragma once

#include "Declaration.h"
#include <vector>

class DeclarationList : public BaseElement {
 public:
  void AddDeclaration(Declaration* decl);
  void Accept(Visitor* visitor) override;
  std::vector<Declaration*> decls;
};
