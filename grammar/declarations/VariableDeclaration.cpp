#include "VariableDeclaration.h"

VariableDeclaration::VariableDeclaration(DeclType *type, std::string identifier) : type(type), identifier(identifier) {}

void VariableDeclaration::Accept(Visitor *visitor) {
  visitor->Visit(this);
}
