#pragma once

#include "SimpleType.h"

class ClassType : public SimpleType {
 public:
  ClassType(std::string identifier);
  void Accept(Visitor* visitor) override;
  std::string name;
};
