#pragma once

#include "SimpleType.h"

class VoidType : public SimpleType {
 public:
  void Accept(Visitor* visitor) override;
};
