#include "ClassType.h"

ClassType::ClassType(std::string identifier) : name(identifier){}

void ClassType::Accept(Visitor *visitor) {
  visitor->Visit(this);
}
