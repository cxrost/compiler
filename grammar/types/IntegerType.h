#pragma once

#include "types/SimpleType.h"

class IntegerType : public SimpleType {
 public:
  IntegerType();
  void Accept(Visitor* visitor) override;
};
