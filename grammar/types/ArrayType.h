#pragma once

#include "DeclType.h"

class ArrayType : public DeclType {
 public:
  ArrayType(DeclType*  type);
  void Accept(Visitor* visitor) override;
  DeclType* simple_type;
};
