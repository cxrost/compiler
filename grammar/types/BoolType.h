#pragma once

#include "SimpleType.h"

class BoolType : public SimpleType {
 public:
  void Accept(Visitor* visitor) override;
};
