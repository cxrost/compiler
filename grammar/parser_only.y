%skeleton "lalr1.cc"
%require "3.5"

%defines
%define api.token.constructor
%define api.value.type variant
%define parse.assert

%code requires {
    #include <string>
    #include "visitors/forward_decl.h"

    class Scanner;
    class Driver;
}

// %param { Driver &drv }

%define parse.trace
%define parse.error verbose

%code {
    #include "driver.hh"
    #include "location.hh"
    #include "visitors/elements.h"

    static yy::parser::symbol_type yylex(Scanner &scanner, Driver& driver) {
        return scanner.ScanToken();
    }

    void Create_UB(std::string reason) {
    	std::cout << "Sorry, on this step \"" << reason << "\" is not availible" << std::endl;
    }
}

%lex-param { Scanner &scanner }
%lex-param { Driver &driver }
%parse-param { Scanner &scanner }
%parse-param { Driver &driver }

%locations

%define api.token.prefix {TOK_}

%token
    END 0 "end of file"
    ASSIGN "="
    MINUS "-"
    PLUS "+"
    STAR "*"
    PERCENT "%"
    LOGIC_AND "&&"
    LOGIC_OR "||"
    LESS_THAN "<"
    GREATER_THAN ">"
    EQUEL "=="
    NEG "!"

    SLASH "/"
    LPAREN "("
    RPAREN ")"
    LPAREN_FIG "{"
    RPAREN_FIG "}"
    LPAREN_SQR "["
    RPAREN_SQR "]"
    SEMICOLON ";"
    COMMA ","
    POINT "."



    KEY_W_CLASS "class"
    KEY_W_IF "if"
    KEY_W_ELSE "else"
    KEY_W_WHILE "while"
    KEY_W_PUBLIC "public"
    KEY_W_STATIC "static"
    KEY_W_MAIN "main"
    KEY_W_PRINT "System.out.println"
    KEY_W_ASSERT "assert"
    KEY_W_RETURN "return"
    KEY_W_EXTENDS "extends"
    KEY_W_LENGTH "length"
    KEY_W_NEW "new"

    KEY_W_VOID "void"
    KEY_W_INT "int"
    KEY_W_BOOLEAN "boolean"
    KEY_W_TRUE "true"
    KEY_W_FALSE "false"
    KEY_W_THIS "this"
;

%token <std::string> IDENTIFIER "identifier"
%token <int> NUMBER "number"


%nterm <Program*> program
%nterm <MainClass*> main_class
%nterm <ClassDeclarationList*> class_declarations
%nterm <ClassDeclaration*> class_declaration
%nterm <Declaration*> declaration
%nterm <DeclarationList*> declarations
%nterm <Declaration*> variable_declaration
%nterm <Declaration*> local_variable_declaration
%nterm <Declaration*> method_declaration


%nterm <Type*> type
%nterm <Type*> simple_type
%nterm <Type*> array_type
%nterm <Type*> type_identifier

%nterm <ExpressionList*> exprs
%nterm <Expression*> expr
%nterm <Expression*> method_invocation

%nterm <StatementList*> statement_list
%nterm <Statement*> statement


%nterm <FormalList*> formals
%nterm <Formal*> formal

%nterm <Lvalue*> lvalue


%printer { yyo << $$; } <*>;

%%
%start program;


program: main_class class_declarations { /* $$ = new Program($1, $2); driver.program = $$;*/}

main_class: "class" "identifier" "{"
			"public" "static" "void" "main" "(" ")"
			"{" statement_list "}"   "}"
			 { /* $$ = new MainClass($11); */}


class_declarations: %empty { /* $$ = new ClassDeclarationList(); */}
		    | class_declarations class_declaration { /* $1->AddClassDeclaration($2); $$ = $1; */}
		    ;

class_declaration:
    "class" "identifier" "extends" "identifier" "{" declarations "}"
    { /* $$ = new ClassDeclaration($2, $6); */}
    | "class" "identifier" "{" declarations "}" { /* $$ = new ClassDeclaration($2, $4); */}
    ;


declarations:
    %empty { /* $$ = new DeclarationList(); */}
    | declarations declaration { /* $1->AddDeclaration($2); $$ = $1; */}
    ;


declaration:
    variable_declaration { /* $$ = $1; */}
    | method_declaration { /* $$ = $1; */}
    ;


method_declaration:
    "public" type "identifier" "(" formals ")" "{" statement_list "}"
    { /* $$ = new MethodDeclaration($2, $3, $5, $8); */}
    | "public" type "identifier" "(" ")" "{" statement_list "}"
    { /* $$ = new MethodDeclaration($2, $3, new FormalList(), $7); */}
    ;


variable_declaration: type "identifier" ";" { /* $$ = new VariableDeclaration($1, $2); */}


formals:
    formal { /* $$ = new FormalList($1); */}
    | formals "," formal { /* $1->AddFormal($3); $$ = $1; */}
    ;


formal: type "identifier" { /* $$ = new Formal($1, $2); */}


type:
    simple_type { /* $$ = $1; */}
    | array_type { /* $$ = $1; */}
    ;

simple_type:
    "int" { /* $$ = new IntegerType(); */}
    | "boolean" { /* $$ = new BoolType(); */}
    | "void" { /* $$ = new VoidType(); */}
    | type_identifier { /* $$ = $1; */}
    ;


array_type: simple_type "[" "]" { /* $$ = new ArrayType($1); */}


type_identifier: "identifier" { /* $$ = new ClassType($1); */}


statement_list:
    %empty { /* $$ = new StatementList(); */}
    | statement_list statement { /* $1->AddStatement($2); $$ = $1; */}
    ;

statement:
    lvalue "=" expr ";" { /* $$ = new Assignment($1, $3); */}
    | "System.out.println" "(" expr ")" ";" { /* $$ = new Println($3); */}
    | "return" expr ";" { /* $$ = new ReturnStatement($2); */}
    | "assert" "(" expr ")" { /* $$ = new AssertStatement($3); */}
    | local_variable_declaration { /* $$ = new LocalVariableDeclaration($1); */}
    | "{" statement_list "}" { /* $$ = new ScopeStatement($2); */}
    | "if"  "(" expr ")" statement { /* $$ = new ConditionalStatement($3, $5); */}
    | "if"  "(" expr ")" statement "else" statement { /* $$ = new ComplexConditionalStatement($3, $5, $7); */}
    | "while"  "(" expr ")" statement { /* $$ = new WhileStatement($3, $5); */}
    | method_invocation ";" { /* $$ = new MethodInvocationStatement($1); */}
    ;


local_variable_declaration: variable_declaration { /* $$ = $1; */};

method_invocation:
    expr "." "identifier" "(" ")" { /* $$ = new MethodInvocation($1, $3); */}
    | expr "." "identifier" "(" exprs ")" { /* $$ = new MethodInvocation($1, $3, $5); */}
    ;

exprs:
    expr { /* $$ = new ExpressionList($1); */}
    | exprs "," expr { /* $1->AddExpression($3); $$ = $1; */}
    ;

lvalue:
    "identifier" { /* $$ = new SimpleLvalue($1); */}
    | "identifier" "[" expr "]" { /* $$ = new ArrayElementReference($1, $3); */}
    ;


%left "<" "==";
%left "+" "-";
%left "*" "/";
%left "!";


expr:
    "number" { /* $$ = new NumberExpression($1); */}
    | "identifier" { /* driver.variables[$1]; $$ = new IdentExpression($1); */}
    | expr "+" expr { /* $$ = new AddExpression($1, $3); */}
    | expr "-" expr { /* $$ = new SubExpression($1, $3); */}
    | expr "*" expr { /* $$ = new MulExpression($1, $3); */}
    | expr "<" expr { /* $$ = new LessExpression($1, $3); */}
    | expr "==" expr { /* $$ = new EquelExpression($1, $3); */}

//    | expr "&&" expr { /* $$ = $1 && $3; */}
//    | expr "||" expr { /* $$ = $1 || $3; */}
//    | expr ">" expr { /* $$ = $1 > $3; */}
//    | expr "/" expr { /* $$ = $1 / $3; */}
//    | expr "%" expr { /* $$ = $1 % $3; */}

    | "!" expr   { /* $$ = new NegExpression($2); */}
    | "(" expr ")" { /* $$ = new BracedExpression($2); */}
    | "true"  { /* $$ = new BoolConstantExpression(1); */}
    | "false"  { /* $$ = new BoolConstantExpression(0); */}

    | expr "[" expr "]" { /* $$ = new ArrayElement($1, $3); */}
    | expr "." "length" { /* $$ = new ArrayLength($1); */}
    | "new" simple_type "[" expr "]" { /* $$ = new CreateArray($2, $4); */}
    | "new" type_identifier "(" ")"  { /* $$ = new CreateObject($2); */}
    | "this"  { /* $$ = new ThisExpression(); */}
    | method_invocation  { /* $$ = $1; */}

    ;
%%

void
yy::parser::error(const location_type& l, const std::string& m)
{
  std::cerr << l << ": " << m << '\n';
}
