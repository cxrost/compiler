#pragma once

#include <vector>
#include <unordered_map>
#include <unordered_set>
#include <memory>
#include <types/Type.h>
#include <types/Integer.h>
#include <types/ComplexType.h>
#include <grammar/declarations/MethodDeclaration.h>

class MethodDescription {
 public:
  MethodDescription(std::string class_name, std::string method_name) : class_name(class_name), method_name(method_name) {}

  void AddArg(const std::shared_ptr<Type>& type) {
    args.emplace_back(type);
  }

  void AddReturnType(const std::shared_ptr<Type>& type) {
    return_type = type;
  }

  void AddStartPoint(MethodDeclaration* decl_) {
    decl = decl_;
  }
  std::string class_name;
  std::string method_name;
  std::shared_ptr<Type> return_type;
  std::vector<std::shared_ptr<Type>> args;
  MethodDeclaration* decl;
};