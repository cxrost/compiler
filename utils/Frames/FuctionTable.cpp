//
// Created by pavel on 07.07.2020.
//

#include "FuctionTable.h"
void FunctionTable::Put(std::string var, int  value) {
  values_[var].pop();
  values_[var].push(value);
  symbols_.push(var);
}

void FunctionTable::CreateVariable(std::string var) {
  values_[var].push(0);
}

int FunctionTable::Get(std::string var) {
  return values_[var].top();
}

void FunctionTable::BeginScope() {
  symbols_.push("{");
}


void FunctionTable::EndScope() {
  while (symbols_.top() != "{") {
    std::string symbol = symbols_.top();

    std::cerr << "Popping " << symbol << std::endl;

    values_[symbol].pop();
    if (values_[symbol].empty()) {
      values_.erase(symbol);
    }
    symbols_.pop();
  }
  symbols_.pop();
}
