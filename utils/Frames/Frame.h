#pragma once

#include "utils/ClassDescription.h"
#include "types/Type.h"
#include "types/Integer.h"
#include "types/ComplexType.h"
#include "types/Object.h"
#include "memory"
#include "stack"

class Frame {
 public:
  Frame() {
    // That's function stack
    data = new int64_t[10];
    AllocScope();
  }

  ~Frame() {
    delete[] data;
  }

  void SetParams(const std::vector<std::shared_ptr<Object>> &values) {
    params_ = values;
  }

  int AllocVariable() {
    int64_t index = variables_.size();
    auto obj = std::make_shared<Object>();
    obj->SetAddr(data + index);
    variables_.push_back(obj);
    return index;
  }

  std::shared_ptr<Object> Get(int index) {
    if (index >= 0) {
      return variables_.at(index);
    } else {
      return params_.at(-index - 1);
    }
  }

  void Set(int index, std::shared_ptr<Object> value) {
    if (index >= 0) {
      variables_.at(index) = value;
    } else {
      params_.at(-index - 1) = value;
    }
  }

  void SetReturnValue(std::shared_ptr<Object> value) {
    returned = true;
    ret_value_ = value;
  }

  std::shared_ptr<Object> GetReturnValue() {
    return ret_value_;
  }

  void DeallocScope() {
    // TODO : destructors for pointers?!
    variables_.resize(offsets_.top());
    offsets_.pop();
  }

  void AllocScope() {
    offsets_.push(variables_.size());
  }

  std::stack<int> offsets_;
  std::vector<std::shared_ptr<Object>> params_;
  std::vector<std::shared_ptr<Object>> variables_;
  std::shared_ptr<Object> ret_value_;
  std::shared_ptr<ClassDescription> current_class;
  int64_t* data;
  bool returned{false};
};