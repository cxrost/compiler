//
// Created by pavel on 21.07.2020.
//
#include "IrUtils/visitors/IncludeNodes.h"
#include "AddressWithOffset.h"

IRT::AddressWithOffset::AddressWithOffset(IRT::Address *address, int offset) : address_(address), offset_(offset){
}

IRT::Expression *IRT::AddressWithOffset::ToExpr() {
  Expression* offset_expression;
  if (offset_ != 0) {
    offset_expression = new BinopExpr(
        IRT::BinOperator::PLUS,
        address_->ToExpr(),
        new ConstExpr(offset_)
    );
  } else {
    offset_expression = address_->ToExpr();
  }
  return new MemExpr(offset_expression);
}
