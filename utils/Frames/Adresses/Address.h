//
// Created by pavel on 21.07.2020.
//

#pragma once
#include <IrUtils/nodes/expressions/Expression.h>

namespace IRT {

class Address {
 public:
  virtual ~Address() = default;
  virtual IRT::Expression* ToExpr() = 0;
};

}
