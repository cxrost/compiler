//
// Created by pavel on 21.07.2020.
//
#pragma once

#include "Address.h"
#include "IrUtils/utils/Temporary.h"

namespace IRT {

class AddressAsRegister : public IRT::Address {
 public:
  explicit AddressAsRegister(Temporary temp);
  IRT::Expression * ToExpr() override;

 private:
  Temporary temp_;
};

}

