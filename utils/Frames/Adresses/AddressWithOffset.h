//
// Created by pavel on 21.07.2020.
//

#pragma once
#include "Address.h"

namespace IRT {

class AddressWithOffset : public Address {
 public:
  AddressWithOffset(Address* address, int offset);
  IRT::Expression * ToExpr() override;
 private:
  Address* address_;
  int offset_;
};

}