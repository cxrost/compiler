//
// Created by pavel on 21.07.2020.
//

#include "AddressAsRegister.h"
#include "IrUtils/nodes/expressions/TempExpr.h"

IRT::Expression *IRT::AddressAsRegister::ToExpr() {
  return new IRT::TempExpr(temp_);
}
IRT::AddressAsRegister::AddressAsRegister(IRT::Temporary temp) : temp_(temp){

}
