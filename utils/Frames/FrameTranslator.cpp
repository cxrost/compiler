//
// Created by pavel on 21.07.2020.
//

#include "Adresses/AddressAsRegister.h"
#include "Adresses/AddressWithOffset.h"

#include "FrameTranslator.h"
IRT::FrameTranslator::FrameTranslator(std::string name) : method_name_(name) {
  addresses_[frame_pointer_address_].push(new AddressAsRegister(Temporary(frame_pointer_address_)));
  addresses_[return_value_address_].push(new AddressAsRegister(Temporary(return_value_address_)));
}

void IRT::FrameTranslator::SetupScope() {
  symbols_.push("{");
}
void IRT::FrameTranslator::TearDownScope() {
  while (symbols_.top() != "{") {
    std::string symbol = symbols_.top();
    addresses_[symbol].pop();
    if (addresses_[symbol].empty()) {
      addresses_.erase(symbol);
    }
    symbols_.pop();
  }
  symbols_.pop();
}
void IRT::FrameTranslator::AddVariable(const std::string &name) {
  symbols_.push(name);
  if (addresses_.find(name) == addresses_.end()) {
    addresses_[name];
  }

  addresses_[name].push(
      new AddressWithOffset(FramePointer(), GetOffset())
  );
  IncOffset();
}

IRT::Address *IRT::FrameTranslator::GetAddress(const std::string &name) {
  return addresses_[name].top();
}



void IRT::FrameTranslator::AddReturnAddress() {
  AddVariable(return_address_);
}
IRT::Address *IRT::FrameTranslator::GetReturnValueAddress() {
  return  GetAddress(return_value_address_);
}


IRT::Address *IRT::FrameTranslator::FramePointer() {
  return addresses_[frame_pointer_address_].top();
}
int IRT::FrameTranslator::GetOffset() {
  return max_offset_;
}
void IRT::FrameTranslator::IncOffset() {
  max_offset_ += word_size_;
}
void IRT::FrameTranslator::AddArgumentAddress(const std::string &name) {
  AddVariable(name);
}
void IRT::FrameTranslator::AddLocalVariable(const std::string &name) {
  AddVariable(name);
}
void IRT::FrameTranslator::AddPseudoArgument(const std::string &name, IRT::Address *address) {
  symbols_.push(name);
  if (addresses_.find(name) == addresses_.end()) {
    addresses_[name];
  }
  addresses_[name].push(address);
}









