//
// Created by pavel on 21.07.2020.
//

#include <string>
#include <stack>
#include <unordered_map>
#include "Adresses/Address.h"

namespace IRT {
class FrameTranslator {
 public:
  explicit FrameTranslator(std::string name);
  void SetupScope();
  void TearDownScope();

  void AddVariable(const std::string &name);
  void AddLocalVariable(const std::string &name);
  void AddReturnAddress();

  void AddArgumentAddress(const std::string &name);
  void AddPseudoArgument(const std::string &name, Address* address);

  Address *FramePointer();
  Address *GetAddress(const std::string &name);
  Address *GetReturnValueAddress();

 private:
  std::string return_address_ = "::return";
  std::string frame_pointer_address_ = "::fp";
  std::string return_value_address_ = "::return_value";

  std::unordered_map<std::string, std::stack<Address *> > addresses_;

  std::stack<std::string> symbols_;
  int word_size_ = 4;
  std::string method_name_;

  int GetOffset();
  void IncOffset();
  int max_offset_ = 0;
};

}