#pragma once
#include "types/Type.h"
#include "types/Object.h"
#include "memory"
#include "stack"
#include "unordered_map"

class FunctionTable {
 public:
  void Put(std::string var, int value);

  void CreateVariable(std::string var);


  int Get(std::string var);

  void BeginScope();
  void EndScope();

  std::unordered_map<std::string, std::stack<int>> values_;
  std::stack<std::string> symbols_;
};