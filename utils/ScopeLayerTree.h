#pragma once

#include "ScopeLayer.h"
#include <fstream>
#include <map>


class ScopeLayerTree {
 public:
  ScopeLayerTree() {
    root_ = new ScopeLayer;
  }

  ~ScopeLayerTree() {
    delete root_;
  }

  ScopeLayer* GetRoot() {
    return root_;
  }

  void AddMapping(std::string class_name, std::string func_name, ScopeLayer *layer) {
    function_scope_map_[std::make_pair(class_name, func_name)] = layer;
  }

  ScopeLayer* GetFunctionScopeByName(std::string class_name, std::string func_name) {
    return function_scope_map_[std::make_pair(class_name, func_name)];
  }

  std::map<std::pair<std::string, std::string>, ScopeLayer*> function_scope_map_;
  ScopeLayer* root_;
};