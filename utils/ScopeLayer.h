#pragma once

#include "string"
#include "memory"
#include "vector"
#include "unordered_map"
#include "types/Object.h"

class ScopeLayer {
 public:
  ScopeLayer() = default;
  explicit ScopeLayer(ScopeLayer* parent);
  ~ScopeLayer();
  void DeclareVariable(const std::string& symbol, std::shared_ptr<Object> value);

  std::shared_ptr<Object> Get(const std::string& symbol);
  bool Has(const std::string& symbol);

  void AddChild(ScopeLayer* child);

  ScopeLayer* GetParent() const;

 private:
  std::unordered_map<std::string, std::shared_ptr<Object>> values_;
  ScopeLayer* parent_{nullptr};
  std::vector<ScopeLayer* > children_;
};
