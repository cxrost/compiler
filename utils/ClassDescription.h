#pragma once

#include <utility>
#include <vector>
#include <unordered_map>
#include <set>
#include <memory>
#include <types/Type.h>
#include <types/Integer.h>
#include <types/ComplexType.h>
#include "MethodDescription.h"

class ClassDescription {
 public:
  explicit ClassDescription(std::string name) : class_name(std::move(name)) {}
  void AddAttribute(const std::string& name, const std::shared_ptr<Type>& type) {
    if (attributes.find(name) != attributes.end()) {
      throw std::runtime_error("Attr already declared" + name + "{class : " + class_name + " }");
    }
    attributes.insert(name);
    attributes_type[name] = type;
  }

  std::string GetAttribute(const std::string& name) {
    if (attributes.find(name) == attributes.end()) {
      throw std::runtime_error("Attr not declared: " + name + "{class : " + class_name + " }");
    }
    return *attributes.find(name);
  }

  bool HasAttribute(const std::string& name) {
    return (attributes.find(name) != attributes.end());
  }

  std::string GetMethod(const std::string& name) {
    if (methods.find(name) == methods.end()) {
      throw std::runtime_error("Method not declared: " + name + "{class : " + class_name + " }");
    }
    return *methods.find(name);
  }

  std::shared_ptr<MethodDescription> GetMethodDesc(const std::string& name) {
    if (methods_desc.find(name) == methods_desc.end()) {
      throw std::runtime_error("Method not declared: " + name + "{class : " + class_name + " }");
    }
    return methods_desc.find(name)->second;
  }


  void AddMethod(const std::string& name) {
    if (methods.find(name) != methods.end()) {
      throw std::runtime_error("Method already declared: " + name);
    }

    methods.insert(name);
    methods_desc[name] = std::make_shared<MethodDescription>(class_name, name);
  }
  std::string class_name;
  std::set<std::string> attributes;
  std::unordered_map<std::string, std::shared_ptr<Type>> attributes_type;
  std::unordered_set<std::string> methods;
  std::unordered_map<std::string, std::shared_ptr<MethodDescription>> methods_desc;
};