
#include "TypeWrapper.h"
#include <iostream>
std::shared_ptr<ClassDescription> TypeWrapper::GetClassDesc(const std::string &name) const {
  auto class_desc = classes.find(name);
  if (class_desc == classes.end()) {
    throw std::runtime_error("Type not declared: " + name);
  }
  return class_desc->second;
}

void TypeWrapper::AddClass(const std::string &name) {
  auto class_desc = classes.find(name);
  if (class_desc != classes.end()) {
    throw std::runtime_error("Type already declared: " + name);
  }
  classes[name] = std::make_shared<ClassDescription>(name);
}

void TypeWrapper::AddAttrToClass(const std::string &class_name, std::string attr, std::shared_ptr<Type> type) {
  auto class_desc = GetClassDesc(class_name);
  class_desc->AddAttribute(attr, type);
}
void TypeWrapper::AddMethodToClass(const std::string &class_name, const std::string &method) {
  auto class_desc = GetClassDesc(class_name);
  class_desc->AddMethod(method);
}

// TODO циклы зависимостей и т.д.
void TypeWrapper::CheckCorrectness() {
//  for (const auto& class_desc : classes) {
//    CountSize(class_desc.first);
//  }
  for (const auto& class_desc : classes) {
    int64_t last = 0;
    for (auto attr : class_desc.second->attributes_type) {
      attr_offsets[class_desc.first][attr.first] = last;
//      last += Size(attr.second);
      last += 4;
    }
    sizes[class_desc.second->class_name] = last;
  }

}

void TypeWrapper::Compare(const std::shared_ptr<Type>& first, const std::shared_ptr<Type>& second) {
  if (first->GetType() != second->GetType()) {
    throw std::runtime_error("Types are not equal");
  }
  if (first->GetType() == Type::RealType::COMPLEXTYPE) {
    auto a = std::dynamic_pointer_cast<ComplexType>(first);
    auto b = std::dynamic_pointer_cast<ComplexType>(second);
    if (a->class_name != b->class_name) {
      throw std::runtime_error("Types are not equal");
    }
  }
  if (first->GetType() == Type::RealType::ARRAY) {
//    std::cerr << "Compare arrays - good" << std::endl;
    auto a = std::dynamic_pointer_cast<Array>(first);
    auto b = std::dynamic_pointer_cast<Array>(second);
    Compare(a->type, b->type);
  }
}

void TypeWrapper::IsArithmetic(const std::shared_ptr<Type> &type) {
  if (type->GetType() != Type::RealType::INTEGER) {
    throw std::runtime_error("Element is not arithmetic");
  }
}

std::string TypeWrapper::IsCallable(const std::shared_ptr<Type> &type) {
  if (type->GetType() != Type::RealType::COMPLEXTYPE) {
    throw std::runtime_error("Element is not callable");
  }
  return std::dynamic_pointer_cast<ComplexType>(type)->class_name;
}

std::shared_ptr<ClassDescription> TypeWrapper::GetClass(const std::shared_ptr<Type>& type) const {
  if (type->GetType() != Type::RealType::COMPLEXTYPE) {
    throw std::runtime_error("Is not type of a class");
  }
  auto class_name = std::dynamic_pointer_cast<ComplexType>(type)->class_name;
  return GetClassDesc(class_name);
}

void TypeWrapper::AddArgToMethod(const std::string &class_name, const std::string &method, const std::shared_ptr<Type>& type) {
  GetClassDesc(class_name)->GetMethodDesc(method)->AddArg(type);
}

void TypeWrapper::AddReturnTypeToMethod(const std::string &class_name,
                                        const std::string &method,
                                        const std::shared_ptr<Type> &type) {
  GetClassDesc(class_name)->GetMethodDesc(method)->AddReturnType(type);
}
void TypeWrapper::AddStartPoint(const std::string &class_name, const std::string &method, MethodDeclaration *decl) {
  GetClassDesc(class_name)->GetMethodDesc(method)->AddStartPoint(decl);
}
std::shared_ptr<MethodDescription> TypeWrapper::GetMain() {
  for (const auto& class_ : classes) {
    if (class_.second->methods.find("main") != class_.second->methods.end()) {
      return class_.second->GetMethodDesc("main");
    }
  }
  throw std::runtime_error("There is no main function");
}
std::shared_ptr<MethodDescription> TypeWrapper::GetMethod(const std::string &class_name, const std::string &method_name) const {
  return GetClassDesc(class_name)->GetMethodDesc(method_name);
}

std::shared_ptr<Object> TypeWrapper::TakeAttr(std::shared_ptr<Object> obj, std::string attr) {
  return std::shared_ptr<Object>();
}

int64_t TypeWrapper::CountSize(std::string class_name) {
  if (sizes.find(class_name) != sizes.end()) {
    return sizes[class_name];
  }
  int64_t size = 0;
  for (auto attr : classes[class_name]->attributes_type) {
    if (attr.second->GetType() == Type::INTEGER) {
      size += 1;
    } else {
      size += CountSize(GetClass(attr.second)->class_name);
    }
  }
  sizes[class_name] = size;
  return size;
}

int64_t TypeWrapper::Size(std::shared_ptr<Type> type) {
  if (type->GetType() == Type::INTEGER) {
    return 4;
  } else {
    return sizes[GetClass(type)->class_name];
  }
}
int64_t* TypeWrapper::Allocate(const std::shared_ptr<Object>& obj) {
  if (obj->type->GetType() == Type::COMPLEXTYPE) {
    //std::cout << "Allocate class" << std::endl;
    //std::cout << "Size: " << sizes[GetClass(obj->type)->class_name] + 1 << std::endl;
    return new int64_t[sizes[GetClass(obj->type)->class_name] + 1];
  } else {
    obj->SetAddr(new int64_t);
    throw std::runtime_error("allocate Int WTF");
  }
}

// TODO : Bug - кто-то затирает 0 offset
int64_t* TypeWrapper::GetAttr(const std::shared_ptr<Object> &obj, std::string attr) {
  if (obj->type->GetType() == Type::COMPLEXTYPE) {
    auto class_desk = GetClass(obj->type);
    auto obj_next = std::make_shared<Object>(class_desk->attributes_type[attr]);
    int64_t* addr = (int64_t *)(*obj->data_) + attr_offsets[class_desk->class_name][attr];
    // std::cout << "Get Attr class " << attr_offsets[class_desk->class_name][attr] << " "  << attr << addr << std::endl;
    return addr;
  } else {
    throw std::runtime_error("Attr from Int WTF");
  }
}
std::shared_ptr<Type> TypeWrapper::GetArraySubType(const std::shared_ptr<Type> &type) const {
  if (type->GetType() != Type::ARRAY) {
    throw std::runtime_error("Type is not Array");
  }
  auto a = std::dynamic_pointer_cast<Array>(type);
  return a->type;
}

