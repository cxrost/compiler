
#include "ScopeLayer.h"

#include <utility>


ScopeLayer::ScopeLayer(ScopeLayer *parent) : parent_(parent) {

}
void ScopeLayer::DeclareVariable(const std::string& symbol, std::shared_ptr<Object> value) {
  if (values_.find(symbol) != values_.end()) {
    throw std::runtime_error("Variable has declared: " + symbol);
  }
  values_[symbol] = std::move(value);
}

std::shared_ptr<Object> ScopeLayer::Get(const std::string& symbol) {
  ScopeLayer* current_layer = this;

  while (!current_layer->Has(symbol) && current_layer->parent_ != nullptr) {
    current_layer = current_layer->parent_;
  }

  if (current_layer->Has(symbol)) {
    return current_layer->values_[symbol];
  } else {
    throw std::runtime_error("Variable not declared: " + symbol);
  }
}

bool ScopeLayer::Has(const std::string& symbol) {
  return values_.find(symbol) != values_.end();
}
void ScopeLayer::AddChild(ScopeLayer *child) {
  children_.emplace_back(child);
}

ScopeLayer *ScopeLayer::GetParent() const {
  return parent_;
}

ScopeLayer::~ScopeLayer() {
  for (ScopeLayer* layer: children_) {
    delete layer;
  }
}

