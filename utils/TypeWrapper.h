#pragma once

#include <unordered_map>
#include <string>
#include <memory>
#include "types/Object.h"
#include <grammar/declarations/MethodDeclaration.h>

#include "ClassDescription.h"

class TypeWrapper {
 public:
  std::shared_ptr<ClassDescription> GetClassDesc(const std::string& name) const;
  std::shared_ptr<ClassDescription> GetClass(const std::shared_ptr<Type>& type) const;
  std::shared_ptr<Type> GetArraySubType(const std::shared_ptr<Type>& type) const;
  std::shared_ptr<MethodDescription> GetMethod(const std::string& class_name, const std::string& method_name) const;

  void AddClass(const std::string& name);
  void AddAttrToClass(const std::string &class_name, std::string attr, std::shared_ptr<Type> type);
  void AddMethodToClass(const std::string& class_name, const std::string& method);
  void AddArgToMethod(const std::string& class_name, const std::string& method, const std::shared_ptr<Type>& type);
  void AddReturnTypeToMethod(const std::string& class_name, const std::string& method, const std::shared_ptr<Type>& type);
  void AddStartPoint(const std::string& class_name, const std::string& method, MethodDeclaration* decl);


  void Compare(const std::shared_ptr<Type>& first, const std::shared_ptr<Type>& second);
  void IsArithmetic(const std::shared_ptr<Type>& type);
  std::shared_ptr<Object> TakeAttr(std::shared_ptr<Object> obj, std::string attr);
  std::string IsCallable(const std::shared_ptr<Type>& type);
  void CheckCorrectness();
  std::shared_ptr<MethodDescription> GetMain();

  int64_t* Allocate(const std::shared_ptr<Object>& obj);
  int64_t* GetAttr(const std::shared_ptr<Object>& obj, std::string attr);

  std::string GetTypeByString(std::shared_ptr<Type> type) {
    std::cout << (type == nullptr) << std::endl;
    if (type->GetType() == Type::INTEGER) {
      return "Integer";
    }
    if (type->GetType() == Type::COMPLEXTYPE) {
      return GetClass(type)->class_name;
    }
    auto a = std::dynamic_pointer_cast<Array>(type);
    return GetTypeByString(a->type) + "[]";
  }

  int64_t Size(std::shared_ptr<Type> type);
  //private:
  std::unordered_map<std::string, std::shared_ptr<ClassDescription>> classes;
  std::unordered_map<std::string, int64_t> sizes;
  std::unordered_map<std::string, std::unordered_map<std::string, int64_t>> attr_offsets;
 private:
  int64_t CountSize(std::string);
};
