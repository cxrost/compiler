//
// Created by pavel on 05.08.2020.
//
#pragma once
#include <IrUtils/utils/Temporary.h>
#include <IrUtils/utils/Label.h>
#include "assembler/instructions/BaseInstruct.h"

namespace ASM {

class Label : public BaseInstruct {
 public:
  ~Label() override = default;
  explicit Label(IRT::Label label);
  void Accept(ASM::Visitor *visitor) override;
  std::string ToString() override;
  IRT::Label label_;
};

}