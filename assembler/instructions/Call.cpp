//
// Created by pavel on 05.08.2020.
//

#include "Call.h"

#include <utility>

void ASM::Call::Accept(ASM::Visitor *visitor) {
  visitor->Visit(this);
}

std::string ASM::Call::ToString() {
  auto out =  CommandToString() + " [" + ret_.ToString() + "], " + label_.ToString() + ", ";
  for (const auto& arg : args_) {
    out += arg.ToString() + ", ";
  }
  return out;
}

ASM::Call::Call(IRT::Temporary ret, IRT::Label label, std::vector<IRT::Temporary> args)
: BaseInstruct(InstructType::CALL), ret_(std::move(ret)), label_(std::move(label)), args_(std::move(args)) {

}
