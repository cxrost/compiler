//
// Created by pavel on 05.08.2020.
//

#include "Store.h"

#include <utility>

void ASM::Store::Accept(ASM::Visitor *visitor) {
  visitor->Visit(this);
}

ASM::Store::Store(IRT::Temporary addr, int offset, IRT::Temporary source)
    : BaseInstruct(InstructType::STORE), addr_(std::move(addr)), offset_(offset), source_(std::move(source)) {}
std::string ASM::Store::ToString() {
  return CommandToString() + " " + addr_.ToString() + ", " + std::to_string(offset_) + ", " + source_.ToString();
}
