//
// Created by pavel on 05.08.2020.
//

#include "MoveEm.h"

#include <utility>

void ASM::MoveEm::Accept(ASM::Visitor *visitor) {
  visitor->Visit(this);
}

ASM::MoveEm::MoveEm(IRT::Temporary target, IRT::Temporary source)
    : BaseInstruct(InstructType::MOVEM), target_(std::move(target)), source_(std::move(source)) {}

std::string ASM::MoveEm::ToString() {
  return CommandToString() + target_.ToString() + " " + source_.ToString();
}
