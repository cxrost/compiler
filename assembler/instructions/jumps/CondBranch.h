//
// Created by pavel on 05.08.2020.
//
#pragma once
#include <IrUtils/utils/Temporary.h>
#include <IrUtils/utils/Label.h>
#include "assembler/instructions/BaseInstruct.h"

namespace ASM {

class CondBranch : public BaseInstruct {
 public:
  ~CondBranch() override = default;
  CondBranch(InstructType type, IRT::Label label_, IRT::Temporary expr_);
  void Accept(ASM::Visitor *visitor) override;
  std::string ToString() override ;
  IRT::Label label_;
  IRT::Temporary expr_;
};

}