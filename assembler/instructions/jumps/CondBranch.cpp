//
// Created by pavel on 05.08.2020.
//

#include "CondBranch.h"

#include <utility>

void ASM::CondBranch::Accept(ASM::Visitor *visitor) {
  visitor->Visit(this);
}

ASM::CondBranch::CondBranch(InstructType type, IRT::Label label, IRT::Temporary expr)
    : BaseInstruct(type), label_(std::move(label)), expr_(std::move(expr)) {}
std::string ASM::CondBranch::ToString() {
  return CommandToString() + " " + label_.ToString() + ", " + expr_.ToString();
}
