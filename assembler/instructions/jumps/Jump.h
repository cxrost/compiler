//
// Created by pavel on 05.08.2020.
//
#pragma once
#include <IrUtils/utils/Temporary.h>
#include <IrUtils/utils/Label.h>
#include "assembler/instructions/BaseInstruct.h"

namespace ASM {

class Jump : public BaseInstruct {
 public:
  ~Jump() override = default;
  explicit Jump(IRT::Label label_);
  void Accept(ASM::Visitor *visitor) override;
  std::string ToString() override;
  IRT::Label label_;
};

}