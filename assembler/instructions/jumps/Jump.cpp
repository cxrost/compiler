//
// Created by pavel on 05.08.2020.
//

#include "Jump.h"

#include <utility>

void ASM::Jump::Accept(ASM::Visitor *visitor) {
  visitor->Visit(this);
}

ASM::Jump::Jump(IRT::Label label)
    : BaseInstruct(InstructType::JUMP), label_(std::move(label)) {}
std::string ASM::Jump::ToString() {
  return CommandToString() + " " + label_.ToString();
}
