//
// Created by pavel on 05.08.2020.
//
#pragma once
#include <IrUtils/utils/Temporary.h>
#include "assembler/instructions/BaseInstruct.h"

namespace ASM {

class Store : public BaseInstruct {
 public:
  ~Store() override = default;
  Store(IRT::Temporary addr, int offset, IRT::Temporary source);
  void Accept(ASM::Visitor *visitor) override;
  std::string ToString() override;
  IRT::Temporary addr_;
  int offset_;
  IRT::Temporary source_;
};

}