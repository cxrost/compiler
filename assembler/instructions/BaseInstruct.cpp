//
// Created by pavel on 05.08.2020.
//

#include <iostream>
#include "BaseInstruct.h"

std::string ASM::BaseInstruct::CommandToString() {
  switch (type_) {
    case InstructType::LABEl: return  "label";
    case InstructType::ADD: return "add";
    case InstructType::SUB: return "sub";;
    case InstructType::MUL: return "mul";
    case InstructType::DIV: return "div";
    case InstructType::ADDI: return "addi";
    case InstructType::SUBI: return "subi";
    case InstructType::MOVEA: return "movea";
    case InstructType::MOVED: return "moved";
    case InstructType::LOAD: return "load";
    case InstructType::STORE: return "store";
    case InstructType::MOVEM: return "movem";
    case InstructType::BGE: return "bge";
    case InstructType::BLT: return "blt";
    case InstructType::BEQ: return "beq";
    case InstructType::BNG: return "bng";
    case InstructType::JUMP: return "jump";
    case InstructType::CALL: return "call";
    case InstructType::RETURN:  return "return";

  }

}
std::string ASM::BaseInstruct::ToString() {
  return CommandToString();
}
