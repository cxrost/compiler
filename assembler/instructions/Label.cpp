//
// Created by pavel on 05.08.2020.
//

#include "Label.h"

#include <utility>

void ASM::Label::Accept(ASM::Visitor *visitor) {
  visitor->Visit(this);
}

ASM::Label::Label(IRT::Label label)
    : BaseInstruct(InstructType::LABEl), label_(std::move(label)){}

std::string ASM::Label::ToString() {
  return label_.ToString() + ":";
}
