//
// Created by pavel on 05.08.2020.
//

#include "Load.h"

#include <utility>

void ASM::Load::Accept(ASM::Visitor *visitor) {
  visitor->Visit(this);
}

ASM::Load::Load(IRT::Temporary target, IRT::Temporary addr, int offset)
    : BaseInstruct(InstructType::LOAD), target_(std::move(target)), addr_(std::move(addr)), offset_(offset) {}
std::string ASM::Load::ToString() {
  return CommandToString() + " [" + target_.ToString() + "] " + addr_.ToString() + " " + std::to_string(offset_);
}
