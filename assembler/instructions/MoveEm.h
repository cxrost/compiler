//
// Created by pavel on 05.08.2020.
//
#pragma once
#include <IrUtils/utils/Temporary.h>
#include "assembler/instructions/BaseInstruct.h"

namespace ASM {

class MoveEm : public BaseInstruct {
 public:
  ~MoveEm() override = default;
  MoveEm(IRT::Temporary target_, IRT::Temporary source_);
  void Accept(ASM::Visitor *visitor) override;
  std::string ToString() override;
  IRT::Temporary target_;
  IRT::Temporary source_;
};

}