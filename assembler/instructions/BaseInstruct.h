//
// Created by pavel on 05.08.2020.
//

#pragma once

#include <vector>
#include <string>
#include "assembler/visitors/Visitor.h"

namespace ASM {

class BaseInstruct {
 public:
  enum class InstructType {
    ADD,
    SUB,
    MUL,
    DIV,
    ADDI,
    SUBI,

    MOVEA,
    MOVED,

    LOAD,
    STORE,
    MOVEM,

    BGE,
    BLT,
    BEQ,
    BNG,
    JUMP,

    CALL,
    RETURN,
    LABEl
  };
  std::string CommandToString();
  virtual void Accept(ASM::Visitor* visitor) = 0;
  virtual std::string ToString() = 0;
  virtual ~BaseInstruct() = default;
  explicit BaseInstruct(InstructType type) : type_(type) {}
  InstructType type_;
};

}
