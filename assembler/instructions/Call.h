//
// Created by pavel on 05.08.2020.
//
#pragma once
#include <IrUtils/utils/Temporary.h>
#include "assembler/instructions/BaseInstruct.h"
#include "Label.h"

namespace ASM {

class Call : public BaseInstruct {
 public:
  ~Call() override = default;
  Call(IRT::Temporary ret, IRT::Label label, std::vector<IRT::Temporary> args);
  void Accept(ASM::Visitor *visitor) override;
  std::string ToString() override;
  IRT::Temporary ret_;
  IRT::Label label_;
  std::vector<IRT::Temporary> args_;
};

}