//
// Created by pavel on 05.08.2020.
//

#include "SubI.h"

#include <utility>

void ASM::SubI::Accept(ASM::Visitor *visitor) {
  visitor->Visit(this);
}

ASM::SubI::SubI(IRT::Temporary target, IRT::Temporary l_arg, int r_arg)
    : BaseInstruct(InstructType::SUBI), target_(std::move(target)), l_arg_(std::move(l_arg)), r_arg_(r_arg) {}
