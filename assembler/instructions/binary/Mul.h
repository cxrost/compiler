//
// Created by pavel on 05.08.2020.
//
#pragma once
#include <IrUtils/utils/Temporary.h>
#include "assembler/instructions/BaseInstruct.h"

namespace ASM {

class Mul : public BaseInstruct {
 public:
  ~Mul() override = default;
  Mul(IRT::Temporary target_, IRT::Temporary l_arg_, IRT::Temporary r_arg_);
  void Accept(ASM::Visitor *visitor) override;
  std::string ToString() override;
  IRT::Temporary target_;
  IRT::Temporary l_arg_;
  IRT::Temporary r_arg_;
};

}