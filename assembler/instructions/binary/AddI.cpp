//
// Created by pavel on 05.08.2020.
//

#include "AddI.h"

#include <utility>

void ASM::AddI::Accept(ASM::Visitor *visitor) {
  visitor->Visit(this);
}

ASM::AddI::AddI(IRT::Temporary target, IRT::Temporary l_arg, int r_arg)
    : BaseInstruct(InstructType::ADDI), target_(std::move(target)), l_arg_(std::move(l_arg)), r_arg_(r_arg) {}
std::string ASM::AddI::ToString() {
  return CommandToString() + " [" + target_.ToString() + "] " + l_arg_.ToString() + " " + std::to_string(r_arg_);
}
