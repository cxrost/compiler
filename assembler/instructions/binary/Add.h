//
// Created by pavel on 05.08.2020.
//
#pragma once
#include <IrUtils/utils/Temporary.h>
#include "assembler/instructions/BaseInstruct.h"

namespace ASM {

class Add : public BaseInstruct {
 public:
  ~Add() override = default;
  Add(IRT::Temporary target_, IRT::Temporary l_arg_, IRT::Temporary r_arg_);
  void Accept(ASM::Visitor *visitor) override;
  std::string ToString() override;
  IRT::Temporary target_;
  IRT::Temporary l_arg_;
  IRT::Temporary r_arg_;
};

}