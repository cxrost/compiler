//
// Created by pavel on 05.08.2020.
//

#include "Add.h"

#include <utility>

void ASM::Add::Accept(ASM::Visitor *visitor) {
  visitor->Visit(this);
}

ASM::Add::Add(IRT::Temporary target, IRT::Temporary l_arg, IRT::Temporary r_arg)
    : BaseInstruct(InstructType::ADD), target_(std::move(target)), l_arg_(std::move(l_arg)), r_arg_(std::move(r_arg)) {}
std::string ASM::Add::ToString() {
  return CommandToString() + " [" + target_.ToString() + "] " + l_arg_.ToString() + " " + r_arg_.ToString();
}
