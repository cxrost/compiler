//
// Created by pavel on 05.08.2020.
//

#include "Mul.h"

#include <utility>

void ASM::Mul::Accept(ASM::Visitor *visitor) {
  visitor->Visit(this);
}

ASM::Mul::Mul(IRT::Temporary target, IRT::Temporary l_arg, IRT::Temporary r_arg)
    : BaseInstruct(InstructType::MUL), target_(std::move(target)), l_arg_(std::move(l_arg)), r_arg_(std::move(r_arg)) {}
std::string ASM::Mul::ToString() {
  return CommandToString() + " [" + target_.ToString() + "] " + l_arg_.ToString() + " " + r_arg_.ToString();
}
