//
// Created by pavel on 05.08.2020.
//
#pragma once
#include <IrUtils/utils/Temporary.h>
#include "assembler/instructions/BaseInstruct.h"

namespace ASM {

class SubI : public BaseInstruct {
 public:
  ~SubI() override = default;
  SubI(IRT::Temporary target_, IRT::Temporary l_arg_, int r_arg_);
  void Accept(ASM::Visitor *visitor) override;


  IRT::Temporary target_;
  IRT::Temporary l_arg_;
  int r_arg_;
};

}