//
// Created by pavel on 05.08.2020.
//
#pragma once
#include <IrUtils/utils/Temporary.h>
#include <IrUtils/nodes/expressions/ConstExpr.h>
#include "assembler/instructions/BaseInstruct.h"

namespace ASM {

class AddI : public BaseInstruct {
 public:
  ~AddI() override = default;
  AddI(IRT::Temporary target_, IRT::Temporary l_arg_, int r_arg_);
  void Accept(ASM::Visitor *visitor) override;
  std::string ToString() override;
  IRT::Temporary target_;
  IRT::Temporary l_arg_;
  int r_arg_;
};

}