//
// Created by pavel on 05.08.2020.
//

#pragma once
#include <fstream>
#include "Visitor.h"
namespace ASM {

class PrintVisitor : public Visitor {
 public:

  void Print(const std::string& str) {
    stream_ << str << std::endl;
  }
  explicit PrintVisitor(const std::string& filename);
  void Visit(Add* instr) override;
  void Visit(Sub* instr) override;
  void Visit(Mul* instr) override;
  void Visit(Div* instr) override;
  void Visit(AddI* instr) override;
  void Visit(SubI* instr) override;
  void Visit(Jump* instr) override;
  void Visit(CondBranch* instr) override;
  void Visit(Load* instr) override;
  void Visit(Store* instr) override;
  void Visit(MoveEm* instr) override;
  void Visit(Label* instr) override;
  void Visit(Call* instr) override;
  std::ofstream stream_;
};

} 
