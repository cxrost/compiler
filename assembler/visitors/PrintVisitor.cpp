//
// Created by pavel on 05.08.2020.
//


#include "PrintVisitor.h"

#include "ASMincludes.h"
void ASM::PrintVisitor::Visit(ASM::Add *instr) {
  stream_ << "\t" << instr->ToString() << std::endl;
}
void ASM::PrintVisitor::Visit(ASM::Sub *instr) {
  stream_ << "\t" << instr->ToString() << std::endl;
}
void ASM::PrintVisitor::Visit(ASM::Mul *instr) {
  stream_ << "\t" << instr->ToString() << std::endl;
}
void ASM::PrintVisitor::Visit(ASM::Div *instr) {
  stream_ << "\t" << instr->ToString() << std::endl;
}
void ASM::PrintVisitor::Visit(ASM::AddI *instr) {
  stream_ << "\t" << instr->ToString() << std::endl;
}
void ASM::PrintVisitor::Visit(ASM::SubI *instr) {
  stream_ << "\t" << instr->ToString() << std::endl;
}
void ASM::PrintVisitor::Visit(ASM::Jump *instr) {
  stream_ << "\t" << instr->ToString() << std::endl;
}
void ASM::PrintVisitor::Visit(ASM::CondBranch *instr) {
  stream_ << "\t" << instr->ToString() << std::endl;
}
void ASM::PrintVisitor::Visit(ASM::Load *instr) {
  stream_ << "\t" << instr->ToString() << std::endl;
}
void ASM::PrintVisitor::Visit(ASM::Store *instr) {
  stream_ << "\t" << instr->ToString() << std::endl;
}
void ASM::PrintVisitor::Visit(ASM::MoveEm *instr) {
  stream_ << "\t" << instr->ToString() << std::endl;
}
void ASM::PrintVisitor::Visit(ASM::Label *instr) {
  stream_ << instr->ToString() << std::endl;
}
void ASM::PrintVisitor::Visit(ASM::Call *instr) {
  stream_ << "\t" << instr->ToString() << std::endl;
}
ASM::PrintVisitor::PrintVisitor(const std::string &filename) : stream_(filename){
  
}
