//
// Created by pavel on 05.08.2020.
//

namespace ASM {
  class Add;
  class Sub;
  class Mul;
  class Div;
  class AddI;
  class SubI;
  class Jump;
  class CondBranch;
  class Load;
  class Store;
  class MoveEm;
  class Label;
  class Call;

}