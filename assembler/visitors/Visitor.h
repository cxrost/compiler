//
// Created by pavel on 05.08.2020.
//
#pragma once

#include "forward_ASM_decl.h"

namespace ASM {

class Visitor {
 public:
  virtual void Visit(Add* instr) = 0;
  virtual void Visit(Sub* instr) = 0;
  virtual void Visit(Mul* instr) = 0;
  virtual void Visit(Div* instr) = 0;
  virtual void Visit(AddI* instr) = 0;
  virtual void Visit(SubI* instr) = 0;
  virtual void Visit(Jump* instr) = 0;
  virtual void Visit(CondBranch* instr) = 0;
  virtual void Visit(Load* instr) = 0;
  virtual void Visit(Store* instr) = 0;
  virtual void Visit(MoveEm* instr) = 0;
  virtual void Visit(Label* instr) = 0;
  virtual void Visit(Call* instr) = 0;
};

}
