//
// Created by pavel on 05.08.2020.
//
#pragma once
#include "assembler/instructions/binary/Add.h"
#include "assembler/instructions/binary/Sub.h"
#include "assembler/instructions/binary/Mul.h"
#include "assembler/instructions/binary/Div.h"
#include "assembler/instructions/binary/AddI.h"
#include "assembler/instructions/binary/SubI.h"


#include "assembler/instructions/jumps/Jump.h"
#include "assembler/instructions/jumps/CondBranch.h"


#include "assembler/instructions/Load.h"
#include "assembler/instructions/Store.h"
#include "assembler/instructions/MoveEm.h"
#include "assembler/instructions/BaseInstruct.h"
#include "assembler/instructions/Label.h"
#include "assembler/instructions/Call.h"

