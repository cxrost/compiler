#include <iostream>
#include <string>
#include <driver.hh>
#include <fstream>
#include <exception>

int main(int argc, char** argv) {
  int result = 0;
  Driver driver;
  driver.trace_parsing = false;
  driver.trace_scanning = false;
  for (int i = 1; i < argc; ++i) {
    if (argv[i] == std::string("-p")) {
      driver.trace_parsing = true;
    } else if (argv[i] == std::string("-s")) {
      driver.trace_scanning = true;
    } else {
      try {
        driver.parse(argv[i]);
        driver.PrintAST(argv[i + 1]);
      } catch (std::exception& ex) {
        std::cout << "Compile Error:\n " << ex.what() << std::endl;
      }
      ++i;
    }
  }

  return result;
}